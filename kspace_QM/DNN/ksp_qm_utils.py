import os, sys, time
import torch
from ksp_qm_models import *
from ksp_qm_models import kspace_quality_metric_cnn

def saveRandomMinibatchPredToDisk(x_val, out_val, y_val, dstDir):
    torch.save(x_val, os.path.join(dstDir, 'random_minibatch_x_val.pth'))
    torch.save(out_val, os.path.join(dstDir, 'random_minibatch_prediction_val.pth'))
    torch.save(y_val, os.path.join(dstDir, 'random_minibatch_y_val.pth'))


def print2logfile(infoString, dstDir, fn='log.txt'):
    with open(os.path.join(dstDir,fn), 'a') as f:
        f.write(infoString+'\n')
        
def printCurrentLr(opt, dstDir):
    with torch.no_grad():
        current_lr = opt.param_groups[0]['lr']
        print2logfile(infoString=str(current_lr), dstDir=dstDir, fn='lr.txt')
            
            
def load_model(checkpoint_path, nb_kernels=[8, 16, 24], modelType = 'v1', map_loc='cpu'):
    print('DEBUG : entered in load_model func ... ') 
    from ksp_qm_models import kspace_quality_metric_cnn
    mu = torch.load(os.path.join(checkpoint_path, 'mu.pth'))
    std = torch.load(os.path.join(checkpoint_path, 'std.pth'))
    model = kspace_quality_metric_cnn(num_classes=256, nb_kernels=nb_kernels)
    model.load_state_dict(torch.load(checkpoint_path, map_location=torch.device(map_loc)))
    model.eval()
    return model, mu, std


    
def computeModelAccuray(model, X, Y, mbsz, PE_range=None):
    n=0.0
    error_rate = 0.0
    if PE_range is None:
        PE_range = [0] +list(range(5, 119)) + list(range(140, 250))

    model.eval()
    with torch.no_grad():
        for x, y in zip(X.split(mbsz), Y.split(mbsz)):
            out = model(x)#out shape minibatchsz x 1 x 256
            idx = torch.argmax(out, dim=2)
            y=y.view_as(idx).to(idx.dtype)#reshape as idx's shape and datatype

            #ne stands for 'not equal'
            error_rate += torch.ne(idx, y).sum().float() / mbsz #% error on batch

            n+=1
            
            '''
            if idx.T.sum() != 0 and e >= 10 and datasetType == 'VAL':
                print('\n\n------ type: ', datasetType)
                print('out max=', idx.T)
                print('y=', y.T)
                print('------------------------\n\n')
                time.sleep(3.0)
            
            '''
    return error_rate/n



def computeModelAccuray_MSE256(model, X, Y, mbsz, PE_range=None):
    n=0.0
    error_rate = 0.0
    tol = 4.0#means that 'classification' is correct within 4 PE lines before or after e.g. PE71 or PE79 are ok for a true PE=75
    model.eval()
    with torch.no_grad():
        for x, y in zip(X.split(mbsz), Y.split(mbsz)):
            out = model(x)#out shape minibatchsz x 1 x 256
            out=out.squeeze().float()
            y=y.squeeze().float()
            idx_out=torch.argmax(out, dim=1).float()
            idx_y=torch.argmax(y, dim=1).float()
            
            #"1.0 to implicitly convert bool to float
            abs_error = torch.sum( (torch.abs(idx_out-idx_y) > tol )*1.0 ).item()
            error_rate += abs_error / mbsz #% error on batch

            n+=1

            '''
            print('DEBUG in comp. accu. 256:', ' out shape=', out.shape, ', y.shape=', y.shape, ', idx_out.shape=', idx_out.shape, ', idx_y.shape=', idx_y.shape)   
            print('abs_Err=', abs_error)
            print('err_rate=', abs_error / mbsz)
            time.sleep(10.0)
            print('\n\n idx_out=', idx_out)
            print('\n\n idx_y = ', idx_y)
            print('\n')
            time.sleep(10.0)
            '''
    return error_rate/n



def computeModelAccuray_MSE(model, X, Y, mbsz, PE_range=None):
    n=0.0
    error_rate = 0.0
    tol = 4.0
    model.eval()
    with torch.no_grad():
        for x, y in zip(X.split(mbsz), Y.split(mbsz)):
            out = model(x)#out shape minibatchsz x 1 x 256
            out=out.squeeze().float()
            y=y.squeeze().float()
            #"1.0 to convert bool to float
            abs_error = torch.sum( (torch.abs(out-y) > tol )*1.0 ).item()
            error_rate += abs_error / mbsz #% error on batch

            n+=1
            
    return error_rate/n


    

#mbsz : minibatch size
def computeErrorRate_perClass(model, X, Y, mbsz, PE_range=None):
    err_cls = torch.zeros(256)
    if PE_range is None:
        PE_range = [0] +list(range(5, 119)) + list(range(140, 250))
    model.eval()
    with torch.no_grad():
        for x, y in zip(X.split(mbsz), Y.split(mbsz)):
            out = model(x)#out shape minibatchsz x 1 x 256
            idx = torch.argmax(out, dim=2)
            y=y.view_as(idx).to(idx.dtype)#reshape as idx's shape and datatype

            
            for i, y_ in enumerate( y.flatten().tolist() ):
                #y_ is the gnd truth vector and idx is the predicted vector
                # err_cls indexed by y_ will place the errors in the correct bin
                #since y_ is the gnd truth.
                print('DEBUG: y_=', y_, ', idx[i].item() = ', idx[i].item())
                err_cls[y_] += int(y_ != idx[i].item())

    return err_cls







'''

def computeModelAccuray_soft(model, X, Y, mbsz, PE_range=None):
    n=0.0
    error_rate = 0.0
    thresh = 0.5
    
    if PE_range is None:
        PE_range = [0] +list(range(5, 119)) + list(range(140, 250))

    model.eval()
    with torch.no_grad():
        for x, y in zip(X.split(mbsz), Y.split(mbsz)):
            
            out = model(x.view(-1,1,256))#out shape minibatchsz x 1 x 256
            idx = (x >= thresh)*1.0 #*1.0 to convert bool to float
            #y=y.view_as(idx).to(idx.dtype)#reshape as idx's shape and datatype
            idx_thresh = torch.where(idx >= 1.0)
            y_thresh = torch.where( (y >= thresh)*1.0 >= 1.0)
            #index like [0] because tuples are returned by torch.where 
            error_rate += (   torch.logical_not(torch.all(torch.isin(idx_thresh[0], y_thresh[0])))*1.0 ).item()
            #ne stands for 'not equal'
            #error_rate += torch.ne(idx, y).sum().float() / mbsz #% error on batch

            n+=1
            
    return error_rate/n

'''
