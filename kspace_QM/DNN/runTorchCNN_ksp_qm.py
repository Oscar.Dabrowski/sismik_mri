import os, sys, time
import torch
import numpy as np

from torch import optim, nn
from torch.nn import functional as F
import h5py
import json



class kspace_quality_metric_cnn(nn.Module):

    def __init__(self, num_classes, nb_kernels=[8,16,32]):

        super(kspace_quality_metric_cnn, self).__init__()

        pad=1
        self.num_classes=num_classes
        self.prelu = nn.PReLU()

        self.conv1 = nn.Conv1d(1, nb_kernels[0], kernel_size = 3, padding=pad)
        self.conv2 = nn.Conv1d(nb_kernels[0], nb_kernels[1], kernel_size = 3,  padding=pad)
        self.conv3 = nn.Conv1d(nb_kernels[1], nb_kernels[2], kernel_size = 3,  padding=pad)

        self.conv_out = nn.Conv1d(nb_kernels[2], 1, kernel_size=3, padding=pad)

    def forward(self, x):

        x = self.prelu(self.conv1(x))

        x = self.prelu(self.conv2(x))

        x = self.prelu(self.conv3(x))


        #no activation here because will use nn.CrossEntropyLoss
        x = self.conv_out(x)

        return x



def load_model(checkpoint_path, nb_kernels=[3, 6, 9], modelType = 'v1', map_loc='cpu'):
    mu = torch.load(os.path.join(checkpoint_path, 'mu.pth'),  map_location=torch.device(map_loc)).numpy()
    std = torch.load(os.path.join(checkpoint_path, 'std.pth'),  map_location=torch.device(map_loc)).numpy()
    model = kspace_quality_metric_cnn(num_classes=256, nb_kernels=nb_kernels)
    chkpt = os.path.join(checkpoint_path, 'checkpoint.pth')
    model.load_state_dict(torch.load(chkpt, map_location=torch.device(map_loc))['model_state'] )
    model.eval()
    return model, mu, std



#def predict(model, mu, std, input_data):
#    # need to pass by numpy because Matlab does not recog pytorch tensors
#    if isinstance(input_data, np.ndarray):
#        input_data = torch.from_numpy(input_data).float()#
#
#    model.eval()
#   
#   with torch.no_grad():
#        return model(input_data.sub_(mu).div_(std)).numpy()

    
def predict(model, mu, std, input_data):
    model.eval()
    if isinstance(mu, np.ndarray):
        mu = torch.from_numpy(mu)
    if isinstance(std, np.ndarray):
        std = torch.from_numpy(std)
    if isinstance(input_data, np.ndarray):
        input_data = torch.from_numpy(input_data)
    input_data = input_data.view(-1, 1, 256).float()
    with torch.no_grad():
        return model(input_data.sub_(mu).div_(std))



def readKspMatfile(f):
    with h5py.File(f, 'r') as file:
        angle = torch.tensor( np.array(file['out']['angle']) )
        PELine = torch.tensor( np.array(file['out']['PELine']) )
        pnorm_vector = torch.tensor( np.array(file['out']['pnorm']) )
        simu_labels = torch.tensor( np.array(file['out']['simu_labels']['theta']) )#angle traj only
    return pnorm_vector, angle, PELine, simu_labels



if __name__=='__main__':

    f = sys.argv[1] #should be the filepath (+name) of the corrupted kspace acquisition (with pnorm )

    

    
    #hardcoded model for now
    checkpoint_path = '/home/od/Lp_things/model_FCNN_v2_e150_lr0_0001_nbkrnls_3__6__9__minib32_L2reg0_0_empZ0_3_model_v1_idxRnd/'
    model, mu, std = load_model(checkpoint_path, nb_kernels=[3, 6, 9], modelType = 'v1', map_loc='cpu')
    model.eval()

    pnorm_vector, angle, PELine, simu_labels = readKspMatfile(f)
    
    model_raw_predictions = predict(model=model, mu=mu, std=std, input_data = pnorm_vector)

    
    predicted_PELine = torch.argmax(model_raw_predictions).item()

    formatted_result = {"status": "success", "data": predicted_PELine}
    
    try:
        print(json.dumps(formatted_result))
    except Exception as e:
        print(json.dumps({"status": "error", "message": str(e)}))
        sys.exit(1)
