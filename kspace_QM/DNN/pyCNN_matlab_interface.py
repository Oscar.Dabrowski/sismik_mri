import os, sys, time
import torch
import numpy as np

from torch import optim, nn
from torch.nn import functional as F


class kspace_quality_metric_cnn(nn.Module):

    def __init__(self, num_classes, nb_kernels=[8,16,32]):

        super(kspace_quality_metric_cnn, self).__init__()

        pad=1
        self.num_classes=num_classes
        self.prelu = nn.PReLU()

        self.conv1 = nn.Conv1d(1, nb_kernels[0], kernel_size = 3, padding=pad)
        self.conv2 = nn.Conv1d(nb_kernels[0], nb_kernels[1], kernel_size = 3,  padding=pad)
        self.conv3 = nn.Conv1d(nb_kernels[1], nb_kernels[2], kernel_size = 3,  padding=pad)

        self.conv_out = nn.Conv1d(nb_kernels[2], 1, kernel_size=3, padding=pad)

    def forward(self, x):

        x = self.prelu(self.conv1(x))

        x = self.prelu(self.conv2(x))

        x = self.prelu(self.conv3(x))


        #no activation here because will use nn.CrossEntropyLoss
        x = self.conv_out(x)

        return x
    
def load_model(checkpoint_path, nb_kernels=[8, 16, 24], modelType = 'v1', map_loc='cpu'):
    
    #mu = torch.load(os.path.join(checkpoint_path, 'mu.pth'),  map_location=torch.device(map_loc)).numpy()
    #std = torch.load(os.path.join(checkpoint_path, 'std.pth'),  map_location=torch.device(map_loc)).numpy()
    model = kspace_quality_metric_cnn(num_classes=256, nb_kernels=nb_kernels)
    chkpt = os.path.join(checkpoint_path, 'checkpoint.pth')
    model.load_state_dict(torch.load(chkpt, map_location=torch.device(map_loc))['model_state'] )
    model.eval()
    return model#, mu, std



def predict(model, mu, std, input_data):
    # need to pass by numpy because Matlab does not recog pytorch tensors
    if isinstance(input_data, np.ndarray):
        input_data = torch.from_numpy(input_data).float()

    model.eval()
    
    with torch.no_grad():
        return model(input_data.sub_(mu).div_(std)).numpy()
