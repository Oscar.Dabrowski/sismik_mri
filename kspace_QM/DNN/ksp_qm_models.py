import torch
from torch import optim, nn
from torch.nn import functional as F
import numpy as np
#import matplotlib.pyplot as plt
import sys, os, time
#from torchinfo import summary



import torch
import torch.nn as nn
import torch.nn.functional as F



class kspace_quality_metric_cnn_basic(nn.Module):
    
    def __init__(self, nb_kernels=256, kernelSize = 3):
        
        super(kspace_quality_metric_cnn_basic, self).__init__()

        self.prelu = nn.PReLU()
        
        self.conv1 = nn.Conv1d(1, nb_kernels, kernel_size = kernelSize, padding = int( (kernelSize-1)/2 ) )
        self.conv_1x1_out = nn.Conv1d(nb_kernels, 1, kernel_size = 1 )
        
    def forward(self, x):
        
        x = self.prelu(self.conv1(x))
        x = self.conv_1x1_out(x)
        return x



    
class Inception1DBlock(nn.Module):
    def __init__(self, in_channels=1, internal_channels=None):
        super(Inception1DBlock, self).__init__()

        NB_BRANCHES = 5
        
        # Branch 1: 1x1 Conv
        self.branch1x1 = nn.Sequential(
            nn.Conv1d(in_channels, internal_channels, kernel_size=1),
            nn.PReLU()
        )
        
        # Branch 2: 1x1 Conv -> 3x3 Conv
        self.branch3x3 = nn.Sequential(
            nn.Conv1d(in_channels, internal_channels, kernel_size=1),
            nn.PReLU(),
            nn.Conv1d(internal_channels, internal_channels, kernel_size=3, padding=1),
            nn.PReLU()
        )
        
        # Branch 3: 1x1 Conv -> 5x5 Conv
        self.branch5x5 = nn.Sequential(
            nn.Conv1d(in_channels, internal_channels, kernel_size=1),
            nn.PReLU(),
            nn.Conv1d(internal_channels, internal_channels, kernel_size=5, padding=2),
            nn.PReLU()
        )

        # Branch 4: 1x1 Conv -> 7x7 Conv
        self.branch7x7 = nn.Sequential(
            nn.Conv1d(in_channels, internal_channels, kernel_size=1),
            nn.PReLU(),
            nn.Conv1d(internal_channels, internal_channels, kernel_size=7, padding=3),
            nn.PReLU()
        )
        
        # Branch 5: MaxPool -> 1x1 Conv
        self.branch_pool = nn.Sequential(
            nn.MaxPool1d(kernel_size=3, stride=1, padding=1),
            nn.Conv1d(in_channels, internal_channels, kernel_size=1),
            nn.PReLU()
        )

        # Reduce channel dimensions back to 1
        self.output_conv = nn.Conv1d(internal_channels * NB_BRANCHES, in_channels, kernel_size=1)
        self.final_prelu = nn.PReLU()

    def forward(self, x):
        branch1 = self.branch1x1(x)
        branch2 = self.branch3x3(x)
        branch3 = self.branch5x5(x)
        branch4 = self.branch7x7(x)
        branch5 = self.branch_pool(x)
        
        # Concatenate along the channel dimension
        outputs = torch.cat([branch1, branch2, branch3, branch4, branch5], 1)
        #outputs = torch.cat([branch1, branch2, branch3], 1)
        
        # Reduce the channels back to the original `in_channels`
        outputs = self.output_conv(outputs)
        outputs = self.final_prelu(outputs)
        return outputs

class CustomInception1DModel(nn.Module):
    def __init__(self):#, in_channels=1, internal_channels=None):
        super(CustomInception1DModel, self).__init__()

        '''
        self.inception1 = Inception1DBlock(in_channels, internal_channels)
        self.inception2 = Inception1DBlock(in_channels, internal_channels)
        self.inception3 = Inception1DBlock(in_channels, internal_channels)
        '''

        
        self.inception1 = Inception1DBlock(1, 8)
        self.inception2 = Inception1DBlock(1, 8)
        self.inception3 = Inception1DBlock(1, 8)
        self.inception4 = Inception1DBlock(1, 12)
        self.inception5 = Inception1DBlock(1, 16)
        #self.inception6 = Inception1DBlock(in_channels, 8)
        #self.inception7 = Inception1DBlock(in_channels, 9)
        #self.inception8 = Inception1DBlock(in_channels, 10)
    def forward(self, x):
        x = self.inception1(x)
        x = self.inception2(x)
        x = self.inception3(x)
        x = self.inception4(x)
        x = self.inception5(x)
        #x = self.inception6(x)
        #x = self.inception7(x)
        #x = self.inception8(x)
        return x




class kspace_quality_metric_cnn_withDenseBlock(nn.Module):
    def __init__(self, num_classes, nb_kernels=[8, 16, 24], kernelSize = [5,3,3],  nb_neurons_dense = [16,32], dropoutProb = 0.25):
        super(kspace_quality_metric_cnn_withDenseBlock, self).__init__()

        self.num_classes=num_classes
        self.prelu = nn.PReLU()
        self.relu = nn.ReLU()
        self.maxpool1d = nn.MaxPool1d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)

        self.conv1 = nn.Conv1d(1, nb_kernels[0], kernel_size = kernelSize[0], padding = int( (kernelSize[0]-1)/2 ) )
        self.conv2 = nn.Conv1d(nb_kernels[0], nb_kernels[1], kernel_size = kernelSize[1],  padding= int( (kernelSize[1]-1)/2 ) )
        self.conv3 = nn.Conv1d(nb_kernels[1], nb_kernels[2], kernel_size = kernelSize[2],  padding= int( (kernelSize[2]-1)/2 ) )

        #in = 32 because i apply maxpool 3 times in forward and input is 256
        self.lin1  = nn.Linear(in_features = 32*nb_kernels[2], out_features = nb_neurons_dense[0])
        self.lin2  = nn.Linear(in_features = nb_neurons_dense[0], out_features = nb_neurons_dense[1])
        self.lin3  = nn.Linear(in_features = nb_neurons_dense[1], out_features = num_classes)

        self.dropout = nn.Dropout(dropoutProb)
        
    def forward(self,x):
        x = self.prelu(self.conv1(x))
        x = self.maxpool1d(x)
        x = self.prelu(self.conv2(x))
        x = self.maxpool1d(x)
        x = self.prelu(self.conv3(x))
        
        x = self.maxpool1d(x)
        
        x = x.view(x.shape[0],-1)
        
        x = self.dropout(x)
        
        x = self.prelu(self.lin1(x))
        
        #x = self.dropout(x)
        x = self.prelu(self.lin2(x))
        
        x = self.relu(self.lin3(x))#relu to predic motion amplitude in the PE line bin (~ abs rot angle value)
        
        return x



class kspace_quality_metric_cnn_v2_BN(nn.Module):
    def __init__(self, num_classes, nb_kernels=[4,4,6,6,8,8,8,10,10,12,12,16]):
        super(kspace_quality_metric_cnn_v2_BN, self).__init__()

        pad = 1
        self.prelu = nn.PReLU()

        self.conv1 = nn.Conv1d(1, nb_kernels[0], kernel_size=3, padding=pad)
        self.bn1 = nn.BatchNorm1d(nb_kernels[0])
        self.conv2 = nn.Conv1d(nb_kernels[0], nb_kernels[1], kernel_size=3, padding=pad)
        self.bn2 = nn.BatchNorm1d(nb_kernels[1])
        self.conv3 = nn.Conv1d(nb_kernels[1], nb_kernels[2], kernel_size=3, padding=pad)
        self.bn3 = nn.BatchNorm1d(nb_kernels[2])
        self.conv4 = nn.Conv1d(nb_kernels[2], nb_kernels[3], kernel_size=3, padding=pad)
        self.bn4 = nn.BatchNorm1d(nb_kernels[3])
        self.conv5 = nn.Conv1d(nb_kernels[3], nb_kernels[4], kernel_size=3, padding=pad)
        self.bn5 = nn.BatchNorm1d(nb_kernels[4])
        self.conv6 = nn.Conv1d(nb_kernels[4], nb_kernels[5], kernel_size=3, padding=pad)
        self.bn6 = nn.BatchNorm1d(nb_kernels[5])

        self.conv7 = nn.Conv1d(nb_kernels[5], nb_kernels[6], kernel_size=3, padding=pad)
        self.bn7 = nn.BatchNorm1d(nb_kernels[6])
        self.conv8 = nn.Conv1d(nb_kernels[6], nb_kernels[7], kernel_size=3, padding=pad)
        self.bn8 = nn.BatchNorm1d(nb_kernels[7])
        self.conv9 = nn.Conv1d(nb_kernels[7], nb_kernels[8], kernel_size=3, padding=pad)
        self.bn9 = nn.BatchNorm1d(nb_kernels[8])

        self.conv10 = nn.Conv1d(nb_kernels[8], nb_kernels[9], kernel_size=3, padding=pad)
        self.bn10 = nn.BatchNorm1d(nb_kernels[9])
        self.conv11 = nn.Conv1d(nb_kernels[9], nb_kernels[10], kernel_size=3, padding=pad)
        self.bn11 = nn.BatchNorm1d(nb_kernels[10])
        self.conv12 = nn.Conv1d(nb_kernels[10], nb_kernels[11], kernel_size=3, padding=pad)
        self.bn12 = nn.BatchNorm1d(nb_kernels[11])
        

        self.conv_out = nn.Conv1d(nb_kernels[11], 1, kernel_size=3, padding=pad)

    def forward(self, x):
        x = self.prelu(self.bn1(self.conv1(x)))
        x = self.prelu(self.bn2(self.conv2(x)))
        x = self.prelu(self.bn3(self.conv3(x)))
        x = self.prelu(self.bn4(self.conv4(x)))
        x = self.prelu(self.bn5(self.conv5(x)))
        x = self.prelu(self.bn6(self.conv6(x)))
        x = self.prelu(self.bn7(self.conv7(x)))
        x = self.prelu(self.bn8(self.conv8(x)))
        x = self.prelu(self.bn9(self.conv9(x)))
        x = self.prelu(self.bn10(self.conv10(x)))
        x = self.prelu(self.bn11(self.conv11(x)))
        x = self.prelu(self.bn12(self.conv12(x)))
        # No activation here because nn.CrossEntropyLoss includes softmax
        x = self.conv_out(x)

        return x

    

class kspace_quality_metric_cnn_v2(nn.Module):

    def __init__(self, num_classes, nb_kernels=[2,4,8,16,32,64], kernelSize = None):

        super(kspace_quality_metric_cnn_v2, self).__init__()

        pad=1
        self.prelu = nn.PReLU()

        self.conv1 = nn.Conv1d(1, nb_kernels[0], kernel_size = 3, padding=pad)
        self.conv2 = nn.Conv1d(nb_kernels[0], nb_kernels[1], kernel_size = 3,  padding=pad)
        self.conv3 = nn.Conv1d(nb_kernels[1], nb_kernels[2], kernel_size = 3,  padding=pad)
        self.conv4 = nn.Conv1d(nb_kernels[2], nb_kernels[3], kernel_size = 3,  padding=pad)
        self.conv5 = nn.Conv1d(nb_kernels[3], nb_kernels[4], kernel_size = 3,  padding=pad)
        self.conv6 = nn.Conv1d(nb_kernels[4], nb_kernels[5], kernel_size = 3,  padding=pad)

        self.conv_out = nn.Conv1d(nb_kernels[5], 1, kernel_size=3, padding=pad)

    def forward(self, x):

        x = self.prelu(self.conv1(x))

        x = self.prelu(self.conv2(x))

        x = self.prelu(self.conv3(x))

        x = self.prelu(self.conv4(x))
        x = self.prelu(self.conv5(x))
        x = self.prelu(self.conv6(x))


        #no activation here because will use nn.CrossEntropyLoss
        x = self.conv_out(x)

        return x

    
class kspace_quality_metric_cnn(nn.Module):
    
    def __init__(self, num_classes, nb_kernels=[8,16,32], kernelSize = [5,3,3,3]):
        
        super(kspace_quality_metric_cnn, self).__init__()

        
        self.num_classes=num_classes
        self.prelu = nn.PReLU()
        
        self.conv1 = nn.Conv1d(1, nb_kernels[0], kernel_size = kernelSize[0], padding = int( (kernelSize[0]-1)/2 ) )
        self.conv2 = nn.Conv1d(nb_kernels[0], nb_kernels[1], kernel_size = kernelSize[1],  padding= int( (kernelSize[1]-1)/2 ) )
        self.conv3 = nn.Conv1d(nb_kernels[1], nb_kernels[2], kernel_size = kernelSize[2],  padding= int( (kernelSize[2]-1)/2 ) )

        self.conv_out = nn.Conv1d(nb_kernels[2], 1, kernel_size=kernelSize[3], padding= int( (kernelSize[3]-1)/2 ) )

    def forward(self, x):
        
        x = self.prelu(self.conv1(x))
        
        x = self.prelu(self.conv2(x))
        
        x = self.prelu(self.conv3(x))
        

        #no activation here because will use nn.CrossEntropyLoss
        x = self.conv_out(x)
        
        return x



class kspace_quality_metric_cnn_softmax(nn.Module):
    
    def __init__(self, num_classes, nb_kernels=[8,16,32]):
        
        super(kspace_quality_metric_cnn_softmax, self).__init__()

        pad=1
        self.num_classes=num_classes
        self.prelu = nn.PReLU()
        
        self.conv1 = nn.Conv1d(1, nb_kernels[0], kernel_size = 3, padding=pad)
        self.conv2 = nn.Conv1d(nb_kernels[0], nb_kernels[1], kernel_size = 3,  padding=pad)
        self.conv3 = nn.Conv1d(nb_kernels[1], nb_kernels[2], kernel_size = 3,  padding=pad)

        self.conv_out = nn.Conv1d(nb_kernels[2], 1, kernel_size=1)###, padding=pad)
        self.logsoftmax = nn.LogSoftmax(dim=2)
        
    def forward(self, x):
        
        x = self.prelu(self.conv1(x))
        
        x = self.prelu(self.conv2(x))
        
        x = self.prelu(self.conv3(x))
        
        #to be trained with nn.NLLLoss
        x = self.logsoftmax(self.conv_out(x))
        
        return x.squeeze()




        
'''

class kspace_quality_metric_dnn(nn.Module):
    def __init__(self, inputSize, nb_kernels, nb_neuronsDense):
        #inputSize = 256 in principle i.e. Lp norm of each phase line of a 256x256 k-space
        super(kspace_quality_metric_dnn, self).__init__()

        self.conv1 = nn.Conv2d(1, nb_kernels, kernel_size = (1,3), stride=(1, 1), padding=(0, pad))
        self.conv2 = nn.Conv2d(nb_kernels, nb_kernels, kernel_size = (1,3), stride=(1, 1), padding=(0, pad))
        self.conv3 = nn.Conv2d(nb_kernels, nb_kernels, kernel_size = (1,3), stride=(1, 1), padding=(0, pad))

        #div by 2 because of the maxPool op
        in_nbNeuronsDense = nb_kernels * inputSize/2
        
        self.lin1 = nn.Linear(in_nbNeuronsDense, nb_neuronsDense)
        self.lin2 = nn.Linear(nb_neuronsDense, 2)

        #self.layers = nn.ModuleList()
        
        #self.maxpool2 = nn.MaxPool2d(2)
        self.maxpool2_1d = nn.MaxPool1d(kernel_size=2, stride=2)
        
        
    def forward(self, x):
        x = self.prelu(self.conv1(x))
        x = self.prelu(self.conv2(x))
        x = self.prelu(self.conv3(x))
        
        x = self.maxpool2_1d(x.squeeze())
        x = self.prelu(self.lin(x))
        x = self.lin(x)

        return x
'''
