import numpy as np
import matplotlib
import matplotlib.pyplot as plt
#Agg : to avoid any graphics display if sismik is run from GUI
matplotlib.use('Agg')
from numpy import linalg
from skimage import data
from skimage.color import rgb2gray
import torch
from torch import optim, nn
from torch.nn import functional as F
import sys, os, time
from torchinfo import summary
from torch.utils.data import Dataset, DataLoader
import torchvision
from torchvision import datasets
from torchvision.transforms import ToTensor
import h5py
import pickle
import argparse
import glob

from contextlib import redirect_stdout
from ksp_qm_models import *

from ksp_qm_utils import *
#####################################################


### Custom "type" function for argparse
def parse_kernels(arg_value):
    try:
        kernels = [int(x) for x in arg_value.split(',')]
        return kernels
    except ValueError:
        raise argparse.ArgumentTypeError(f"List of integers expected, got '{arg_value}' instead.")


def k2s(k):
    return k.replace(' ','_').replace(',','_').replace('[','_').replace(']','_')

#because simulated in matlab
def readKspMatfile(f):
    with h5py.File(f, 'r') as file:
        angle = torch.tensor( np.array(file['out']['angle']) )
        PELine = torch.tensor( np.array(file['out']['PELine']) )
        pnorm_vector = torch.tensor( np.array(file['out']['pnorm']) )
        simu_labels = torch.tensor( np.array(file['out']['simu_labels']['theta']) )#angle traj only
    return pnorm_vector, angle, PELine, simu_labels

#with hard labels
def readDataset_hardLabels(datasetPath, empiricalZero=0.3):
    #files = glob.glob(os.path.join(datasetPath, '*.mat'))
    files = glob.glob(os.path.join(datasetPath, '**', '*.mat'), recursive=True)
    #files=files[0:10000]
    #print('files : ', files)
    X = torch.zeros(len(files),1, 256)
    Y = torch.zeros(len(files))
    for idx, f in enumerate(files):
        pnorm_vector, angle, PELine, simu_labels = readKspMatfile(f)
        if torch.abs(angle) <= empiricalZero:
            PELine = 0
        else:
            PELine = PELine.squeeze()
        
        X[idx,0,:] = pnorm_vector.T
        Y[idx] = PELine
    return X,Y



    
def validation(model, X_val, Y_val, mbsz, slossFunc):
    sloss_val_acc = 0.0
    n=0.0
    with torch.no_grad():
        model.eval()
        for x_val, y_val in zip(X_val.split(mbsz), Y_val.split(mbsz)):
            out_val = model(x_val)
            sloss_val_acc += surrogateLossFunc(out_val.squeeze(), y_val.long())
            n+=1
    return sloss_val_acc.item()/n


def saveToLog( dstDir, sloss_train_acc, avg_errorRate_train, sloss_val_acc, avg_errorRate_val):
    
    train_line = f"{sloss_train_acc}, {avg_errorRate_train}\n"
    val_line = f"{sloss_val_acc}, {avg_errorRate_val}\n"
    
    
    train_log_path = os.path.join( dstDir, 'train.txt')
    val_log_path = os.path.join( dstDir, 'val.txt' )
    
    
    with open(train_log_path, 'a') as file:
        file.write(train_line)
        
    
    with open(val_log_path, 'a') as file:
        file.write(val_line)

    print(f"Logged training: {train_line.strip()} | validation: {val_line.strip()}")


def saveModel(model, optimizer, epoch, dstDir, checkpoint_name='checkpoint.pth', accVal = None):
    checkpoint = {
        'nb_epochs_finished': epoch + 1,
        'model_state': model.state_dict(),
        'optimizer_state': optimizer.state_dict(),
        'accuracyVal' : accVal
    }
    torch.save(checkpoint, os.path.join(dstDir, checkpoint_name))



if __name__=='__main__':

    ######################################################################
    
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    
    devName=torch.cuda.get_device_name(device)
    
    print('----------------------------------------------------------------')
    print('------> running on device: ', device, ', device name: ', devName)
    print('----------------------------------------------------------------')
    time.sleep(1.0)
    ######################################################################


    
    parser = argparse.ArgumentParser(description = 'ksp qm small convnet')

    parser.add_argument('--epochs', type = int, default = 100)
    parser.add_argument('--minibatch_size', type = int, default = 32)
    parser.add_argument('--lr', type = float, default = 1e-3)
    parser.add_argument('--L2_reg', type = float, default = 1e-8)#Tikhonov regularization
    parser.add_argument('--nb_kernels', type=parse_kernels, help='A comma-separated list of integers')
    parser.add_argument('--empiricalZero', type=float, default = 0.3)
    
    #parser.add_argument('--nbconvFilters', type=int, default=128)#will set this number as the number of filters for ALL conv layers

    #nb // workers for trainDataloader
    #parser.add_argument('--numworkers_train', type = int, default = 16)
    #parser.add_argument('--numworkers_val', type = int, default = 2)
    #parser.add_argument('--numworkers_test', type = int, default = 2)

    
    #this line adds the args as variables with the name specified after the '--' and the value provided
    #in cmd-line, or with the default value if no cmd-line provided (or with None if no default value)
    args = parser.parse_args()



    trainingSetPath = '/data2/OD/Lp_things/train/'
    validationSetPath = '/data2/OD/Lp_things/val/'
    #testSetPath = '/data2/OD/Lp_things/test/'
    
    ### *******************************************************************
    ### *******************************************************************
    ###  dst dir harcoded for convenience
    dstDir = '/data2/OD/Lp_things/cnn_results/'
    dstDir = os.path.join(dstDir,
                          'model_FCNN_e' + str(args.epochs)
                          + '_lr'+str(args.lr).replace('.','_')
                          + '_nbkrnls' + k2s(str(args.nb_kernels))
                          + '_minib' + str(args.minibatch_size)
                          +'_L2reg' + str(args.L2_reg).replace('.','_')
                          +'_empZ' + str(args.empiricalZero).replace('.','_')
                          +'_model_v2_idxRnd_withBN')
    os.makedirs(dstDir, exist_ok=True)
    ### *******************************************************************
    ### *******************************************************************
    
    print('\n>>>>> argparse got args: ', args)
    
    time.sleep(3.0)#some time to check that args are correct ... still time to ctrl+C
    


    
    model = kspace_quality_metric_cnn_v2_BN(num_classes=256,nb_kernels=args.nb_kernels)
    
    surrogateLossFunc = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr = args.lr, weight_decay=args.L2_reg)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min')

    bestValLossSoFar = torch.inf
    




    input_size =  (args.minibatch_size, 1, 256)
    summary(model, input_size)

    summary_file = os.path.join(dstDir, 'model_summary.txt')

    with open(summary_file, 'w') as f:
        with redirect_stdout(f):
            summary(model, input_size)


    X_train, Y_train = readDataset_hardLabels(trainingSetPath, empiricalZero=args.empiricalZero)
    X_val, Y_val = readDataset_hardLabels(validationSetPath,  empiricalZero=args.empiricalZero)
    #X_test, Y_test = readDataset_hardLabels(testSetPath,  empiricalZero=args.empiricalZero)

    model=model.to(device)
    
    X_train=X_train.to(device)
    Y_train=Y_train.to(device)
    
    X_val = X_val.to(device)
    Y_val = Y_val.to(device)

    #X_test = X_test.to(device)
    #Y_test = Y_test.to(device)


    #'classical' data normalization
    mu, std = X_train.mean(), X_train.std()
    X_train.sub_(mu).div_(std)
    X_val.sub_(mu).div_(std)

    torch.save(mu, os.path.join(dstDir, 'mu.pth'))
    torch.save(std, os.path.join(dstDir, 'std.pth'))

    
    
    print('X_tr.shape = ', X_train.shape, 'Y_tr.shape=',  Y_train.shape)
    print('X_val.shape = ', X_val.shape, 'Y_val.shape=',  Y_val.shape)
    #print('X_test.shape = ', X_test.shape, 'Y_test.shape=',  Y_test.shape)
    
    print('>>>>>>>> Starting training loop ...\n\n\n')
    
    for e in range(args.epochs):
        sloss_train_acc = 0.0
        n=0.0
        
        idx_rnd = torch.randperm(X_train.shape[0])
        X_train = X_train[idx_rnd]
        Y_train = Y_train[idx_rnd]
        print('randomized idx :', idx_rnd)
        
        for x_train, y_train in zip(X_train.split(args.minibatch_size),
                                    Y_train.split(args.minibatch_size)):
            
            #print(x_train)
            #print(y_train)
            #print('x_train.shape=', x_train.shape)
            #print('y_train.shape=', y_train.shape)
            
            
            model.train()
            out_train = model(x_train)
            
            
            #print('out.shape = ', out_train.shape)
            #print('y_train.shape = ', y_train.shape)
            
            
            # ( squeeze because cross-entropy wants shape mbsize x num_classes i.e. mbx256 here
            # but model output is mb x 1 x 256 )
            sloss_train = surrogateLossFunc(out_train.squeeze(), y_train.long())
            
            optimizer.zero_grad()
            sloss_train.backward()
            optimizer.step()

            sloss_train_acc += sloss_train.item()
            n+=1


        avg_errorRate_train = computeModelAccuray( model,  X_train, Y_train , args.minibatch_size)
        avg_errorRate_val = computeModelAccuray( model,  X_val, Y_val , args.minibatch_size)
        #avg_errorRate_test = computeModelAccuray( model,  X_test, Y_test , args.minibatch_size)
        #if e >= 100:
        #    err_cls_val = computeErrorRate_perClass(model, X_val, Y_val, args.minibatch_size)
        #    #err_cls_test = computeErrorRate_perClass(model, X_test, Y_test, args.minibatch_size)
        #    print('err_cls_val = ', err_cls_val)
        #    #print('err cls test = ', err_cls_test)
            
        #precision_per_class_train, recall_per_class_train, overall_precision_train, overall_recall_train, overall_f1_train = \
        #            computeMetrics(model, X_train, Y_train, args.minibatch_size, PE_range=None)
        #precision_per_class_val, recall_per_class_val, overall_precision_val, overall_recall_val, overall_f1_val = \
        #            computeMetrics(model, X_val, Y_val, args.minibatch_size, PE_range=None)
        
        sloss_val_acc = validation(model, X_val, Y_val, args.minibatch_size, surrogateLossFunc)
        sloss_train_acc /= n
        
        print('\n\n', 'Epoch : ', e, ' loss (train) = ',sloss_train_acc, ', loss (val) = ', sloss_val_acc)
        #print(' overall_precision_train = ', overall_precision_train, ', overall_precision_val = ', overall_precision_val)
        print('avg err. rate train = ', avg_errorRate_train.item()*100.0, ' %')
        print('avg err. rate val = ', avg_errorRate_val.item()*100.0, ' %')
        #print('avg err. rate test = ', avg_errorRate_test.item()*100.0, ' %')
        print('\n\n')
        saveToLog(dstDir, sloss_train_acc, avg_errorRate_train.item()*100.0, sloss_val_acc,avg_errorRate_val.item()*100.0)



        if avg_errorRate_val.item() < bestValLossSoFar:
            bestValLossSoFar = avg_errorRate_val.item()
            saveModel(model=model, optimizer=optimizer, epoch=e, dstDir=dstDir, accVal = bestValLossSoFar)
            with torch.no_grad():
                mb_idx = torch.randint(X_val.shape[0],(args.minibatch_size,))
                out_val = model( X_val[mb_idx,:].view(args.minibatch_size, 1, 256) )
                saveRandomMinibatchPredToDisk(x_val = X_val[mb_idx,:].view(args.minibatch_size, 1, 256) , out_val=out_val, y_val=Y_val[mb_idx], dstDir=dstDir)
            
        scheduler.step(sloss_val_acc)
        printCurrentLr(opt=optimizer, dstDir=dstDir)


        
        '''
        #if sloss_val_acc <  bestValLossSoFar :
        if avg_errorRate_val.item() < bestValLossSoFar:
            bestValLossSoFar = avg_errorRate_val.item()
            saveModel(model=model, optimizer=optimizer, epoch=e, dstDir=dstDir)

        scheduler.step(sloss_val_acc)
        '''
