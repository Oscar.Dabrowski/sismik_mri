function [drop_indices, abs_dLp, dLp, dLp_noMask]  = Lp_detect(Lp,params, doFit)
%input args:
% Lp: "Lp" quasi-norm i.e. ||.||_p vector computed from each phase line
% params: a struct of parameters specifying the following params:
% window_size: smoothing window size (to reduce noise in Lp signal)
% idxDC: index (in phase direction) of DC in kspace (e.g. 129 for a 256x256 kspace)
% threshold: to be found manually, can try default: 0.022 (found via ROC curves)
% offsetDC: "security" region where detectability usually fails, default 16 lines (around DC)
% offsetStart: ignore first (higest freq) : default: 4
% offsetEnd: same as prev (symmetric) : default: 4
%
%output args:
% drop_indices: indexes that the algorithm found as signal drops (motion onset)
% abs_dLp: absolute value of discrete derivatives (useful for sanity checks / debug etc.)
% dLp: discrete derivatives of Lp (useful for debug)
%

if ~exist('doFit','var')
    doFit=false;
end

window_size=params.window_size;
idxDC=params.idxDC;
threshold = params.threshold;
offsetDC = params.offsetDC;
offsetStart = params.offsetStart;
offsetEnd = params.offsetEnd ;
if isfield(params, 'diffOrder')
    diffOrder = params.diffOrder ;
else
    diffOrder = 1;
end

filtered_signal = smoothdata(Lp,'movmean',window_size);
dLp = diff(filtered_signal);%discrete derivative of Lp vector
if diffOrder > 1%only up to 2, any val above 2 results in 2nd derivative anyway
    dLp = diff(dLp);%2nd derivative 
end

dLp_noMask = dLp;

dLp(1:offsetStart)=0;
dLp((end-offsetEnd):end)=0;
dLp( (idxDC-offsetDC):(idxDC+offsetDC) ) = 0;%ignore indexes here
abs_dLp = abs(dLp);


if doFit
    [ abs_dLp fitted_curve ] = detrend_custom(abs(dLp_noMask), abs_dLp);
end

%abs_dLp = (abs_dLp-min(abs_dLp(:)))/(max(abs_dLp(:))-min(abs_dLp(:)));

drop_indices = find(abs_dLp > threshold);


end

