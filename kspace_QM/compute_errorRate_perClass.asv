clc;clear all;close all;

%%

dstDir = '~/MRI_DATA/results/Lp_CNN_vs_discreteDerivatives_errorRateOnValSet/'

err_cnn = load('err_files_cnn.mat')
err_files_cnn = err_cnn.err_files_cnn


pe_numbers = extractPENumbers(err_files_cnn);
pe_numbers = cell2mat( cellfun(@(x) str2num(x), pe_numbers, 'UniformOutput', false) )


angles = extractAngles(err_files_cnn)


figHistPE = figure;
histogram(pe_numbers);
xlabel('PE line');ylabel('Frequency')



figHistAngle = figure;
histogram(angles);

xlabel('Rotation angle');ylabel('Frequency')

fig2pdf('histo_PELines',dstDir, figHnd)


%% Helper functions
%GPT generated !

function pe_numbers = extractPENumbers(filePaths)
    % Function to extract PE numbers from a list of file paths
    % filePaths is a cell array of strings

    % Preallocate the output cell array for efficiency
    pe_numbers = cell(size(filePaths));

    % Define the regular expression pattern to find numbers following '_PE'
    pattern = '_PE(\d+)_';

    % Loop through each file path in the cell array
    for i = 1:numel(filePaths)
        % Extract the matching group (digits after '_PE')
        match = regexp(filePaths{i}, pattern, 'tokens');
        
        % If a match is found, retrieve the number, otherwise put empty
        if ~isempty(match)
            pe_numbers{i} = match{1}{1};  % First {1} accesses the first match, second {1} accesses the first token
        else
            pe_numbers{i} = '';  % No match found, return empty string
        end
    end
end



function angles = extractAngles(filePaths)
    % Function to extract angle values from a list of file paths
    % filePaths is a cell array of strings

    % Initialize the output matrix with NaN values to handle any files without angles
    angles = NaN(size(filePaths));

    % Define the regular expression pattern to find floating-point numbers (including negative) between 'angle' and 'deg'
    pattern = 'angle(-?[\d.]+)deg';

    % Loop through each file path in the cell array
    for i = 1:numel(filePaths)
        % Extract the matching group (floating-point number after 'angle')
        match = regexp(filePaths{i}, pattern, 'tokens');

        % If a match is found, convert the string to a number and assign it to the matrix
        if ~isempty(match)
            angles(i) = str2double(match{1}{1});
        end
    end
end