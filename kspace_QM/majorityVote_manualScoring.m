clc;clear all;close all;

%%

dstDir =  '/home/od/MRI_DATA/results/manual_scoring/majorityVotes/'

f=load('~/MRI_DATA/results/manual_scoring/Francois/invivo/motion_scores_Francois_invivo_v2__2024-03-21_105454.mat')
f=f.results;

s=load('~/MRI_DATA/results/manual_scoring/Seb/invivo/motion_scores_Sebastien_2024-03-27_174646.mat')
s=s.results;

o = load('/home/od/MRI_DATA/results/manual_scoring/Oscar/invivo/in_vivo_4cls_85img/motion_scores_Oscar_invivo_2024-03-20_123659.mat')
o=o.results;


N=length(f)


for i=1:N

    assert( strcmpi(f(i).filename, s(i).filename) && strcmpi(s(i).filename, o(i).filename) )

    majorityVote_results(i).filename =  f(i).filename %take any of f,s, or o since the filenames are the same in each f,s,o for a given i

    votes = [f(i).score s(i).score o(i).score ]
    %check speical case where all 3 scores are different
    if (votes(1) ~= votes(2)) && (votes(2) ~= votes(3))
        majVote = sort(votes);
        majVote = majVote(2);%take score in between
    else
        majVote = mode(votes);
    end

    majorityVote_results(i).score = majVote
end


save( fullfile(dstDir, 'majorityVote_results_invivo_85files.mat') ,'majorityVote_results')