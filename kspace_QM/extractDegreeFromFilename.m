function degree = extractDegreeFromFilename(filename)
    % This function extracts the degree from the filename where the degree might be
    % a whole number or a fractional number coded with an underscore.

    % Define the regular expression pattern to match 'angle' followed by digits possibly separated by an underscore, then 'deg'
    pattern = 'angle(\d+(_\d+)?)deg';

    % Use regexp to find matches
    tokens = regexp(filename, pattern, 'tokens');

    % Check if any matches were found
    if isempty(tokens)
        error('No degree information found in the filename.');
    end

    % Extract the first match which is the degree value, replace underscore with decimal point
    degreeStr = strrep(tokens{1}{1}, '_', '.');

    % Convert the modified string to a number
    degree = str2double(degreeStr);
end