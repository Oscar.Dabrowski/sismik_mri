addpath('../../utils/')
addpath('../../kspace_QM/')

%to compute ROC curves, call something like:

simuDir = '/home/od/DATA/Lp_simu_latest/';
numThresh = 1000;
empiricalZero = 0.31;

computeLpROC(simuDir,numThresh, empiricalZero )