function detectionResults = computeLpDetectionResults(data, files, detectionParams)
%ex pattern : 'theta3_5' or 'theta0_9' or 'theta4' etc. ...

empiricalZero = detectionParams.empiricalZero
nb_LpNorms = length( detectionParams.p)

N=length(files);
tolerance = 2;

TP_files(N) = struct('TP',[]);
TN_files(N) = struct('TN',[]);
FP_files(N) = struct('FP',[]);
FN_files(N) = struct('FN',[]);

abs_dLp_files = cell(1,N);

parfor i=1:N %files %parfor
    fprintf('processing file#%d/%d \n',i,N);
    x = data{i};
    
    theta = x.theta;
    
%     if isfield(x,'effectiveTheta')
%         theta = x.effectiveTheta;
%     elseif isfield(x,'maxEffectiveTheta')
%         theta = x.maxEffectiveTheta;
%     else
%         disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
%         error('>>>>>>>>>>> MISSING FIELDS : effectiveTheta or maxEffectiveTheta <<<<<<<<<<<');
%     end
    
    PE_GT = x.PE_line;
    TP = zeros(1, nb_LpNorms);
    TN = zeros(1, nb_LpNorms);
    FP = zeros(1, nb_LpNorms);
    FN = zeros(1, nb_LpNorms);
    abs_dLp_ = zeros(nb_LpNorms,255);
    
    for pNorm_idx = 1:nb_LpNorms
        Lp_norm = x.Lp_all_list(pNorm_idx);
        Lp_norm = Lp_norm.Lp_norm;
        [dropIdx, abs_dLp, dLp]  = Lp_detect(Lp_norm,detectionParams);
        lowestPEdrop = find( abs_dLp == max(abs_dLp),1 );
        abs_dLp_maxVal =  max(abs_dLp);
        
        if (abs(theta) <= empiricalZero) &&  (abs_dLp_maxVal > detectionParams.threshold)
            FP(pNorm_idx) = 1;
        end
        
        if (abs(theta) <= empiricalZero) &&  (abs_dLp_maxVal < detectionParams.threshold)
            TN(pNorm_idx) = 1;
        end
        
        if (abs(theta) > empiricalZero) && (abs_dLp_maxVal < detectionParams.threshold)
            FN(pNorm_idx) = 1;
        end
        
        if (abs(theta) > empiricalZero) && (abs_dLp_maxVal > detectionParams.threshold)
            if (PE_GT-tolerance)<= lowestPEdrop <=  (PE_GT+tolerance)
                TP(pNorm_idx) = 1;
            else
                FP(pNorm_idx) = 1;
            end
        end
        
%         dropIdx_{pNorm_idx} = dropIdx;
%         abs_dLp_{pNorm_idx} =  [dropIdx, abs_dLp, dLp];
%         dLp_{pNorm_idx} = dLp;
        abs_dLp_(pNorm_idx,:) = abs_dLp;
    end
    
    TP_files(i) = struct('TP', TP);
    TN_files(i) = struct('TN', TN);
    FP_files(i) = struct('FP', FP);
    FN_files(i) = struct('FN', FN);
    abs_dLp_files{i} =  abs_dLp_;
end

detectionResults.TP = TP_files;
detectionResults.TN = TN_files;
detectionResults.FP = FP_files;
detectionResults.FN = FN_files;
detectionResults.abs_dLp_files = abs_dLp_files;
end
