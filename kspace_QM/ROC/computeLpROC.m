function detectionResultsCell = computeLpROC(srcDir,numThresholds,empiricalZero, pattern, detectionParams, dstDir)
%COMPUTETPFP Summary of this function goes here
%srcDir : the path to the dir where the .mat files with simulated norms are
%stored.
%detectionParams: a struct of parameters (optional, see below)
%
%   Note: this func can be called with just srcDir as arg since the
%   multiple thresh will be computed anyway. One can pass the
%   detectionParams arg to change the pNorn value for example
%
%ex pattern : 'theta3_5' or 'theta0_9' or 'theta4' etc. ...
%ex. call: computeLpROC('/home/od/DATA/Lp_simu_latest',10, 'theta3_5' )
%
%ex call NO pattern:  computeLpROC('/home/od/DATA/Lp_simu_latest',1000, 0.31 )
%
%typical call:  computeLpROC('/home/od/DATA/Lp_simu_latest',1000, 0.31 )

delete(gcp('nocreate'))
parpool(30);


if ~exist('dstDir','var')
    dstDir = '~/DATA/LpROC/';
    mkdir(dstDir)
end
if ~exist('numThresholds','var')
    numThresholds = 1000;
end

thresholds = linspace(0.01,0.1,numThresholds) 
numThresholds=length(thresholds);

pause(3)

if ~exist('empiricalZero','var')
    empiricalZero = 0.31;
end

%p is now an array of different norms
if ~exist('detectionParams','var')
     detectionParams.p=[ 1/16, 1/8, 1/4, 1/2, 1, 2, 4];
     detectionParams.window_size = 3;
     detectionParams.idxDC = 129;
     detectionParams.threshold = NaN;
     detectionParams.offsetDC = 8;
     detectionParams.offsetStart = 4;
     detectionParams.offsetEnd = 4;
     %important
     detectionParams.empiricalZero = empiricalZero
end

nb_LpNorms = length( detectionParams.p);

if exist('pattern','var')
    files = dir(fullfile(srcDir, [ '*' pattern '*.mat' ]));
else
    files = dir(fullfile(srcDir, '*.mat'));
end
N = length(files);

x = NaN;
y=cell(1,N);
offsetKFreq_detect = 0;


parfor file_idx=1:N
    Lp_all_list = struct();
    x = load(fullfile(srcDir, files(file_idx).name));
    if isfield(x,'effectiveTheta')
        theta = x.effectiveTheta;
    elseif isfield(x,'maxEffectiveTheta')
        theta = x.maxEffectiveTheta;
    else
        disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
        error('>>>>>>>>>>> MISSING FIELDS : effectiveTheta or maxEffectiveTheta <<<<<<<<<<<');
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for p_idx = 1:nb_LpNorms
        %[~, Lp_norm_noOffset_, Lp_norm, Lp_flip ] = computeMDIScore_LpSym_libmri(x.ksp_corrupted_combined_all,detectionParams.p(p_idx),offsetKFreq_detect);
        [Lp_norm, Lp_norm_noOffset, Lp_flip, Lp_score ] = kspace_quasinorms(x.ksp_corrupted_combined_all,detectionParams.p(p_idx))
        Lp_all_list(p_idx).name = ['L_' strrep(num2str( detectionParams.p(p_idx)),'.','_') ]
        Lp_all_list(p_idx).p_norm = detectionParams.p(p_idx)
        Lp_all_list(p_idx).Lp_norm_noOffset =  Lp_norm_noOffset_;
        Lp_all_list(p_idx).Lp_norm = Lp_norm;
        Lp_all_list(p_idx).Lp_flip = Lp_flip;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    PE_GT = x.PE_line;
    
    y{file_idx} = struct('theta',theta,'PE_line',PE_GT,'Lp_all_list',Lp_all_list);
    file_idx
end

for threshIdx=1:numThresholds
    detectionParams.empiricalZero
    currThresh = thresholds(threshIdx);
    %set new detection thresh:
    %change the thresh regardless of the given params, this is for the ROC
    detectionParams.threshold = currThresh;
    detectionResultsCell{threshIdx} = computeLpDetectionResults(y, files, detectionParams);
    fprintf('-------> processing thresh#%d/%d\n',threshIdx,numThresholds);
end


TPR = zeros(1, numThresholds);
FPR = zeros(1, numThresholds);

tmp = detectionResultsCell{1};tmp=tmp.TP;
nbf = length(tmp);
TPR_Lp = NaN;
FPR_Lp = NaN;

for ithresh = 1:numThresholds
    
    ithresh
    for idxLpNorm = 1:nb_LpNorms
        
        detectionResults = detectionResultsCell{ithresh};
        
        TP =  arrayfun(@(x) x.TP(idxLpNorm), detectionResults.TP); %detectionResultsCell{i};
        TN =  arrayfun(@(x) x.TN(idxLpNorm), detectionResults.TN);
        FP =  arrayfun(@(x) x.FP(idxLpNorm), detectionResults.FP);
        FN =  arrayfun(@(x) x.FN(idxLpNorm), detectionResults.FN);
        
        tpr_demoninator = (sum(TP) + sum(FN));
        fpr_denominator = (sum(FP) + sum(TN));
        
        if tpr_demoninator ~= 0
            TPR = sum(TP) / (sum(TP) + sum(FN));  %sensitivity or recall
        else
            TPR=0;
        end
        
        if fpr_denominator ~= 0
            FPR = sum(FP) / (sum(FP) + sum(TN));  % 1 - specificity
        else
            FPR=0;
        end
        
        TPR_Lp(idxLpNorm,ithresh) = TPR %(ithresh);
        FPR_Lp(idxLpNorm,ithresh) = FPR %(ithresh);

    end
end

%size FPR_Lp = 7x3

% they are already ordered
% [FPR, order] = sort(FPR);
% TPR = TPR(order);

% Plot the ROC curve
fig_colors =  { [1 1 0], [0 1 1], [0 1 0], [0 0 1], [0.58, 0, 0.83], [0.93, 0.51, 0.93], [1 0 0]};
% fig_markers = {'o', '+', '^', 'none', 'x', 's', 'd'};
fig_markers = {'none', 'none', 'none', 'none', 'none', 'none', 'none'};
fig_lineStyles = {'-', '--', '-.', '--', '-', '-', '-'};
lw = 2.5;

fig=figure;
for i=1:nb_LpNorms
    
    plot( FPR_Lp(i,:), TPR_Lp(i,:), 'LineWidth', lw,  'Color', fig_colors{i}, 'Marker', fig_markers{i},  'LineStyle', fig_lineStyles{i});

    hold on;
end


hold on;
plot([0, 1], [0, 1], 'r--');
xlabel('False Positive Rate');
ylabel('True Positive Rate');
xticks([0.1:0.1:1])
yticks([0.1:0.1:1])
title('ROC Curve');
grid on;

legend_strings = cell(1, numel(detectionParams.p) + 3);
[legend_strings{:}] = genLegendRat(detectionParams.p);
legend(legend_strings{:});



%% save ROC fig to disk

fn = [ 'ROC_' 'numThresholds_' num2str(numThresholds) '_pNorm' array2str(detectionParams.p) '_nbf' num2str(nbf) '_empiricalZ_' strrep(num2str(empiricalZero),'.','_') ];
%fig2pdf(fn,dstDir,fig);
saveas(fig,fullfile(dstDir,[fn '.png']));
saveas(fig,fullfile(dstDir,[fn '.fig']));


save 

save(fullfile(dstDir,'FPR_Lp'),'FPR_Lp');
save(fullfile(dstDir,'TPR_Lp'),'TPR_Lp');
save(fullfile(dstDir,'detectionResultsCell'),'detectionResultsCell');

end
