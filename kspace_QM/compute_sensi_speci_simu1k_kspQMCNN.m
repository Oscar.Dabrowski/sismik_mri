clc;clear all;close all;

suffix = '_simu_1k_' %simu
addpath('/home/od/git/sismik_mri/kspace_QM')
addpath('/home/od/git/sismik_mri/utils')

%%
empiricalZero = 0.3;

pyTorchCNNDir = '/home/od/git/sismik_mri/kspace_QM/DNN/'

% different weighting type
noWeighting = ones(1,256);
wDC = kspWeighting( 'DC');

doFit = true

expertName = 'majorityVote_SIMU_1k_'
%resDir = '/home/od/MRI_DATA/results/manual_scoring/Seb/invivo/'%%%%%%%/home/od/MRI_DATA/results/manual_scoring/Francois/invivo/' %'/home/od/MRI_DATA/results/manual_scoring/Oscar/invivo/in_vivo_4cls_85img/v2/'%%%%'/home/od/MRI_DATA/results/manual_scoring/Francois/bicubic_1k_4cls/' %'/home/od/MRI_DATA/results/manual_scoring/Oscar/bicubic_1k_4cls/'
resDir = '/home/od/MRI_DATA/results/manual_scoring/majorityVotes/'
% f = fullfile(resDir,'Oscar_results_manualScoringWeighted.mat' )

f =   fullfile(resDir,'majorityVote_results_simu_1k.mat')%fullfile(resDir, 'motion_scores_Francois_invivo_v2__2024-03-21_105454.mat')%fullfile(resDir, 'motion_scores_Oscar_invivo_v2_2024-03-20_141851.mat')%%%%%% fullfile(resDir,'motion_scores_Francois_1k__2024-03-19_102940.mat' )

%load manual scores (perceptual) made by human expert
%in the "score" field
data=load(f);
data= data.majorityVote_results%data.data; %fields: filename, score

allFiles = { data.filename }

TP = 0;
FN = 0;
FP = 0;
TN = 0;

for i=1:length(allFiles)
    currentFile = data(i).filename;
    currentScore = data(i).score;
    
    degree = extractDegreeFromFilename(currentFile)
    isMo = degree > empiricalZero
    
    x=load(currentFile);
    x=x.out;
    ksp=x.ksp_corrupted_combined_all_nearest;
    ksp = rearrangeKsp(ksp);
    [ score_wDC, ~ ] = ksp_scoring_latest_CNN(ksp, wDC, pyTorchCNNDir, i);

    curr_TP = (score_wDC > 0) && isMo;
    curr_FN = (score_wDC < 0) && isMo;
    curr_FP = (score_wDC > 0) && ~isMo;
    curr_TN = (score_wDC < 0) && ~isMo;

    TP = TP + double(curr_TP);
    FN = FN + double(curr_FN);
    FP = FP + double(curr_FP);
    TN = TN + double(curr_TN);
end

