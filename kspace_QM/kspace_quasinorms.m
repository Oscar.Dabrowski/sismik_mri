function [Lp_norm, Lp_norm_noOffset, Lp_flip, Lp_score ] = kspace_quasinorms(ksp,p, offsetDC)
%input args:
% ksp: 3D (complex) k-space tensor of shape: rows x cols x slices (this
% is expected to be coil-combined slices from coil sensi profiles)
% p : p quasinorm value (default: p=0.5)
%outputs: [Lp_norm, Lp_norm_noOffset, Lp_flip, Lp_score ]
%  Lp_norm: vector of p-quasinorms foreach phase line
%  Lp_norm_noOffset: same but without the safety offset around DC
%  Lp_flip: flipped version to compute 'hermitian symmetry' score
%  Lp_score: quality score based on Hermitian symmetry (this is not the
%  recommended one, since Hermitian symmetry is not good enough in
%  practice, in general)

if nargin == 1
    p=0.5;
end

%expecting phase direction to be columns
phaseDir = 2;

nb_phaseLines = size(ksp,phaseDir);

%some custom offsets to avoid problems too close
%or too far from DC
if ~exist('offsetDC','var')
    offsetDC=1;%was 5
end
offsetStart=4;

%nb offsets to try (possibly can use a smaller range)
offsets = [-4:0.1:4];

%% the quasinorm formula
Lp_norm = -vecnorm(log(mean(abs(ksp),3)),p);
%%

Lp_norm_noOffset = Lp_norm;
%normalize between 0 and 1
Lp_norm = (Lp_norm-min(Lp_norm(:)))/(max(Lp_norm(:)-min(Lp_norm(:)))); 
idx_max = find(Lp_norm == max(Lp_norm));%DC index

v=nan(length(offsets), nb_phaseLines);
sum_of_diffs = nan(length(offsets),1);
i=1;

%% this part ('offsets') is only needed for the Hermitian symmetry score 
for offset=offsets
    
    Lp_norm(idx_max-offsetDC:idx_max+offsetDC) = 0;
    
    [vflipDCOffset, ~]  = doVflipWithOffset(Lp_norm, offset, 0);
    %store them for display, debug etc...
    v(i,:) = vflipDCOffset;
    
    %% filter first few elements possibly badly aligned
    abs_diff = abs(Lp_norm-vflipDCOffset);
    abs_diff(1:offsetStart)=0;
    abs_diff(end-offsetStart:end) = 0;
    %%
    
    sum_of_diffs(i) = sum(abs_diff);
    
    i=i+1;
end

idx = find(sum_of_diffs == min(sum_of_diffs));
Lp_score = sum_of_diffs(idx);
Lp_flip = v(idx,:);
end

