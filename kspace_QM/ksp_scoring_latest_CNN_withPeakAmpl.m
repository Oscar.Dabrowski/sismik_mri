function  [score, pnorm] = ksp_scoring_latest_CNN_withPeakAmpl(ksp, w, pyTorchCNNDir, idx)
    %ksp: complex multislice coil-combined k-space
    %w: k-space weighting (to be applied to Lp norm of ksp magnitude)

    peakOffset = 1;
    %detectionParams = loadDefaultDetParams();
    p=0.5;
    m  = mean(abs(ksp),3);
    pnorm = log(vecnorm(m,p));%IMPORTANT: NO minus sign ! CNN was trained with log(vecnorm(...)) not -log(vecnorm(...
    pnorm = (pnorm-min(pnorm)) / (max(pnorm)-min(pnorm));
    %becuase of concurrency in parfors outside when calling this func
    pnorm_tmp_file = ['pnorm' num2str(idx) '.mat'];
    save( pnorm_tmp_file,'pnorm', '-v7.3')

    [status, cmdout] = system([ 'python3 ' fullfile(pyTorchCNNDir, ['runTorchCNN_ksp_qm_pnormFileOnly.py ' pnorm_tmp_file] ) ]);
    cnn_result = jsondecode(cmdout);
    %disp(['Received data: ', num2str(cnn_result.data)]);
    cnn_PELine = cnn_result.data;
    
     
    if cnn_PELine > 0
        idxStart = cnn_PELine - peakOffset;
        if idxStart <= 0
            idxStart = 1;
        end
        idxEnd = cnn_PELine + peakOffset;
        pnormPeak_neighborhood = pnorm(idxStart:idxEnd);
        score = w(cnn_PELine) * sum(pnormPeak_neighborhood);
    else
        score = 0;
    end

    delete(pnorm_tmp_file)
    % figure('name',['Class : ' num2str(i)]);
    % subplot(131);
    % plot(pnorm);
    % 
    % subplot(132);
    % plot(abs_dLp);
    % 
    % subplot(133);
    % plot(abs_dLp_filtered.*w);
    % 
    % title( ['score (with detect) =' num2str(score)])
end