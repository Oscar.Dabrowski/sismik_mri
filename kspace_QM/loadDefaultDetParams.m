function detectionParams = loadDefaultDetParams(nbPhaseLines)
% this function returns a struct with the default detection params 
% for a 256x256 k-space (possible to optionnaly specify nbPhaseLines)
    
    if ~exist('nbPhaseLines','var')
        nbPhaseLines=256;
    end
    
    %set params for line detection in k-space
     detectionParams.p=0.5;
     detectionParams.window_size = 3;
     detectionParams.idxDC = floor(nbPhaseLines/2)+1;%129 for 256x256
     detectionParams.threshold = 0.022;
     %can put -1 to include DC (i.e. no 'safety region')
     detectionParams.offsetDC = 8;%was 8
     detectionParams.offsetStart = 4;
     detectionParams.offsetEnd = 4;
     
end

