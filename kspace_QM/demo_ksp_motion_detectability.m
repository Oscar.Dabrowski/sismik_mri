clc;clear all;close all;
addpath('../utils/');
%%

%%read example (in vivo acquisition obtained from a volunteer who went through the 
%%ChoCo protocol to generate 3 motion events at 3 different lines in k-space
%%(PE ~75, PE ~90 and PE ~105)
%%(Note : kc stands for "kspace coil-combined")
%%Note: this one has one miss (FALSE NEGATIVE) for line PE ~89-90

%ksp = getfield( load('../data/chocodb_1/chocodb_3moEvt.mat'), 'kc');


%this example has all lines detected (6 motion events at lines ...)

ksp = getfield( load('../data/chocodb_2/chocodb_6moEvt.mat'), 'kc');


p=0.5;%p-quasinorm
[Lp_norm, Lp_norm_noOffset, Lp_flip, Lp_score ]= kspace_quasinorms(ksp, p);

%set params for line detection in k-space
 detectionParams.p=p;
 detectionParams.window_size = 3;
 detectionParams.idxDC = floor(size(ksp,2))/2+1;%129 for 256x256
 detectionParams.threshold = 0.022;
 detectionParams.offsetDC = 8;
 detectionParams.offsetStart = 4;
 detectionParams.offsetEnd = 4;
 %detectionParams.empiricalZero = 0.3%degrees
[drop_indices, abs_dLp, dLp]  = Lp_detect(Lp_norm,detectionParams);

drop_indices

%% display results
q=0.98;
sliceToDisp=7;
img_magn = normalize01( abs(ifft2(ksp(:,:,sliceToDisp))) );
avg_ksp_magn = normalize01( log(mean(abs(ksp),3)) );

titleFntSz = 14;

fig=figure;
set(fig, 'Position', [100, 100, 900, 800]);

subplot(221)
imagesc(rot90(img_magn,2),[0 quantile(img_magn(:),q)]);
colormap('gray');
title(['Corrupted slice (#' num2str(sliceToDisp) ')'],'fontsize',titleFntSz);
%pbaspect([1 1 1]);

subplot(222)
imagesc( avg_ksp_magn, [0 quantile(avg_ksp_magn(:),q)]);
colormap('gray');
title('Avg kspace (all slices)','fontsize',titleFntSz);
%pbaspect([1 1 1]);

subplot(223);
plot(Lp_norm,'linewidth',2);
%overlay the detected drop indexes
hold on;
plot(drop_indices, Lp_norm(drop_indices),'r+','linewidth',1.5');
legend('p-quasinorm','detected lines');
grid on;
xlim([0 length(Lp_norm)]);
title('p-quasinorm of each PE line','fontsize',titleFntSz);

subplot(224);
plot(abs_dLp, 'linewidth',1.5);%'quality metric' based on abs of discrete derivatives of the quasinorms
hold on;
plot( [0 length(abs_dLp)],[detectionParams.threshold detectionParams.threshold],'--k');
legend('abs diff metric','detection thresh');
title('ksp quality metric','fontsize',titleFntSz);

%pbaspect([1 1 1]);
grid on;
xlim([0 length(Lp_norm)]);
