
clc;clear all;close all; clear classes; clear java
%%




dstDir = pwd


%% pytorch scripts dir
pyTorchCNNDir = '/home/od/git/sismik_mri/kspace_QM/DNN/'

%to be compared with CNN error rate

% %% python integration stuff - will NEVER work ...X((((((((((((((((((
% if count(py.sys.path, '/home/od/git/sismik_mri/kspace_QM/DNN') == 0
%     insert(py.sys.path, int32(0), '/home/od/git/sismik_mri/kspace_QM/DNN');
% end
% % py.importlib.import_module('ksp_qm_models');
% % py.importlib.import_module('ksp_qm_utils');
% pyCNN_matlab_interface = py.importlib.import_module('pyCNN_matlab_interface');
% modelPath = '/home/od/Lp_things/model_FCNN_v2_e150_lr0_0001_nbkrnls_3__6__9__minib32_L2reg0_0_empZ0_3_model_v1_idxRnd/'
% model = pyCNN_matlab_interface.load_model( modelPath, py.list({3, 6, 9}), 'v1', 'cpu')
%%




empiricalZero = 0.3


addpath('/home/od/git/sismik_mri/kspace_QM/')
addpath('/home/od/git/sismik_mri/utils/')
addpath('/home/od/git/sismik_mri/misc/Lp_things/latest_afterDebug/')

dataDir = '/home/od/Lp_things/DATA/val/'
files = dir(fullfile(dataDir, '**', '*.mat'))

doFit = false;
detectionParams = loadDefaultDetParams();

tol = 4;
error_dLp = 0;
error_cnn = 0;

cnn_cnt = 1;
lp_cnt = 1;

%load
for i=1:length(files)
    
    fprintf( 'Processing %d/%d \n', i, length(files));

    tmp_fullfile = fullfile(files(i).folder, files(i).name)
    tmp=load(tmp_fullfile);
    tmp=tmp.out;

    data(i).pnorm =tmp.pnorm;
    data(i).PELine = tmp.PELine;
    data(i).angle = tmp.angle;
    
    [drop_indices, abs_dLp, dLp, dLp_noMask]  = Lp_detect( data(i).pnorm,detectionParams, doFit);

   %% pytorch neural net
   [status, cmdout] = system([ 'python3 ' fullfile(pyTorchCNNDir, ['runTorchCNN_ksp_qm.py ' tmp_fullfile] ) ]);
   cnn_result = jsondecode(cmdout);
   %disp(['Received data: ', num2str(cnn_result.data)]);
   cnn_PELine = cnn_result.data;

   %%

   if abs(tmp.angle) < empiricalZero
       data(i).PELine = 0;
   end
   
   if isempty(drop_indices)
       drop_indices = 0;
   end
       
   
   if any(drop_indices < (data(i).PELine - tol) | drop_indices > (data(i).PELine + tol))
       error_dLp = error_dLp+1;
       err_files_dLp{lp_cnt} = tmp_fullfile;
       lp_cnt=lp_cnt+1;
   end

   if any(cnn_PELine < (data(i).PELine - tol) | cnn_PELine > (data(i).PELine + tol))
      error_cnn = error_cnn + 1;
      err_files_cnn{cnn_cnt} = tmp_fullfile;
      cnn_cnt=cnn_cnt+1;
   end

   i
end

error_dLp 
error_rate_dLp = error_dLp/length(files)

error_rate_CNN = error_cnn/length(files)


save(fullfile(dstDir, 'err_files_dLp'),'err_files_dLp')
save(fullfile(dstDir, 'err_files_cnn'),'err_files_cnn')

%% write to txt file
% Define the file name
filename = fullfile(dstDir, ['error_rates_ksp_qm_cnn_vs_dLp.txt'] );

% Open the file for writing
fileID = fopen(filename, 'w');

% Check if file was opened successfully
if fileID == -1
    error('File could not be opened.');
end

% Define the format of the data to be written
dataFormat = 'ERROR_RATE_dLP, %f\nERROR_RATE_CNN, %f\n';

% Write the data with labels
fprintf(fileID, dataFormat, error_rate_dLp, error_rate_CNN);

% Close the file
fclose(fileID);

disp(['Data written to ', filename]);