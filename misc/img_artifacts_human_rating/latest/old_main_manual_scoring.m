clc; clear all; close all;
%%

rng('shuffle');

%Scoring categories (degradation categories):
scores = {'No motion (or very small)', 'Medium motion', 'Large motion'};
scoreValues = [0, 1, 2];

dstDir = pwd; %save scores here
simuDir = 'C:\MRIDATA\Lp_things\samples_from5244simu\';
files = dir(fullfile(simuDir,'*.mat'));
relevant_slices = 5:8;
q = 0.98;


%Initialize GUI
uiFig = uifigure('Name', 'Motion Score Selector');
uiFig.WindowState = 'maximized'; % Start GUI in full screen
screenSize = get(0, 'ScreenSize');
btnGroupYPosition = screenSize(4) * 0.1;

%initially disabled
nextBtn = uibutton(uiFig, 'push', 'Text', 'Next', 'Position', [250, btnGroupYPosition - 30, 100, 20], 'Enable', 'off');
nextBtn.ButtonPushedFcn = @(btn, event) set(btn, 'UserData', 1);

quitBtn = uibutton(uiFig, 'push', 'Text', 'Quit', 'Position', [screenSize(3) - 150, btnGroupYPosition - 30, 100, 20], 'ButtonPushedFcn', @(btn,event) quitApplication(uiFig));

btnGroup = uibuttongroup('Parent', uiFig, 'Position', [20, btnGroupYPosition, screenSize(3)-40, 60],  'SelectionChangedFcn', @(btn,event) set(nextBtn, 'Enable', 'on'));
hiddenRadio = uiradiobutton('Parent', btnGroup, 'Text', 'Hidden', 'Position', [0, 0, 10, 10], 'Visible', 'off');

for i = 1:length(scores)
    rb = uiradiobutton(btnGroup, 'Text', scores{i}, 'Position', [20+(i-1)*180, 20, 160, 20], 'Value', i == 1);
end

%custom progress bar
progressBarBg = uipanel('Parent', uiFig, 'Position', [20, 20, screenSize(3)-40, 25], 'BackgroundColor', 'white');
progressBar = uipanel('Parent', progressBarBg, 'Position', [0, 0, 0, 25], 'BackgroundColor', 'blue');
progressLabel = uilabel('Parent', uiFig, 'Position', [screenSize(3)/2 - 50, 5, 100, 20], 'Text', '0/0', 'HorizontalAlignment', 'center');

axPlaceholder = uiaxes('Parent', uiFig, 'Position', [20, btnGroupYPosition + 70, screenSize(3)-40, screenSize(4) - (btnGroupYPosition + 100)], 'Visible', 'off');

%struct for saving scores
results = struct('filename', [], 'score', []);

cnt = 1;
%main loop
idx_rnd = randperm(length(files));
for i = idx_rnd
    clearSelection(hiddenRadio);%'click' on the hiddenRadio btn to clear the others
    nextBtn.Enable = 'off';
    nextBtn.UserData = 0;
    
    f = fullfile(files(i).folder, files(i).name);
    ksp = readSimuFile(f);
    img = abs(ifft2(ksp(:,:,relevant_slices)));
    for j = 1:size(img,3)
        img(:,:,j) = normalize_custom(img(:,:,j), q); 
    end
    
    delete(axPlaceholder.Children); %clear existing axes or image components
    montage(img, 'Size', [1 4], 'Parent', axPlaceholder); %display new montage
    drawnow;

    waitfor(nextBtn, 'Enable', 'on');%wait for click on any radio button (set score)
    waitfor(nextBtn, 'UserData', 1);%wait for click on 'next'
    
    selectedScore = btnGroup.SelectedObject.Text;
    scoreIndex = find(strcmp(scores, selectedScore));
    numericalScore = scoreValues(scoreIndex);
    
    results(i).filename = f;
    results(i).score = numericalScore;
    
    %update custom progress bar and label
    progressFraction = cnt/length(files);
    progressBar.Position(3) = progressFraction*(screenSize(3)-40); 
    progressLabel.Text = sprintf('%d/%d', cnt, length(files));
    cnt = cnt+1;
end

curr_timestamp = generateTimestampForFilename();
%additionnal randomness...
maxVal = 10000;
paddedRandIntStr = generatePaddedRandIntString(maxVal);

% save scores
save(fullfile(dstDir, [ 'motion_scores_' paddedRandIntStr '_' curr_timestamp '.mat'] ), 'results');

%disable all GUI components in the figure
allUIComponents = findall(uiFig, '-property', 'Enable');
for i = 1:length(allUIComponents)
    allUIComponents(i).Enable = 'off';
end

%% 

function quitApplication(fig)
    delete(fig);
end

function ksp = readSimuFile(f)
    s = load(f);
    ksp = getfield(s, 'ksp_corrupted_combined_all'); % typically 256x256xnslices
end

function img = normalize_custom(img, q)
    if exist('q', 'var')
        maxval = quantile(img(:), q);
        img(img > maxval) = maxval;
    end
    img = (img - min(img(:))) / (max(img(:)) - min(img(:)));
end

function clearSelection(hiddenRadio)
    hiddenRadio.Value = true;
end

function timestamp = generateTimestampForFilename()
    formatOut = 'yyyy-mm-dd_HHMMSS';
    timestamp = datestr(now, formatOut); 
end

function paddedRandIntStr = generatePaddedRandIntString(maxVal)
    randInt = randi(maxVal);
    numDigits = numel(num2str(maxVal));
    %creates format string, e.g., '%03d' for maxVal=100
    formatStr = sprintf('%%0%dd', numDigits); 
    paddedRandIntStr = sprintf(formatStr, randInt);
end
