clc;clear all;close all;


% Create the uifigure
uiFig = uifigure('Name', 'Motion Score Selector');
uiFig.WindowState = 'maximized'; % Start GUI in full screen

% Initialize keyPressed variable and a flag to control the loop
keyPressed = [];
keyPressDetected = false; % Flag to detect key press

% Set up WindowKeyPressFcn callback
uiFig.WindowKeyPressFcn = @(src, event) keyPressCallback(src, event);



% Example loop (replace with your actual loop code and logic)
for i = 1:5 % Example loop, replace with idx_rnd or your specific loop condition
    % Your existing loop code here...

    % Wait for key press
    while ~keyPressDetected
        pause(0.01); % Short pause to yield execution and allow UI to process callbacks
    end

    % Use 'keyPressed' variable as needed within your loop
    if ~isempty(keyPressed)
        disp(['Using Key Pressed: ', num2str(keyPressed)]);
        % Your code to handle the key press here
    end
    
    % Reset for the next iteration
    keyPressDetected = false;
    keyPressed = [];
end




%%
% Define the callback function
function keyPressDetected =  keyPressCallback(src, event)
    % Check if the pressed key is 1, 2, or 3 (including numpad keys)
    key = event.Key;
    if any(strcmp(key, {'1', '2', '3', 'numpad1', 'numpad2', 'numpad3'}))
        if startsWith(key, 'numpad')
            keyPressed = str2double(key(end)); % Extract the number for numpad keys
        else
            keyPressed = str2double(key); % Directly convert the key for number row keys
        end
        disp(['Key Pressed: ', num2str(keyPressed)]); % Display the key pressed for debugging
        keyPressDetected = true; % Set the flag to true when key is pressed
    end
end