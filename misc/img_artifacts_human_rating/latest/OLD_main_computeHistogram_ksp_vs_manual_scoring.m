clc;clear all;close all;

%%

addpath('/home/od/git/sismik_mri/kspace_QM/');

%%

% resDir = '/home/od/git/sismik_mri/misc/img_artifacts_human_rating/latest/Francois_final/nearest_interp/';
% results = load(fullfile(resDir, 'motion_scores_Francois_2024-02-14_133748.mat'));

resDir = '/home/od/git/sismik_mri/misc/img_artifacts_human_rating/latest/Oscar/';
results = load(fullfile(resDir, 'motion_scores_Oscar_2024-03-12_175829.mat'));

results=results.results;

%--- > /home/od/git/sismik_mri/kspace_QM/demo_ksp_motion_detectability.m
nbPhaseLines=256;
detectionParams = loadDefaultDetParams(nbPhaseLines)

N = length(results)
scores_small = []
scores_medium = []
scores_bad = []

for i=1:N
    f=load(results(i).filename);
    f=f.data_new;
    manual_score = results(i).score;
    
    %load L_1/2 norm curve
    Lp_0_5 = f.Lp_all_list(1).Lp_norm;
    [drop_indices, abs_dLp, dLp]  = Lp_detect(Lp_0_5,detectionParams);
    switch manual_score
        case 1
            scores_small  = [ scores_small sum(abs_dLp) ];
        case 2
            scores_medium  = [ scores_medium sum(abs_dLp) ];
        case 3
            scores_bad  = [ scores_bad sum(abs_dLp) ];
    end
    i
end


%%

figure; hold on;
histogram(scores_small,'FaceColor',  'b');
histogram(scores_medium, 'FaceColor', 'y');
histogram(scores_bad, 'FaceColor', 'r')


%%

figure;

% Assuming scores_small, scores_medium, and scores_bad are your data vectors
% Create a single data vector and a group vector to indicate the dataset of each score
data = [scores_small'; scores_medium'; scores_bad'];
groups = [repmat({'Small'}, length(scores_small), 1); repmat({'Medium'}, length(scores_medium), 1); repmat({'Bad'}, length(scores_bad), 1)];

% Create a boxplot
boxplot(data, groups)
title('Boxplot of Score Distributions');
ylabel('Scores');




