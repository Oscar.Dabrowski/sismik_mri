
clc; clear all; close all;
%% 

rng('shuffle');

%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% To anyone who reads this comment:
% I (almost) never program GUIs... so this code is horrible, full of 
% global vars and mess because i never really took the time to understand
% the scope of the callback functions in Matlab i guess ...
% ... sorry for this... Also, it was done in a hurry so...
% no time to clean it up.. :'(((((
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global keyPressDetected;
keyPressDetected = false;

global clickOnRadioBtnDetected;
clickOnRadioBtnDetected = false;

global i;
i = 1;

global idx;
idx = NaN;

global screenSize;
screenSize = get(0, 'ScreenSize');

global f;
f = '';


%Scoring categories (degradation categories):
scores = {'No motion (or very small)', 'Medium motion', 'Large motion'};
scoreValues = [1, 2, 3];

dstDir = pwd; %save scores here
simuDir =  '/shared/Lp_simu_latest/' %'/shared/Lp_simu_sample/'%'/shared/Lp_simu_latest/'% '/shared/Lp_simu_sample/' % '/shared/Lp_simu_latest/' %'C:\MRIDATA\Lp_things\samples_from5244simu\';
files = dir(fullfile(simuDir,'*.mat'));
relevant_slices = 5:8;
q = 0.98;


userName = askForUserName();
[results, idx_rnd, output_filename] = loadPrevRes(length(files), userName)

%Initialize GUI
btnWidth =120;
btnHeight =35;

uiFig = uifigure('Name', 'Motion Score Selector');
uiFig.WindowState = 'maximized'; % Start GUI in full screen

btnGroupYPosition = screenSize(4) * 0.1;

offsetRightLeft = 150;
offsetUpDown = 55;

% 'uiFig' is the figure handle
% increaseFontBtn = uibutton(uiFig, 'push', ...
%     'Text', 'Increase Font Size', ...
%     'Position', [250+offsetRightLeft, btnGroupYPosition - offsetUpDown,  btnWidth, btnHeight], ...
%     'ButtonPushedFcn', @(btn,event) increaseFontSize());

% loadSavedMatBtn = uibutton(uiFig, 'push', ...
%     'Text', 'Load...', ...
%     'Position', [250+offsetRightLeft+btnWidth+150, btnGroupYPosition - offsetUpDown,  btnWidth, btnHeight], ...
%     'ButtonPushedFcn', @(btn,event) increaseFontSize(uiFig, 2));

%initially disabled


% nextBtn = uibutton(uiFig, 'push', 'Text', 'Next', 'Position', [250, btnGroupYPosition - offsetUpDown, btnWidth, btnHeight], 'Enable', 'off');
% nextBtn.ButtonPushedFcn = @(btn, event) set(btn, 'UserData', 1);



quitBtn = uibutton(uiFig, 'push', 'Text', 'Quit', 'Position', [screenSize(3) - 150, btnGroupYPosition - offsetUpDown, btnWidth, btnHeight], 'ButtonPushedFcn', @(btn,event) quitApplication(uiFig));

btnGroup = uibuttongroup('Parent', uiFig, 'Position', [20, btnGroupYPosition, screenSize(3)-40, 60]);%,  'SelectionChangedFcn', @(btn,event) radioClickCallback());
btnGroup.SelectionChangedFcn = @(btn, event) radioClickCallback(btnGroup);

hiddenRadio = uiradiobutton('Parent', btnGroup, 'Text', 'Hidden', 'Position', [0, 0, 10, 10], 'Visible', 'off');

for j = 1:length(scores)
    rb = uiradiobutton(btnGroup, 'Text', scores{j}, 'Position', [20+(j-1)*180, 20, 160, 20], 'Value', j == 1, 'Tag', num2str(j));
end

%custom progress bar
global progressBar;
global progressLabel;
progressBarBg = uipanel('Parent', uiFig, 'Position', [20, 20, screenSize(3)-30, 25], 'BackgroundColor', 'white');
progressBar = uipanel('Parent', progressBarBg, 'Position', [0, 0, 0, 25], 'BackgroundColor', 'blue');
progressLabel = uilabel('Parent', uiFig, 'Position', [screenSize(3)/2 - 50, 5, 500, 20], 'Text', '0/0', 'HorizontalAlignment', 'center');

offset3 = screenSize(3)/20;%555;
offset4 =  screenSize(4)/5;%was 1000 ...
axPlaceholder = uiaxes('Parent', uiFig, 'Position', [20, btnGroupYPosition + 70, screenSize(3)-offset3, screenSize(4) - (btnGroupYPosition + offset4)], 'Visible', 'off');

bkBtn = uibutton(uiFig, 'push', 'Text', 'Back', 'Position', [250, btnGroupYPosition - offsetUpDown, btnWidth, btnHeight], 'Enable', 'on',  'ButtonPushedFcn', @(btn,event) bkBtnCallback(axPlaceholder, idx_rnd, files,relevant_slices, q));

uiFig.WindowKeyPressFcn = @(src, event) keyPressCallback(src, event, btnGroup);

keyPressed = [];



%for i = idx_rnd
while i <= length(idx_rnd)

    idx=idx_rnd(i);

    clearSelection(hiddenRadio);%'click' on the hiddenRadio btn to clear the others
    % nextBtn.Enable = 'off';
    % nextBtn.UserData = 0;
    
    % f = fullfile(files(idx).folder, files(idx).name);
    % ksp = readSimuFile(f);
    % img = abs(ifft2(ksp(:,:,relevant_slices)));
    % for j = 1:size(img,3)
    %     img(:,:,j) = normalize_custom(img(:,:,j), q); 
    % end
    % delete(axPlaceholder.Children); %clear existing axes or image components
    % montage(img, 'Size', [1 4], 'Parent', axPlaceholder); %display new montage
    % drawnow;
    
    
    [ ksp, img ] =  mainLoop_code(axPlaceholder, files,relevant_slices, q);

    fprintf('(before wait) i=%d \n, idx = %d \n, f=%s \n', i, idx, f)
    % Wait for key press
    while ~keyPressDetected && ~clickOnRadioBtnDetected
        pause(0.01); % Short pause to yield execution and allow UI to process callbacks
    end


    % Use 'keyPressed' variable as needed within your loop
    if ~isempty(keyPressed) || clickOnRadioBtnDetected
        disp(['Using Key Pressed: ', num2str(keyPressed)]);
        % Your code to handle the key press here
    end
    
    fprintf('(after wait) i=%d \n, idx = %d \n, f=%s \n', i, idx, f)

    % waitfor(nextBtn, 'Enable', 'on');%wait for click on any radio button (set score)
    % waitfor(nextBtn, 'UserData', 1);%wait for click on 'next'
    
    selectedScore = btnGroup.SelectedObject.Text;
    scoreIndex = find(strcmp(scores, selectedScore));
    numericalScore = scoreValues(scoreIndex);
    
    results(idx).filename = f;
    results(idx).score = numericalScore;
    
    disp('results:')
    results(idx)

    fprintf('(after wait) i=%d \n, idx = %d \n, f=%s \n', i, idx, f)
    disp('----------')


    %update custom progress bar and label
    progressFraction = i/length(idx_rnd);
    progressBar.Position(3) = progressFraction*(screenSize(3)-40); 
    progressLabel.Text = sprintf('%d/%d', i, length(idx_rnd));
    
    % save scores
    save(fullfile(dstDir,output_filename ), 'results');

    keyPressDetected = false;
    clickOnRadioBtnDetected = false;
    keyPressed = [];

    i = i+1;

end


%additionnal randomness...
% maxVal = 10000;
% paddedRandIntStr = generatePaddedRandIntString(maxVal);


%disable all GUI components in the figure
allUIComponents = findall(uiFig, '-property', 'Enable');
for i = 1:length(allUIComponents)
    allUIComponents(i).Enable = 'off';
end

%% 

function quitApplication(fig)
    delete(fig);
end

function ksp = readSimuFile(f)
    s = load(f);
    ksp = getfield(s, 'ksp_corrupted_combined_all'); % typically 256x256xnslices
end

function img = normalize_custom(img, q)
    if exist('q', 'var')
        maxval = quantile(img(:), q);
        img(img > maxval) = maxval;
    end
    img = (img - min(img(:))) / (max(img(:)) - min(img(:)));
end

function clearSelection(hiddenRadio)
    hiddenRadio.Value = true;
end

function timestamp = generateTimestampForFilename()
    formatOut = 'yyyy-mm-dd_HHMMSS';
    timestamp = datestr(now, formatOut); 
end

function paddedRandIntStr = generatePaddedRandIntString(maxVal)
    randInt = randi(maxVal);
    numDigits = numel(num2str(maxVal));
    %creates format string, e.g., '%03d' for maxVal=100
    formatStr = sprintf('%%0%dd', numDigits); 
    paddedRandIntStr = sprintf(formatStr, randInt);
end

function increaseFontSize(uiFig, increment)
    % Find all UI components within the figure
    allUIComponents = findall(uiFig, '-property', 'FontSize');
    
    % Loop through each component and increase its font size
    for i = 1:length(allUIComponents)
        currentFontSize = allUIComponents(i).FontSize;
        allUIComponents(i).FontSize = currentFontSize + increment;
    end
end




function userName = askForUserName()
    % Prompt for user's name using input dialog
    prompt = {'Enter your name:'};
    dlgtitle = 'Input Required';
    dims = [1 35];
    definput = {''};
    answer = inputdlg(prompt, dlgtitle, dims, definput);
    
    % Check if user clicked OK or Cancel
    if isempty(answer)
        userName = 'Unknown'; % Default name or handle cancellation as needed
    else
        userName = answer{1}; % User's input
    end
end


% function [results, idx_rnd] =loadPrevRes()
%     [file, path] = uigetfile('*.mat', 'Select a MAT-file');
%     if isequal(file, 0) || isequal(path, 0)
%        disp('User pressed cancel');
%     else
%        fullPath = fullfile(path, file);
%        disp(['User selected ', fullPath]);
%        % Here you can load the file using load(fullPath) and process it as needed
%        % For example, if you just want to store the fullPath in a variable, you can do so
%        % Remember to declare the variable where you store fullPath at a scope accessible
%        % where you need to use it.
%        results=load(fullPath);
%        for res_idx = 1:length(results)
%             if isempty(results(res_idx))
%                 idx_rnd = [idx_rnd res_idx];
%             end
%        end
% 
%     end
% end

function [results, idx_rnd, output_filename] = loadPrevRes(N, userName)
    [file, path] = uigetfile('*.mat', 'Select a MAT-file');
    
    if isequal(file, 0) || isequal(path, 0)
       curr_timestamp = generateTimestampForFilename();
       %struct for saving scores
       results = struct('filename', cell(1,N), 'score', cell(1,N)); 
       %main loop
       idx_rnd = randperm(N);
       %idx_rnd=idx_rnd(1:11)
       output_filename=  [ 'motion_scores_' userName '_' curr_timestamp '.mat'];
       disp('User pressed cancel');
       return; % Exit the function if user cancels
    else
       idx_rnd_tmp = [],
       output_filename=file;
       fullPath = fullfile(path, file);
       disp(['User selected ', fullPath]);
       results = getfield( load(fullPath), 'results');
       for res_idx = 1:length(results)
            if isempty(results(res_idx).filename)
                idx_rnd_tmp = [idx_rnd_tmp res_idx];
            end
       end
       idx_idx_rnd = randperm(length(idx_rnd_tmp));
       idx_rnd=idx_rnd_tmp(idx_idx_rnd);
    end
end

function  radioClickCallback(btnGroup)
    global clickOnRadioBtnDetected;
    clickOnRadioBtnDetected = true;
end


function keyPressDetected  =  keyPressCallback(src, event, btnGroup)
    global keyPressDetected;
    % Check if the pressed key is 1, 2, or 3 (including numpad keys)
    key = event.Key;
    if any(strcmp(key, {'1', '2', '3', 'numpad1', 'numpad2', 'numpad3'}))
        if startsWith(key, 'numpad')
            keyPressed = str2double(key(end)); % Extract the number for numpad keys
        else
            keyPressed = str2double(key); % Directly convert the key for number row keys
        end
        disp(['Key Pressed: ', num2str(keyPressed)]); % Display the key pressed for debugging
         keyPressDetected = true;
        % Here you can add any action to be performed after a key is detected
        % For example, setting a flag or updating a component's UserData
                % Use the Tag to find and select the corresponding radio button
        allButtons = btnGroup.Children;
        for i = 1:length(allButtons)
            if strcmp(allButtons(i).Tag, num2str(keyPressed))
                btnGroup.SelectedObject = allButtons(i);
                break; % Exit loop once the correct button is found and selected
            end
        end
    end
end


function [ ksp, img ] =  mainLoop_code(axPlaceholder, files,relevant_slices, q)
    global idx;
    global f;
    f = fullfile(files(idx).folder, files(idx).name);
    ksp = readSimuFile(f);
    img = abs(ifft2(ksp(:,:,relevant_slices)));
    for j = 1:size(img,3)
        img(:,:,j) = normalize_custom(img(:,:,j), q); 
    end
    
    delete(axPlaceholder.Children); %clear existing axes or image components
    montage(img, 'Size', [1 4], 'Parent', axPlaceholder); %display new montage
    drawnow;

end




function [ ksp, img ] =  bkBtnCallback(axPlaceholder, idx_rnd, files,relevant_slices, q )
    global i;
    global f;
    global idx;
    global screenSize;
    global progressBar;
    global progressLabel;
    if i > 1
        i=i-1;
        disp('>>>>>> back !')
        fprintf('i = %d \n',i)
        idx =  idx_rnd(i);
        [ksp, img ] =  mainLoop_code(axPlaceholder,files,relevant_slices,  q);
        progressFraction = i/length(idx_rnd);
        progressBar.Position(3) = progressFraction*(screenSize(3)-40); 
        progressLabel.Text = sprintf('%d/%d', i, length(idx_rnd));
    else
        fprintf('>>>>> WARNING: "back" is did nothing because loop index NOT > 1 !!! (i=%d)', i);
    end
end
