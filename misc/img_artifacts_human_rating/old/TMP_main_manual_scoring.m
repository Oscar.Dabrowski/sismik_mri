clc;clear all;close all;

%%

%scoring categories (degradation categories):
% No motion (or very small)
% Medium motion
% Large motion

simuDir = 'C:\MRIDATA\Lp_things\samples_from5244simu\';

files = dir(fullfile(simuDir,'*.mat'))

relevant_slices = 5:8;
q=0.98;

for i=1:length(files)
    %code fragment to show which slices are relevant
    %
    % supposed to read first like : 
    f = fullfile(files(i).folder, files(i).name);
    ksp=readSimuFile(f);
    img = abs(ifft2( ksp(:,:,relevant_slices) ));
    for j=1:size(img,3)
       img(:,:,j) = normalize_custom(img(:,:,j), q); 
    end
    figure;
    montage(img, 'Size', [1 4])
end











%% local helper functions
function ksp = readSimuFile(f)
    s = load(f);
    ksp = getfield(s, 'ksp_corrupted_combined_all');%typically 256x256xnslices
end

function img = normalize_custom(img,q)
    if exist('q','var')
        maxval = quantile(img(:), q);
        img(img >= maxval) = maxval;
    end
    img = (img - min( img(:) )) /(max(img(:))-min(img(:)));
end