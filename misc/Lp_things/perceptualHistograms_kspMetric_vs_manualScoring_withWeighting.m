clc;clear all;close all;
%%

  % % dataDir = '/home/od/git/sismik_mri/misc/Lp_things/Francois/'
  % %  score_class = load(fullfile(dataDir, 'Francois_score_class_weightedGauss1_5.mat'));
  % %  score_class=score_class.score_class_weightedGauss1_5;

  % dataDir = '/home/od/git/sismik_mri/misc/Lp_things/Oscar/'
  %  score_class = load(fullfile(dataDir, 'Oscar_score_class_weightedDC.mat'));
  %  %%% _weightedGauss1_5
  %  %_weightedGauss1_5
  %  score_class=score_class.score_class_weightedDC;


    dataDir = '/home/od/git/sismik_mri/misc/Lp_things/Oscar/'
   score_class = load(fullfile(dataDir, 'Oscar_score_class_weightedGauss1_5.mat'));
   %%% _weightedGauss1_5
   %_weightedGauss1_5
   score_class=score_class.score_class_weightedGauss1_5;

class0 = score_class(1,:);class0 = class0(class0>0);
class1 = score_class(2,:);class1 = class1(class1>0);
class2 = score_class(3,:);class2 = class2(class2>0);
class3 = score_class(4,:);class3 = class3(class3>0);


% Assuming class1, class2, and class3 are already defined as per your snippet

% Find the maximum length among the classes
maxLength = max([length(class0), length(class1), length(class2), length(class3)]);

% Pre-allocate a matrix filled with NaNs
combinedData = NaN(maxLength, 4);


combinedData(1:length(class0), 1) = class0;
combinedData(1:length(class1), 2) = class1;
combinedData(1:length(class2), 3) = class2;
combinedData(1:length(class3), 4) = class3;

% Create the boxplot
boxplot(combinedData, 'Labels', {'No motion','Small motion', 'Medium motion', 'Large motion'});
title('Boxplot for Class Scores');
ylabel('K-space quality score');


figure; hold on;
histogram(class0, 'FaceColor','green')%,'FaceAlpha',0.25);
histogram(class1, 'FaceColor','blue')%,'FaceAlpha',0.15);
histogram(class2, 'FaceColor',[1 1 0])%,'FaceAlpha',0.15);
histogram(class3, 'FaceColor', 'red')%,'FaceAlpha',0.15);
xlabel('k-space quality score');ylabel('Frequency')
legend('No motion','Small motion', 'Medium motion', 'Large motion')
