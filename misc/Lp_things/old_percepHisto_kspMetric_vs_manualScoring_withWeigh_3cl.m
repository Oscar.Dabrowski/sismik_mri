clc;clear all;close all;
%%

  dataDir = '/home/od/git/sismik_mri/misc/Lp_things/Francois/'
   % score_class = load(fullfile(dataDir, 'Francois_score_class_weightedGauss1_5.mat'));
   % score_class=score_class.score_class_weightedGauss1_5;
   % 
   % score_class = load(fullfile(dataDir, 'Francois_score_class_raw.mat'));
   % score_class=score_class.score_class_raw;

    score_class = load(fullfile(dataDir, 'Francois_score_class_weightedDC.mat'));
   score_class=score_class.score_class_weightedDC;

  % dataDir = '/home/od/git/sismik_mri/misc/Lp_things/Oscar/'
  %  score_class = load(fullfile(dataDir, 'Oscar_score_class_raw.mat'));
  %  %%% _weightedGauss1_5
  %  %_weightedGauss1_5
  %  score_class=score_class.score_class_raw;

class1 = score_class(1,:);class1 = class1(class1>0);
class2 = score_class(2,:);class2 = class2(class2>0);
class3 = score_class(3,:);class3 = class3(class3>0);



% Assuming class1, class2, and class3 are already defined as per your snippet

% Find the maximum length among the classes
maxLength = max([length(class1), length(class2), length(class3)]);

% Pre-allocate a matrix filled with NaNs
combinedData = NaN(maxLength, 3);

% Fill the matrix with your data, column-wise
combinedData(1:length(class1), 1) = class1;
combinedData(1:length(class2), 2) = class2;
combinedData(1:length(class3), 3) = class3;

% Create the boxplot
boxplot(combinedData, 'Labels', {'Undetectable or very small motion', 'Medium motion', 'large motion'});
title('Boxplot for Class Scores');
ylabel('K-space quality score');


figure; hold on;
histogram(class1, 'FaceColor','blue'); histogram(class2, 'FaceColor',[1 1 0]); histogram(class3, 'FaceColor', 'red');

legend('Undetectable or very small motion', 'Medium motion', 'large motion')
