function [general_fitresult, x, general_gof, ft] = fitLpCurve(data, formula, xmin,xmax)
%FITLPCURVE Summary of this function goes here
%   formula : equation of the function we use as model for fit
%
% TODO: could rather use varargin instead of exist <- TODO 

%hardcoded for the moment <--- TODO
     initial_a = 1;
     initial_b = 0;
     initial_c = 1;
    
    %if either of the bounds was not defined by user...
    if ~exist('xmin','var') || ~exist('xmax','var')
        x = linspace(-3,3,length(data));
        disp('>>>>>> using x range: (-3,3)')
    else
        x=linspace(xmin,xmax,length(data));
    end

    if ~exist('formula','var')
        
        formula = 'a*exp(-((x-b)/c)^2)'
        %formula = 'a * exp(-abs(x-b)/c)';
%         %initial laplacian b at 0
%         initial_a=0.25;
%         initial_b=0;
%         initial_c=0.25;
    end

    %column format needed for Matlab fit function
    if isrow(data)
        data=data.';
    end
    if isrow(x)
        x=x.';
    end
    ft = fittype(formula, 'independent', 'x', 'coefficients', {'a', 'b', 'c'});
    opts = fitoptions(ft);
    opts.StartPoint = [initial_a, initial_b, initial_c];    
    [general_fitresult, general_gof] = fit(x, data, ft, opts);

end

