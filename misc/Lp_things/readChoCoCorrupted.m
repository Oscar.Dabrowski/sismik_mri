clc;clear all;close all;

%a few random corrupted acq to compare...

f = { 'C:\MRIDATA\choco\choco2\chocodb_230530_1400\twix_mat_extracted\chocodb_230530_1400_se03_mo1evt_trigger_TR500ms_TE12ms_3deg_PE75.dat.mat',
      'C:\MRIDATA\choco\choco2\chocodb_230530_1400\twix_mat_extracted\chocodb_230530_1400_se04_mo1evt_trigger_TR500ms_TE12ms_4deg_PE75.dat.mat',
      'C:\MRIDATA\choco\choco2\chocodb_230530_1400\twix_mat_extracted\chocodb_230530_1400_se09_mo2evt_trigger_TR500ms_TE12ms_4deg_PE90.dat.mat',
      'C:\MRIDATA\choco\choco2\chocodb_230427_1930\twix_mat_extracted\s3_target4deg_PE75.mat',
      'C:\MRIDATA\choco\choco2\chocodb_230427_1930\twix_mat_extracted\chocodb_230427_1930_se09_mo2evt_trigger_TR500ms_TE12ms_4deg_PE90.dat.mat',
      'C:\MRIDATA\choco\choco2\chocodb_230530_1400\twix_mat_extracted\chocodb_230530_1400_se04_mo1evt_trigger_TR500ms_TE12ms_4deg_PE75.dat.mat',
      'C:\MRIDATA\choco\choco2\chocodb_230502_1445\twix_mat_extracted\chocodb_230502_1445_se09_mo2evt_trigger_TR500ms_TE12ms_4deg_PE90.dat.mat',
      'C:\MRIDATA\choco\choco2\chocodb_230502_1445\twix_mat_extracted\chocodb_230502_1445_se04_mo1evt_trigger_TR500ms_TE12ms_4deg_PE75.dat.mat'}
  
  data = zeros(256,256,25,length(f));
  for i = 1:length(f)
      tmp = load(f{i})
      data(:,:,:,i) = tmp.kc;
  end
  
  mkdir('C:\MRIDATA\choco\choco2\chocoMotionSamples\')
  save('C:\MRIDATA\choco\choco2\chocoMotionSamples\chocomotion.mat','data');