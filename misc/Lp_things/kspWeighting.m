function w = kspWeighting( wType, sigma)
%KSPWEIGHTING  weighting for kspace lines wrt DC
% wType : weighting type (Gaussian, log, linear...)
% sigma: std : only for Gaussian weighting

DC=129;
switch wType
    case 'log'
        w=log(1:DC);
        w = [w fliplr(w(2:end-1))];
        w(1)=w(end);%avoid 0 weighting of line1 since log(1)=0
    case 'gauss'
        if ~exist('sigma', 'var')
            error('!!! SIGMA undefined !!!');
        end
        mu=0;
        x=linspace(-3,3,256);
        w = (1/sqrt(2*pi*sigma^2))*exp(-(x-mu).^2/(2*sigma^2));
    case 'DC'
        w = [ [1:DC] [(DC-1):-1:2] ]; %up to 2 for 256 points (othersiwse if :1 would be 257)
    otherwise
        error('wType not recognized (kspWeigting)')
end
end

