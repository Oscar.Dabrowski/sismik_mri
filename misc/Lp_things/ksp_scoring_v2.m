function  score = ksp_scoring_v2(ksp, w, i)
    %ksp: complex multislice coil-combined k-space
    %w: k-space weighting (to be applied to Lp norm of ksp magnitude)
    detectionParams = loadDefaultDetParams();
    p=detectionParams.p;
    m  = mean(abs(ksp),3);
    pnorm = -log(vecnorm(m,p));
    score = sum(pnorm.*w);
    % [drop_indices, abs_dLp, dLp]  = Lp_detect(pnorm,detectionParams);
    % w=w(1:(end-1));
    % score = sum(abs(abs_dLp).*w);

    figure('name',['Class : ' num2str(i)]);
    subplot(121);
    plot(pnorm);
    subplot(122);
    plot(pnorm.*w);
    title( ['score (no absdiff) =' num2str(score)])
end