clc;clear all;close all;
%%

srcDir = 'C:\Users\odabr\OneDrive\Documents\FrL_mechanicalTurk\' %'C:\MRIDATA\postdoc\sismik_paper_ieee\manual_scoring\nearest_interp\Francois_final\';

f = dir([srcDir '*.mat'])
ff = fullfile(srcDir, f.name)

data = load(ff);

res = data.results;%struct with fields: filename and score

for i=1:length(res)
    angleVal(i) = extractAngle(res(i).filename);
end

%info : qualityClasses : small := any angle <= 0.3, medium : any <= 2, large : any >2
idx_small = angleVal <= 0.3;
idx_medium = (angleVal > 0.3) & (angleVal <= 2) ;
idx_large =  angleVal > 2;



scores_smallAngles = [res(idx_small).score];
scores_mediumAngles = [res(idx_medium).score];
scores_largeAngles = [res(idx_large).score];


figure;
hold on;
histogram(scores_smallAngles)
histogram(scores_mediumAngles)
histogram(scores_largeAngles)


%%

function angle = extractAngle(filename)
    % Extract the numeric value following 'theta' and before 'deg' with underscore as decimal
    matches = regexp(filename, 'theta(\d+)_?(\d*)deg', 'tokens');
    if ~isempty(matches)
        % Concatenate the numbers, replacing underscore with a decimal point
        numPart = strjoin(matches{1}, '.');
        % Convert to numeric value
        angle = str2double(numPart);
    else
        angle = NaN; % Return NaN if pattern not found
    end
end
