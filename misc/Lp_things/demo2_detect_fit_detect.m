clc;clear all;close all;
%%

addpath('C:\git\sismik_mri\kspace_QM\')

%%

detectionParams = loadDefaultDetParams()

data_nomo = getfield( load('C:\MRIDATA\choco\choco2\chocoNoMo\choconomo.mat') , 'data');
data_mo = getfield( load('C:\MRIDATA\choco\choco2\chocoMotionSamples\chocomotion.mat'), 'data');
size(data_nomo)
size(data_mo)


for subjectId=1:8

    ksp = data_mo(:,:,:,subjectId);

    [drop_indices, abs_dLp_fit, abs_dLp, abs_dLp_old, fitted_curve, x] = detectWithFit(ksp, detectionParams);

    drop_indices

    %%

    figure('name',['Subject#' num2str(subjectId)]);
    plot(abs_dLp_old,'-r','linewidth',2);hold on;
    plot(abs_dLp,'--b', 'linewidth',2)
    legend('absDiffLp before', 'absDiffLp after')

    figure('name',['Subject#' num2str(subjectId)]);
    plot(x,abs_dLp);hold on;
    plot(x, fitted_curve)

    figure('name',['Subject#' num2str(subjectId)]);
    subplot(121);
    plot(abs_dLp_old,'-b','linewidth',2);title('Before normalization');
    xlim([0 length(abs_dLp_fit)]);
    subplot(122);
    plot(abs_dLp_fit,'-b','linewidth',2);title('After normalization');
    xlim([0 length(abs_dLp_fit)]);
end