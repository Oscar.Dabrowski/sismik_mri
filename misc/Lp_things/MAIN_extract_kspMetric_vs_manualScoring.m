clc;clear all;close all;

addpath('/home/od/git/sismik_mri/kspace_QM')
addpath('/home/od/git/sismik_mri/utils')
%%

expertName = 'Francois'
srcDir = '/home/od/git/sismik_mri/misc/img_artifacts_human_rating/latest/Francois_final/bicubic_interp/' %'C:\MRIDATA\postdoc\sismik_paper_ieee\manual_scoring\nearest_interp\Francois_final\';
res_fn = 'motion_scores_Francois_2024-02-28_102908.mat'


% expertName = 'Oscar'
% srcDir = '/home/od/git/sismik_mri/misc/img_artifacts_human_rating/latest/Oscar/'
% res_fn = 'motion_scores_Oscar_2024-03-15_194143.mat'

isOld = true;

% f = dir([srcDir '*.mat'])
ff = fullfile(srcDir, res_fn)

data = load(ff);

res = data.results;%struct with fields: filename and score

num_classes = 3;
score_class_struct = cell(num_classes,1);

%the score_classes matrix : each row is a vector of k-space scores for each perceptual (manual scoreing) score
%10k is too much but we will remove cells with zeros
score_class_raw = zeros(num_classes, 10000);

score_class_weightedDC  = zeros(num_classes, 10000);
score_class_weightedLog =  zeros(num_classes, 10000);
score_class_weightedGauss1_5 =  zeros(num_classes, 10000);
score_class_weightedGauss2 = zeros(num_classes, 10000);
score_class_weightedGauss3 = zeros(num_classes, 10000);

%because Gaussian curve fitting sometimes fails.
doFit = false;

w_DC = kspWeighting( 'DC');
w_log = kspWeighting( 'log');
w_gauss1_5 = kspWeighting( 'gauss', 1.5);
w_gauss2 = kspWeighting( 'gauss', 2);
w_gauss3 = kspWeighting( 'gauss', 3);

for i = 1:length(res)

    fn = res(i).filename
    tmp = load(fn);
   %%%%%%%% ksp = tmp.out.ksp_corrupted_combined_all_nearest;
    %for the 'old' version
        ksp = tmp.data_new.ksp_corrupted_combined_all;

        %no rearr for 'old' version?
    %%%%%%%%ksp=rearrangeKsp(ksp);

   %%%%% curr_PELine = matchPEString(fn);

    % ksp_corrupted_combined_all
    
    %[drop_indices_ksp1, abs_dLp_fit_ksp1, abs_dLp_ksp1, abs_dLp_old_ksp1, fitted_curve_ksp1, x1] = detectWithFit(ksp);

    if isOld
                    row = res(i).score; %each row corresponds to a score: 0, 1, 2 or 3
            col = find(cellfun('isempty',score_class_struct(row,:)),1); % Find the first empty cell in the row
            if isempty(col) % If no empty cell is found, increase the size of the cell array
                col = size(score_class_struct, 2) + 1; %Determine new column index
            end
    else
            row = (res(i).score)+1; %each row corresponds to a score: 0, 1, 2 or 3
            col = find(cellfun('isempty',score_class_struct(row,:)),1); % Find the first empty cell in the row
            if isempty(col) % If no empty cell is found, increase the size of the cell array
                col = size(score_class_struct, 2) + 1; %Determine new column index
            end
    end
    
    

    res(i).filename = fn;
    res(i).score_ksp =  computeKspScore_sumAbsDiff(ksp, doFit)
    res(i).score_ksp_weightedDC = computeKspScore_sumAbsDiff(ksp, doFit, w_DC);
    res(i).score_ksp_weightedLog = computeKspScore_sumAbsDiff(ksp, doFit,w_log);
    res(i).score_ksp_weightedGauss1_5 = computeKspScore_sumAbsDiff(ksp, doFit,w_gauss1_5);
    res(i).score_ksp_weightedGauss2 =computeKspScore_sumAbsDiff(ksp, doFit,w_gauss2);
    res(i).score_ksp_weightedGauss3 =computeKspScore_sumAbsDiff(ksp, doFit,w_gauss3);
    score_class_struct{row, col} = res(i); %also contains filename

    score_class_raw(row,col)= res(i).score_ksp ;

    score_class_weightedDC(row,col)=  res(i).score_ksp_weightedDC ;
    score_class_weightedLog(row,col)=  res(i).score_ksp_weightedLog ;
    score_class_weightedGauss1_5(row,col)=  res(i).score_ksp_weightedGauss1_5 ;
    score_class_weightedGauss2(row,col)=   res(i).score_ksp_weightedGauss2 ;
    score_class_weightedGauss3(row,col)=  res(i).score_ksp_weightedGauss3;

    fprintf('processing file #%d / %d\n', i, length(res));

end

mkdir(expertName)
save(  fullfile( expertName , [ expertName '_results_manualScoringWeighted.mat' ]) , 'res')
save( fullfile( expertName , [ expertName '_score_class_struct.mat' ]) , 'score_class_struct' )
save( fullfile( expertName ,[ expertName '_score_class_raw.mat' ])  , 'score_class_raw')
save( fullfile( expertName ,[ expertName '_score_class_weightedDC.mat' ])  ,  'score_class_weightedDC')
save( fullfile( expertName ,[ expertName '_score_class_weightedLog.mat'])  , 'score_class_weightedLog')
save( fullfile( expertName ,[ expertName '_score_class_weightedGauss1_5.mat' ])  ,  'score_class_weightedGauss1_5')
save( fullfile( expertName ,[ expertName '_score_class_weightedGauss2.mat' ])  ,   'score_class_weightedGauss2')
save( fullfile( expertName ,[ expertName '_score_class_weightedGauss3.mat' ])  ,  'score_class_weightedGauss3')
