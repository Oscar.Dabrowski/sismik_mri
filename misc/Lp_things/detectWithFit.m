function [drop_indices, abs_dLp_fit, abs_dLp, abs_dLp_old, fitted_curve, x] = detectWithFit(ksp, varargin)
%DETECTWITHFIT Summary of this function goes here
%   Detailed explanation goes here
% neighborhood, formula: optional -> default values should be ok
%e.g. ksp was obtained for example by:
%ksp = data_mo(:,:,:,subjectId);

detectionParams = loadDefaultDetParams();
neighborhood = 3;
offset = 3;
formula = 'a*exp(-((x-b)/c)^2)';%gaussian fit;
scalingFactor=150;

% Parse varargin
for i = 1:length(varargin)
    if strcmp(varargin{i}, 'detectionParams') && i+1 <= length(varargin)
        detectionParams = varargin{i+1};
    elseif strcmp(varargin{i}, 'neighborhood') && i+1 <= length(varargin)
        neighborhood = varargin{i+1};
    elseif strcmp(varargin{i}, 'offset') && i+1 <= length(varargin)
        offset = varargin{i+1};
    elseif strcmp(varargin{i}, 'formula') && i+1 <= length(varargin)
        formula = varargin{i+1};
    elseif strcmp(varargin{i}, 'scalingFactor') && i+1 <= length(varargin)
        scalingFactor = varargin{i+1};
    elseif strcmp(varargin{i}, 'doFit') && i+1 <= length(varargin)
        doFit = varargin{i+1};
    end
end

[Lp_norm, Lp_norm_noOffset, Lp_flip, Lp_score ]= kspace_quasinorms(ksp, detectionParams.p);
[drop_indices, abs_dLp, dLp]  = Lp_detect(Lp_norm,detectionParams);

abs_dLp_old = abs_dLp;



if doFit
    for i=1:length(drop_indices)
        idx = drop_indices(i)
        abs_dLp = setToAvgNeighborhood(abs_dLp, idx, neighborhood, offset);
    end

    [general_fitresult, x, general_gof, ft] = fitLpCurve(abs_dLp,formula );

    fitted_curve =  general_fitresult(x);

    if ~isrow(fitted_curve)
        fitted_curve = fitted_curve.';
    end
    if ~isrow(abs_dLp_old)
        abs_dLp_old = abs_dLp_old.';
    end

    abs_dLp_fit = (abs_dLp_old./fitted_curve)/scalingFactor;
else
    x = NaN;
    fitted_curve = NaN;
    abs_dLp_fit = abs_dLp_old.';
end

drop_indices = find(abs_dLp_fit > detectionParams.threshold);
end

