function  score = ksp_scoring_v3(ksp, w, i)
    %ksp: complex multislice coil-combined k-space
    %w: k-space weighting (to be applied to Lp norm of ksp magnitude)
    detectionParams = loadDefaultDetParams();
    p=detectionParams.p;
    m  = mean(abs(ksp),3);
    pnorm = -log(vecnorm(m,p));
    pnorm = (pnorm-min(pnorm)) / (max(pnorm)-min(pnorm));
    [dropIdx, abs_dLp, dLp]  = Lp_detect(pnorm,detectionParams);
    w=w(1:(end-1));
    %'filter' abs diff curve
    allIdx = 1:length(abs_dLp);
    %setdiff:  difference between sets
    noDetectIdx = setdiff(allIdx, dropIdx);%exclude indxs where no peak is detected
    abs_dLp_filtered = abs_dLp;
    abs_dLp_filtered( noDetectIdx ) = 0;%exclude data where there was no peak detection

    score = sum( abs_dLp_filtered .*w);

    figure('name',['Class : ' num2str(i)]);
    subplot(131);
    plot(pnorm);

    subplot(132);
    plot(abs_dLp);

    subplot(133);
    plot(abs_dLp_filtered.*w);
    
    title( ['score (with detect) =' num2str(score)])
end