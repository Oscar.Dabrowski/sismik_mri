clc;clear all;close all;

addpath('/home/od/git/sismik_mri/kspace_QM')
addpath('/home/od/git/sismik_mri/utils')

%%

w_DC = kspWeighting( 'DC');
w_log = kspWeighting( 'log');
w_gauss1_5 = kspWeighting( 'gauss', 1.5);
w_gauss2 = kspWeighting( 'gauss', 2);
w_gauss3 = kspWeighting( 'gauss', 3);

results_Francois = load('/home/od/git/sismik_mri/misc/Lp_things/Francois/Francois_results_manualScoringWeighted.mat');
results_Francois = results_Francois.res;



score1_example = results_Francois(5112);
score1_example2 = results_Francois(3333);
score1_example3 = results_Francois(331);
%
score2_example = results_Francois(1928)
score2_example2 = results_Francois(1423)
score2_example3 =  results_Francois(37);
%
score3_example = results_Francois(512)
score3_example2 = results_Francois(466)
score3_example3 = results_Francois(1232)

%1,1,1,2,2,2,3,3,3
examples = {score1_example, ...
    score1_example2, ...
    score1_example3, ...
    score2_example, ...
    score2_example2, ...
    score2_example3, ...
    score3_example, ...
    score3_example2, ...
    score3_example3 }

cls = [1,1,1,2,2,2,3,3,3]

for i=1:length(examples)
     data = load(examples{i}.filename);
     fld=fields(data);
     data=getfield(data, fld{1});
     %for the 'old' version (FrL, 3 classes) use : ksp_corrupted_combined_all
     ksp = data.ksp_corrupted_combined_all;

    % ksp = data.
     score = ksp_scoring_v3(ksp, w_DC, cls(i))
    
     i

end


%% -> see file ksp_score.m (i put the function there)
% function  score = ksp_score(ksp)
%     detectionParams = loadDefaultDetParams();
%     p=detectionParams.p;
%     m  = mean(abs(ksp),3);
%     pnorm = vecnorm(m,p);
%     [drop_indices, abs_dLp, dLp]  = Lp_detect(pnorm,detectionParams);
% end
