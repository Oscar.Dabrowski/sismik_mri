clc;clear all;close all;
%%

addpath('/home/od/git/sismik_mri/utils/');

%%

srcDir = '/home/od/MRI_DATA/results/manual_scoring/Seb/invivo/'


cls1 = get1stFieldFromStruct( fullfile(srcDir, 'cls1.mat') );
cls2 = get1stFieldFromStruct( fullfile(srcDir, 'cls2.mat') );
cls3 = get1stFieldFromStruct( fullfile(srcDir, 'cls3.mat') );
cls4 = get1stFieldFromStruct( fullfile(srcDir, 'cls4.mat') );


%chk cls 4

fn_cls4 = {cls4.filename};
ksp_scores_cls4 = [cls4.score_wDC];

size(fn_cls4)
size(ksp_scores_cls4)

idx_problematic_cls4 =  ( ksp_scores_cls4 <= 2 )

cls4_problematic_fn = fn_cls4( idx_problematic_cls4  )

i=1;
for fname = cls4_problematic_fn
    idx = find( strcmp( {cls4.filename} , fname{1}));
    ksp_scores(i) = cls4(idx).score_wDC
    [angle(i), peValue(i)] = extractAngleAndPE(fname{1});
    i=i+1
end

%%

[angleGrid, peValueGrid] = meshgrid(...
    linspace(min(angle), max(angle), 100), ...
    linspace(min(peValue), max(peValue), 100));

kspScoresGrid = griddata(angle, peValue, ksp_scores, angleGrid, peValueGrid, 'natural');

figure;
plot(angle, peValue, 'bo');grid on;
xlabel('angle');
ylabel('PE line')
xlim([0 5])
ylim([70 100])

%%

figure;
plot3( angle, peValue, ksp_scores, 'bo');grid on; hold on;
surf(angleGrid, peValueGrid, kspScoresGrid);

xlabel('angle');
ylabel('PE line')
zlabel('k-space quality score')

%% 

figure;
plot3( angle, peValue, ksp_scores, 'bo');grid on; 

xlabel('angle');
ylabel('PE line')
zlabel('k-space quality score')
