%% this script loads choco invivo acqisitions and saves them in another folder
% in the format expected by /home/od/git/sismik_mri/misc/Lp_things/latest_afterDebug/main_compute_weighted_ksp_scores_4cls_latest.m
% REM this is invivo data so no interpolation !! no
% ksp_corrupted_combined_all_bicubic etc -> only 1 combined k-space !!

clc;clear all;close all;

%%

relevant_slices = 5:8;

srcDir = '/home/od/MRI_DATA/choco_invivo/data/'
f=dir( [ srcDir '*.mat'] )
dstDir = '/home/od/MRI_DATA/choco_invivo/data_forManualScoring/'
mkdir(dstDir)

tic;
for i = 1:length(f)
    fn = fullfile(f(i).folder, f(i).name);
    dstFn = f(i).name;

    tmp = load( fn );
    out.filename = fn;
    out.kc_relevant_slices = fftshift(permute( tmp.kc(:,:,relevant_slices) , [2 1 3] ));
    out.kc_all_slices = fftshift(permute( tmp.kc , [2 1 3] ));
    save(fullfile(dstDir, dstFn),'out');
    i
    clear out
end
toc