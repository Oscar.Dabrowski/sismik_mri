
f = '/home/od/MRI_DATA/results/manual_scoring/Oscar/bicubic_1k_4cls/Oscar_results_manualScoringWeighted.mat'
data=load(f);
data=data.res;


% Initialize an empty array to hold the indices of entries to keep
indicesToKeep = [];

% Loop through each entry in data.res
for i = 1:length(data)
    % Check if the filename does NOT contain '_PE125_'
    if ~contains(data(i).filename, '_PE125_')
        % If it doesn't contain '_PE125_', add its index to indicesToKeep
        indicesToKeep = [indicesToKeep, i];
    end
end

% Use the indicesToKeep to filter out the entries
dataFiltered = data(indicesToKeep);

% % Now dataFiltered contains only the entries where filename does not contain '_PE125_'
% data.res = dataFiltered;


% Assuming dataFiltered is your final structure after removals
res = dataFiltered; % Make sure the variable name matches what you need

% Save the updated structure back to the original file
save(f, 'res', '-v7.3');


% data.res contains the following:
% 
% data.res
% 
% ans = 
% 
%   1×1000 struct array with fields:
% 
%     filename
%     score
%     score_ksp
%     score_ksp_weightedDC
%     score_ksp_weightedLog
%     score_ksp_weightedGauss1_5
%     score_ksp_weightedGauss2
%     score_ksp_weightedGauss3
%
% I need you to compltete this Matlab script to remove any entry that
% has a filename that contains the string '_PE125_'
%leave the other alone!! these entries where the filename field contains
%the '_PE125' string must be removed i.e. the data.res will then have a
%lower number of entries, surely you can do that. thanks.