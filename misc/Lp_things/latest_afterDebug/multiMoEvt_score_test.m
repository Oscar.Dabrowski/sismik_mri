clc;clear all;close all;

%%

srcDir_single = '~/MRI_DATA/choco_invivo/data/'
srcDir_multi = '~/MRI_DATA/choco_invivo/multipleMotionEvt/'

wDC = ones(1,256);% kspWeighting( 'DC' );

%PE75 and PE90 4deg

%multi mo evt 

k1 = load(fullfile(srcDir_multi, 'chocodb_230411_1600_se12_4deg_PE75_PE90.mat'));
k2 = load(fullfile(srcDir_multi,'chocodb_230511_1800_se12_mo2evt_trigger_TR500ms_TE12ms_4deg_PE75_PE90.dat.mat'))
k3 = load(fullfile(srcDir_multi,'chocodb_230517_1700_se12_mo2evt_trigger_TR500ms_TE12ms_4deg_PE75_PE90.dat.mat'))
k4 = load(fullfile(srcDir_multi,'chocodb_230519_1545_se12_mo2evt_trigger_TR500ms_TE12ms_4deg_PE75_PE90.dat.mat'))
k_multi=[k1;k2;k3;k4];

for i=1:4
    scoresDC_multi(i) = ksp_scoring_latest(k_multi(i).kc, wDC); 
end

%single mo evt for each subject

k1_chocodb_230411_1600_PE75 =  load(fullfile(srcDir_single, 'chocodb_230411_1600_se04_4deg_PE75.mat'))
k1_chocodb_230411_1600_PE90 =  load(fullfile(srcDir_single,'chocodb_230411_1600_se09_4deg_PE90.mat'))

scoresDC_single_PE75(1) =  ksp_scoring_latest(k1_chocodb_230411_1600_PE75.kc, wDC)
scoresDC_single_PE90(1) =  ksp_scoring_latest(k1_chocodb_230411_1600_PE90.kc, wDC)

k2_chocodb_230511_1800_PE75 =  load(fullfile(srcDir_single, 'chocodb_230511_1800_se04_mo1evt_trigger_TR500ms_TE12ms_4deg_PE75.dat.mat'))
k2_chocodb_230511_1800_PE90 =  load(fullfile(srcDir_single,'chocodb_230511_1800_se09_mo2evt_trigger_TR500ms_TE12ms_4deg_PE90.dat.mat'))

scoresDC_single_PE75(2) =  ksp_scoring_latest(k2_chocodb_230511_1800_PE75.kc, wDC)
scoresDC_single_PE90(2) =  ksp_scoring_latest(k2_chocodb_230511_1800_PE90.kc, wDC)


k3_chocodb_230517_1700_PE75 =  load(fullfile(srcDir_single, 'chocodb_230517_1700_se04_mo1evt_trigger_TR500ms_TE12ms_4deg_PE75.dat.mat'))
k3_chocodb_230517_1700_PE90 =  load(fullfile(srcDir_single,'chocodb_230517_1700_se09_mo2evt_trigger_TR500ms_TE12ms_4deg_PE90.dat.mat'))

scoresDC_single_PE75(3) =  ksp_scoring_latest(k3_chocodb_230517_1700_PE75.kc, wDC)
scoresDC_single_PE90(3) =  ksp_scoring_latest(k3_chocodb_230517_1700_PE90.kc, wDC)

k4_chocodb_230519_1545_PE75 =  load(fullfile(srcDir_single, 'chocodb_230519_1545_se04_mo1evt_trigger_TR500ms_TE12ms_4deg_PE75.dat.mat'))
k4_chocodb_230519_1545_PE90 =  load(fullfile(srcDir_single,'chocodb_230519_1545_se09_mo2evt_trigger_TR500ms_TE12ms_4deg_PE90.dat.mat'))

scoresDC_single_PE75(4) =  ksp_scoring_latest(k4_chocodb_230519_1545_PE75.kc, wDC)
scoresDC_single_PE90(4) =  ksp_scoring_latest(k4_chocodb_230519_1545_PE90.kc, wDC)







% figure;hold on;
% 
% histogram(scoresDC_single_PE75,'FaceColor','blue')
% 
% histogram(scoresDC_single_PE90,'FaceColor','yellow')
% 
% histogram(scoresDC_multi,'FaceColor','red');
