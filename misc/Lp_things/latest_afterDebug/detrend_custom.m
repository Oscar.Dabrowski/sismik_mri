function [y_detrended, y_trend ]  = detrend_custom(y_toFit, y_toDetrend)

x = 1:length(y_toFit);

%gaussian = @(b, x) b(1) * exp(-((x - b(2)).^2 / (2 * b(3)^2)));
laplacian = @(b, x) b(1) * exp(-abs(x - b(2)) / b(3));
initialGuess = [max(y_toFit), median(x), range(x)/4];

%[beta, resnorm, residual] = lsqcurvefit(gaussian, initialGuess, x, y);

options = optimset('Display','off');
[beta, resnorm, residual] = lsqcurvefit(laplacian, initialGuess, x, y_toFit, [], [], options);



% y_trend = gaussian(beta, x);

y_trend = laplacian(beta, x);


y_detrended = y_toDetrend - y_trend;
end

