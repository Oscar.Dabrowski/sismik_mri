

% Define the directory where the files are located
simuDir = '/home/od/MRI_DATA/Lp_simu_bicubic_and_nearest_v2/';

% Get a list of all .mat files in the directory
files = dir([simuDir '*.mat']);

% Loop through the files
for i = 1:length(files)
    % Check if the file name contains '_PE125_'
    if contains(files(i).name, '_PE125_')
        % Construct the full path to the file
        filePath = fullfile(simuDir, files(i).name);
        
        % Remove the file using MATLAB's delete function
        % delete(filePath);
        
        % Alternatively, use the system command to call 'rm'
        command = sprintf('rm "%s"', filePath);
        status = system(command);
        
        % Check if the command was successful
        if status == 0
            fprintf('Deleted: %s\n', files(i).name);
        else
            fprintf('Failed to delete: %s\n', files(i).name);
        end
    end
end