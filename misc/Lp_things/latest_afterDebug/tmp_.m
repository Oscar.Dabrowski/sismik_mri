clc;clear all;close all;
%%

addpath('/home/od/git/sismik_mri/kspace_QM')
addpath('/home/od/git/sismik_mri/utils')


srcDir = '/home/od/MRI_DATA/Lp_simu_bicubic_and_nearest_v2/'
dstDir = '/home/od/MRI_DATA/extracted_kspace_LpDiff_curves/'

doFit=true;
% different weighting type
noWeighting = ones(1,256);
wDC = kspWeighting( 'DC');
wLog = kspWeighting( 'log');
wGauss1_5 = kspWeighting( 'gauss', 1.5);
wGauss2 = kspWeighting( 'gauss', 2);
wGauss3 = kspWeighting( 'gauss', 3);



%file selection to investigate (all simu)

f_chocodb_230530_1400 = {...
    % 'chocodb_230530_1400_simu_angle0deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230530_1400_simu_angle2deg_PE50_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230530_1400_simu_angle3deg_PE50_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230530_1400_simu_angle1deg_PE75_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230530_1400_simu_angle2deg_PE75_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230530_1400_simu_angle3deg_PE75_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230530_1400_simu_angle0_5deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230530_1400_simu_angle1deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230530_1400_simu_angle2deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    'chocodb_230530_1400_simu_angle3deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    }

f_chocodb_230516_1715 = {...
    % 'chocodb_230516_1715_simu_angle0deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230516_1715_simu_angle2deg_PE50_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230516_1715_simu_angle3deg_PE50_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230516_1715_simu_angle1deg_PE75_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230516_1715_simu_angle2deg_PE75_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230516_1715_simu_angle3deg_PE75_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230516_1715_simu_angle0_5deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230516_1715_simu_angle1deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230516_1715_simu_angle2deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    'chocodb_230516_1715_simu_angle3deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    }


f_chocodb_230502_1445 = {...
    % 'chocodb_230502_1445_simu_angle0deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230502_1445_simu_angle2deg_PE50_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230502_1445_simu_angle3deg_PE50_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230502_1445_simu_angle1deg_PE75_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230502_1445_simu_angle2deg_PE75_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230502_1445_simu_angle3deg_PE75_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230502_1445_simu_angle0_5deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230502_1445_simu_angle1deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    % 'chocodb_230502_1445_simu_angle2deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    'chocodb_230502_1445_simu_angle3deg_PE90_speed0_cx_offset35_cy_offset0.mat', ...
    }



for i=1:length( f_chocodb_230530_1400 )
    f=f_chocodb_230530_1400{i}
    [angle, peValue] = extractAngleAndPE(f, '_angle(\d+)deg_' )
    ksp = load(fullfile(srcDir, f));
    ksp=ksp.out;
    ksp=ksp.ksp_corrupted_combined_all_nearest;
    ksp = rearrangeKsp(ksp);
    [score_wDC, abs_dLp_filtered, abs_dLp] = ksp_scoring_latest(ksp, wDC, doFit);
    [m idx] = max(abs_dLp)
    fig=figure;
    plot(abs_dLp, 'b');hold on;plot(idx, m, 'r+','MarkerSize',12);grid on;
    %title(['max at [' num2str(idx) ', ' num2str(m) '] for angle=' num2str(angle) ' and PE=' num2str(peValue) ' for subj. chocodb 230530 1400 ' ] )
    title(sprintf(['max at [%d, %.4f] \n for angle=%d and PE=%d\nfor subj. chocodb 230530 1400'], idx, m, angle, peValue))

    saveas(fig, fullfile(dstDir, ['chocodb_230530_14005_' 'angle' num2str(angle) 'deg' '_PE' num2str(peValue) '.png'] ))
    saveas(fig, fullfile(dstDir, ['chocodb_230530_14005_' 'angle' num2str(angle) 'deg' '_PE' num2str(peValue) '.fig'] ))
end

close all;

pause(3.5);

for i=1:length( f_chocodb_230516_1715 )
    f=f_chocodb_230516_1715{i}
     [angle, peValue] = extractAngleAndPE(f, '_angle(\d+)deg_' )
    ksp = load(fullfile(srcDir, f));
    ksp=ksp.out;
    ksp=ksp.ksp_corrupted_combined_all_nearest;
    ksp = rearrangeKsp(ksp);
    [score_wDC, abs_dLp_filtered, abs_dLp] = ksp_scoring_latest(ksp, wDC, doFit);
    [m idx] = max(abs_dLp)
    fig=figure;
    plot(abs_dLp, 'b');hold on;plot(idx, m, 'r+','MarkerSize',12);grid on;
    %title(['max at [' num2str(idx) ', ' num2str(m) '] for angle=' num2str(angle) ' and PE=' num2str(peValue) ' for subj. chocodb 230530 1400 ' ] )
    title(sprintf(['max at [%d, %.4f] \n for angle=%d and PE=%d\nfor subj. chocodb 230516 1715'], idx, m, angle, peValue))

    saveas(fig, fullfile(dstDir, ['chocodb_230516_1715_' 'angle' num2str(angle) 'deg' '_PE' num2str(peValue) '.png'] ))
    saveas(fig, fullfile(dstDir, ['chocodb_230516_1715_' 'angle' num2str(angle) 'deg' '_PE' num2str(peValue) '.fig'] ))
end

close all;

pause(3.5);

for i=1:length( f_chocodb_230502_1445 )
    f=f_chocodb_230502_1445{i}
     [angle, peValue] = extractAngleAndPE(f, '_angle(\d+)deg_' )
    ksp = load(fullfile(srcDir, f));
    ksp=ksp.out;
    ksp=ksp.ksp_corrupted_combined_all_nearest;
    ksp = rearrangeKsp(ksp);
    [score_wDC, abs_dLp_filtered, abs_dLp] = ksp_scoring_latest(ksp, wDC, doFit);
    [m idx] = max(abs_dLp)
    fig=figure;
    plot(abs_dLp, 'b');hold on;plot(idx, m, 'r+','MarkerSize',12);grid on;
    %title(['max at [' num2str(idx) ', ' num2str(m) '] for angle=' num2str(angle) ' and PE=' num2str(peValue) ' for subj. chocodb 230530 1400 ' ] )
    title(sprintf(['max at [%d, %.4f] \n for angle=%d and PE=%d\nfor subj. chocodb 230502 1445'], idx, m, angle, peValue))

    saveas(fig, fullfile(dstDir, ['chocodb_230502_1445_' 'angle' num2str(angle) 'deg' '_PE' num2str(peValue) '.png'] ))
    saveas(fig, fullfile(dstDir, ['chocodb_230502_1445_' 'angle' num2str(angle) 'deg' '_PE' num2str(peValue) '.fig'] ))
end


%score_wGauss2 = ksp_scoring_latest(ksp, wGauss2, doFit);