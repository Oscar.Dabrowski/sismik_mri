clc;clear all;close all;

suffix = '_simu'
addpath('/home/od/git/sismik_mri/kspace_QM')
addpath('/home/od/git/sismik_mri/utils')

%% ---->>>>> this is the script i wrote after debug (noticing sum of weighted abs diff not working well if no ''detection'' step)
%% version with with 4 classes (manual scoring of 960 images (approx 1k so i write 1k in filenames to shorten)


% different weighting type
noWeighting = ones(1,256);
wDC = kspWeighting( 'DC');
wLog = kspWeighting( 'log');
wGauss1_5 = kspWeighting( 'gauss', 1.5);
wGauss2 = kspWeighting( 'gauss', 2);
wGauss3 = kspWeighting( 'gauss', 3);


resDir = '/home/od/MRI_DATA/results/manual_scoring/Francois/bicubic_1k_4cls/' %'/home/od/MRI_DATA/results/manual_scoring/Oscar/bicubic_1k_4cls/'
% f = fullfile(resDir,'Oscar_results_manualScoringWeighted.mat' )
f =   fullfile(resDir,'motion_scores_Francois_1k__2024-03-19_102940.mat' )

data=load(f);
data= data.results%data.data; %fields: filename, score

dstDir = resDir;


%classes : cls1 = no or very small motion, cls2= medium, cls3=strong

scores = [data.score];

idx_cls1 = (scores==0); 
cls1 = data(idx_cls1);

idx_cls2 = (scores==1);
cls2 = data(idx_cls2);

idx_cls3 = (scores==2);
cls3 = data(idx_cls3);

idx_cls4 = (scores==3);
cls4 = data(idx_cls4);

tic;

%cls1

for i=1:numel(cls1)
    
    x=load(cls1(i).filename);
    x=x.out;
    ksp=x.ksp_corrupted_combined_all_nearest;
    ksp = rearrangeKsp(ksp);

    score_raw = ksp_scoring_latest(ksp, noWeighting);
    score_wDC = ksp_scoring_latest(ksp, wDC);
    score_wGauss1_5 = ksp_scoring_latest(ksp, wGauss1_5);
    score_wGauss2 = ksp_scoring_latest(ksp, wGauss2);
    score_wGauss3 = ksp_scoring_latest(ksp, wGauss3);
    score_wLog = ksp_scoring_latest(ksp, wLog);

    cls1(i).score_raw=score_raw;
    cls1(i).score_wDC=score_wDC;
    cls1(i).score_wGauss1_5=score_wGauss1_5;
    cls1(i).score_wGauss2=score_wGauss2;
    cls1(i).score_wGauss3=score_wGauss3;
    cls1(i).score_wLog=score_wLog;

i
end

%class2
parfor i=1:numel(cls2)
    
    x=load(cls2(i).filename);
    x=x.out;
    ksp=x.ksp_corrupted_combined_all_nearest;
    ksp = rearrangeKsp(ksp);

    score_raw = ksp_scoring_latest(ksp, noWeighting);
    score_wDC = ksp_scoring_latest(ksp, wDC);
    score_wGauss1_5 = ksp_scoring_latest(ksp, wGauss1_5);
    score_wGauss2 = ksp_scoring_latest(ksp, wGauss2);
    score_wGauss3 = ksp_scoring_latest(ksp, wGauss3);
    score_wLog = ksp_scoring_latest(ksp, wLog);

    cls2(i).score_raw=score_raw;
    cls2(i).score_wDC=score_wDC;
    cls2(i).score_wGauss1_5=score_wGauss1_5;
    cls2(i).score_wGauss2=score_wGauss2;
    cls2(i).score_wGauss3=score_wGauss3;
    cls2(i).score_wLog=score_wLog;

i
end

%class3
parfor i=1:numel(cls3)
    
    x=load(cls3(i).filename);
    x=x.out;
    ksp=x.ksp_corrupted_combined_all_nearest;
    ksp = rearrangeKsp(ksp);

    score_raw = ksp_scoring_latest(ksp, noWeighting);
    score_wDC = ksp_scoring_latest(ksp, wDC);
    score_wGauss1_5 = ksp_scoring_latest(ksp, wGauss1_5);
    score_wGauss2 = ksp_scoring_latest(ksp, wGauss2);
    score_wGauss3 = ksp_scoring_latest(ksp, wGauss3);
    score_wLog = ksp_scoring_latest(ksp, wLog);
    
    cls3(i).score_raw=score_raw;
    cls3(i).score_wDC=score_wDC;
    cls3(i).score_wGauss1_5=score_wGauss1_5;
    cls3(i).score_wGauss2=score_wGauss2;
    cls3(i).score_wGauss3=score_wGauss3;
    cls3(i).score_wLog=score_wLog;

i
end


%class4
parfor i=1:numel(cls4)
    
    x=load(cls4(i).filename);
    x=x.out;
    ksp=x.ksp_corrupted_combined_all_nearest;
    ksp = rearrangeKsp(ksp);

    score_raw = ksp_scoring_latest(ksp, noWeighting);
    score_wDC = ksp_scoring_latest(ksp, wDC);
    score_wGauss1_5 = ksp_scoring_latest(ksp, wGauss1_5);
    score_wGauss2 = ksp_scoring_latest(ksp, wGauss2);
    score_wGauss3 = ksp_scoring_latest(ksp, wGauss3);
    score_wLog = ksp_scoring_latest(ksp, wLog);
    
    cls4(i).score_raw=score_raw;
    cls4(i).score_wDC=score_wDC;
    cls4(i).score_wGauss1_5=score_wGauss1_5;
    cls4(i).score_wGauss2=score_wGauss2;
    cls4(i).score_wGauss3=score_wGauss3;
    cls4(i).score_wLog=score_wLog;

i
end

toc

save(fullfile(dstDir,'cls1.mat'), 'cls1');
save(fullfile(dstDir,'cls2.mat'), 'cls2');
save(fullfile(dstDir,'cls3.mat'), 'cls3');
save(fullfile(dstDir,'cls4.mat'), 'cls4');


%%

wTypeStr = '_noWeighting_'
c1=[cls1.score_raw];
c2=[cls2.score_raw];
c3=[cls3.score_raw];
c4=[cls4.score_raw];

% wTypeStr = '_wDC_'
% c1=[cls1.score_wDC];
% c2=[cls2.score_wDC];
% c3=[cls3.score_wDC];
% c4=[cls4.score_wDC];

% wTypeStr = '_gaussWeightingStd2_'
% c1=[cls1.score_wGauss2];
% c2=[cls2.score_wGauss2];
% c3=[cls3.score_wGauss2];
% c4=[cls4.score_wGauss2];
% 
% wTypeStr = '_logWeighting_'
% c1=[cls1.score_wLog];
% c2=[cls2.score_wLog];
% c3=[cls3.score_wLog];
% c4=[cls4.score_wLog];

numCls = 4;
combinedData = NaN(numel(data), numCls);
combinedData(1:length(cls1), 1) = c1;
combinedData(1:length(cls2), 2) = c2;
combinedData(1:length(cls3), 3) = c3;
combinedData(1:length(cls4), 4) = c4;

% boxplots
figBxplt=figure;
boxplot(combinedData, 'Labels', {'No motion', 'Small motion', 'Medium motion', 'Large motion'});
title('Boxplot for Class Scores');
ylabel('K-space quality score');


% histograms
figHisto = figure; hold on;
histogram(c1, 'FaceColor','black' , 'Normalization' , 'probability');%, 'NumBins',15)%,'FaceAlpha',0.15);
histogram(c2, 'FaceColor','blue',   'Normalization' , 'probability');%,'NumBins',15)%,'FaceAlpha',0.25);
histogram(c3, 'FaceColor','yellow' , 'Normalization' , 'probability')%, 'NumBins',15)%,'FaceAlpha',0.15);
histogram(c4, 'FaceColor','red' , 'Normalization' , 'probability')%,'NumBins',15)%,'FaceAlpha',0.15);

xlabel('k-space quality score');ylabel('Frequency')
legend('No motion', 'Small motion', 'Medium motion', 'Large motion')

%% 

saveas(figHisto, fullfile(dstDir, ['histo_OscarManualScoring4cls_1k_latest' wTypeStr '.png']));

saveas(figBxplt, fullfile(dstDir, [ 'boxplots_OscarManualScoring4cls_1k_latest' wTypeStr '.png' ]));