function score = computeKspScore_sumAbsDiff(ksp, doFit, w)
    %COMPUTEKSPSCORE_SUMABSDIFF Summary of this function goes here
    %   input: ksp is a coil combined k-space (multi-slice)
    % weigting vector
    if ~exist('w','var')
        w=ones(1,256);
    end
    
    [drop_indices_ksp, abs_dLp_fit_ksp, abs_dLp_ksp, abs_dLp_old_ksp, fitted_curve_ksp, x] = detectWithFit(ksp, 'doFit',doFit);
    
    abs_dLp = abs(abs_dLp_fit_ksp);

    if ( isrow(abs_dLp) && ~(isrow(w)) ) || (  ~isrow(abs_dLp) && isrow(w) )
        w=w.';
    end
    
    w=w(1:(end-1));%for some reason abs_dLp ended up 255 element insted of 256
    score = sum(abs_dLp  .*w);
    
    
end

