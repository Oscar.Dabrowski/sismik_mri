clc;clear all;close all;
%%

%% Goal of this script:
% read all available no motion (noMo) ChoCo subject's data (25 slices each)
% the convention is that is they have valid noMo acquisitions then there is
%a subfolder 'twix_mat_nomo\ok\'


chocoDir = 'C:\MRIDATA\choco\choco2\'
subDir = 'twix_mat_nomo\ok\'
s = dir([ chocoDir '\chocodb*'])

valueToFind = 'twix_mat_nomo';

for idx=1:length(s)
    fprintf('s = %s\n ', s(idx).name);
    f = fullfile(s(idx).folder, s(idx).name);
    tmp = dir(f);
    tmp_dirs = {tmp.name};
    containsValue(idx) = any(cellfun(@(c) isequal(c, valueToFind), tmp_dirs));    
end

validSubjects = s(containsValue);

nomoData = {};

data = zeros(256,256,25,length(validSubjects));

for i = 1:length(validSubjects)
    d = fullfile(fullfile(chocoDir,validSubjects(i).name),subDir);
    matfile = dir([d '\*.mat']);
    data_tmp = load(fullfile(d,matfile.name)); 
    data(:,:,:,i) = data_tmp.kc;
    i
end

save('C:\MRIDATA\choco\choco2\chocoNoMo\choconomo.mat','data')