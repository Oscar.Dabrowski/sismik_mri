clc;clear all;close all;

addpath('C:\git\sismik_mri\kspace_QM\')

%%

%available fields (example):
%   struct with fields:
% 
%                    Lp_all_list: [1×7 struct]
%                        PE_line: 25
%                 effectiveTheta: 4
%     ksp_corrupted_combined_all: [256×256×17 double]
%          ksp_nomo_combined_all: [256×256×17 double]
%                         labels: [1×1 struct]
%                          speed: 0
dataDir = 'C:\MRIDATA\Lp_things\samples_from5244simu\'

dir(dataDir)


% w = kspWeighting(129);

ksp1 = load(fullfile(dataDir, 'chocodb_230425_1530_2023_05_25_02_27_24_283_speed2_theta4deg_PE75_rndID479097106.mat'));
ksp1=ksp1.ksp_corrupted_combined_all;
size(ksp1)
ksp2 = load(fullfile(dataDir, 'chocodb_230425_1530_2023_05_25_02_57_19_160_speed0_theta4deg_PE25_rndID732749937.mat'));
ksp2=ksp2.ksp_corrupted_combined_all;
size(ksp2)


[drop_indices_ksp1, abs_dLp_fit_ksp1, abs_dLp_ksp1, abs_dLp_old_ksp1, fitted_curve_ksp1, x1] = detectWithFit(ksp1);
[drop_indices_ksp2, abs_dLp_fit_ksp2, abs_dLp_ksp2, abs_dLp_old_ksp2, fitted_curve_ksp2, x2] = detectWithFit(ksp2);

ksp1_abs=abs(ifft2(ksp1(:,:,7)));
ksp2_abs=abs(ifft2(ksp2(:,:,7)));

figure;
subplot(221);
imagesc(ksp1_abs,[0 quantile(ksp1_abs(:),0.99)]);colormap('gray');
subplot(222);
imagesc(ksp2_abs,[0 quantile(ksp2_abs(:),0.99)]);colormap('gray');
subplot(223);
plot(abs_dLp_fit_ksp1);
subplot(224);
plot(abs_dLp_fit_ksp2);
