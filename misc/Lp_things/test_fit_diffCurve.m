clc;clear all;close all;
addpath('C:\git\sismik_mri\kspace_QM\')
%%

%% Goal of this script:
% assess the possibility of fitting and flattening the discrete derivative
% curves of the Lp metric to improve its robustness
%
% We load all noMo ChoCo acquisitions to try to see if e.g. a lorentzian
% or similar can be fitted, which would then be subsequently used on any
% motion corrupted scan to 'normalize' it by ideally an analytic
% representation of the fitted curve

data_nomo = getfield( load('C:\MRIDATA\choco\choco2\chocoNoMo\choconomo.mat') , 'data');
data_mo = getfield( load('C:\MRIDATA\choco\choco2\chocoMotionSamples\chocomotion.mat'), 'data');
size(data_nomo)
size(data_mo)


figure;
for i = 1:size(data_nomo,4)
    ksp = data_nomo(:,:,:,i);

    p=0.5;%p-quasinorm
    %offsetDC = 0;
    [Lp_norm, Lp_norm_noOffset, Lp_flip, Lp_score ]= kspace_quasinorms(ksp, p)%, offsetDC);

    %set params for line detection in k-space
     detectionParams.p=p;
     detectionParams.window_size = 3;
     detectionParams.idxDC = floor(size(ksp,2))/2+1;%129 for 256x256
     detectionParams.threshold = 0.022;
     %can put -1 to include DC (i.e. no 'safety region')
     detectionParams.offsetDC = 8;
     detectionParams.offsetStart = 4;
     detectionParams.offsetEnd = 4;

    [drop_indices, abs_dLp, dLp]  = Lp_detect(Lp_norm,detectionParams);
    
    all_abs_dLp{i} = abs_dLp;
    %xs{i} = 1:255;
    
    subplot(3,4,i);
    plot(abs_dLp)
    title('|dfdx(Lp)| (NOMO, no DC offset)');
    xlim([0 256]);
    ylim([0 0.04]);
end
    

figure;
for i = 1:size(data_mo,4)
    ksp = data_mo(:,:,:,i);

    p=0.5;%p-quasinorm
    %offsetDC = 0;
    [Lp_norm, Lp_norm_noOffset, Lp_flip, Lp_score ]= kspace_quasinorms(ksp, p);%, offsetDC);

    %set params for line detection in k-space
     detectionParams.p=p;
     detectionParams.window_size = 3;
     detectionParams.idxDC = floor(size(ksp,2))/2+1;%129 for 256x256
     detectionParams.threshold = 0.022;
     %can put -1 to include DC (i.e. no 'safety region')
     detectionParams.offsetDC = 8;
     detectionParams.offsetStart = 4;
     detectionParams.offsetEnd = 4;

    [drop_indices, abs_dLp_mo, dLp]  = Lp_detect(Lp_norm,detectionParams);
    all_abs_dLp_mo{i} = abs_dLp_mo;
    subplot(3,4,i);
    plot(abs_dLp_mo)
    title('|dfdx(Lp)| (motion (3-4°, PE75/90), no DC offset)');
    xlim([0 256]);
    ylim([0 0.04]);
end


% Assuming `data` is a cell array of y-vectors and `x` is your common x-vector.
% If the x-vector is different for each set, you would need a cell array for those as well.

% Example:
% data = {y1, y2, ..., y11};
% xs = {x1, x2, ..., x11}; % This would be used if x is different for each dataset

%%

% data_nomo = getfield( load('C:\MRIDATA\choco\choco2\chocoNoMo\choconomo.mat') , 'data');
% data_mo = getfield( load('C:\MRIDATA\choco\choco2\chocoMotionSamples\chocomotion.mat'), 'data');
% size(data_nomo)
% size(data_mo)

x = linspace(-3,3,255);%1:255;
data =  all_abs_dLp;
data_mo = all_abs_dLp_mo;

% Concatenate all data into one vector for fitting
all_y = [];
all_x = [];

for i = 1:numel(data)
    all_y = [all_y; data{i}(:)]; % Stack all y vectors
    % If x is the same for all
    all_x = [all_x; x(:)]; % Repeat or stack x vector accordingly
    
    % If x is different for each dataset
    % all_x = [all_x; xs{i}(:)]; % Stack all x vectors
end

% Perform the general Gaussian fit
ft = fittype('a*exp(-((x-b)/c)^2)', 'independent', 'x', 'coefficients', {'a', 'b', 'c'});
opts = fitoptions(ft);
opts.StartPoint = [initial_a, initial_b, initial_c];
[general_fitresult, general_gof] = fit(all_x, all_y, ft);

% Plot the general fit with all datasets superimposed
figure;
hold on;
for i = 1:numel(data)
    y = data{i}; % Get the i-th dataset
    plot(x, y, 'o'); % Plot the original data points
end
% Plot the general fit
plot(x, general_fitresult(x), 'r-', 'LineWidth', 2);
hold off;

% Annotate the general fit plot
title('General Gaussian Fit to All Datasets');
xlabel('X-axis');
ylabel('Y-axis');
legend('Data Points', 'General Gaussian Fit');

% Display the general fit parameters and goodness of fit
disp('General Fit Parameters:');
disp(general_fitresult);
disp('Goodness of fit for the general fit:');
disp(general_gof);


figure;
for i = 1:length(data)
    subplot(3,4,i);
    plot( (data{i}./general_fitresult(x).')/100);
    xlim([0 256]);
    ylim([0 0.04]);
    title('nomo normalized by gauss fit');
end

figure;
for i = 1:length(data_mo)
    subplot(3,4,i);
    plot( (data_mo{i}./general_fitresult(x).')/100);
    xlim([0 256]);
    ylim([0 0.04]);
    title('mo. normalized by gauss fit');
end