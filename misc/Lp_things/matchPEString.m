function extractedNumber = matchPEString(str)
% Regular expression to find  pattern
%looks for "_PE" followed by 1 to 4 digits (\d{1,4}) and captures the digits
pattern = '_PE(\d{1,4})_';

matches = regexp(str, pattern, 'tokens');

if ~isempty(matches)
    extractedNumber = str2double(matches{1}{1});
else
    extractedNumber = NaN; 
end

end

