function x = setToAvgNeighborhood(x, idx, neighborhood, offset)
%SETTOAVGNEIGHBORHOOD Summary of this function goes here
%inputs:
%  x: signal (Lp norm curve)
%  idx: index of detected peak (default = 5)
%  neighborhood: (half of) number of values around idx to use to compute average
%  value  (symmetrical neighborhood)
% offset: how many values in x to set to average (near the idx of the
% detected peak) (default=2)
%
%output: a copy of x that was modified at and around the peak at idx

if ~exist('neighborhood','var')
    neighborhood=5;%symmetrical neighborhood ie 5 before 5 after
end
if ~exist('offset','var')
    offset = 2;
end

if idx > neighborhood
    idx_range_noPeak_first = (idx-neighborhood):(idx-1);
    idx_range_noPeak_second = (idx+1):(idx+neighborhood);
    x_noPeak = x( [idx_range_noPeak_first idx_range_noPeak_second] );
    avg = mean(x_noPeak);

    x( (idx-offset):(idx+offset) ) = avg;
else
    x(idx) = mean(x( (idx+1):(idx+neighborhood) ));
end

end

