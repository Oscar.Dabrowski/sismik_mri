clc;clear all;close all;
%%

addpath('C:\git\sismik_mri\kspace_QM\')

%%

detectionParams = loadDefaultDetParams()

data_nomo = getfield( load('C:\MRIDATA\choco\choco2\chocoNoMo\choconomo.mat') , 'data');
data_mo = getfield( load('C:\MRIDATA\choco\choco2\chocoMotionSamples\chocomotion.mat'), 'data');
size(data_nomo)
size(data_mo)

subjectId=1
ksp = data_mo(:,:,:,subjectId);

[Lp_norm, Lp_norm_noOffset, Lp_flip, Lp_score ]= kspace_quasinorms(ksp, detectionParams.p)
[drop_indices, abs_dLp, dLp]  = Lp_detect(Lp_norm,detectionParams);


abs_dLp_old = abs_dLp;

neighborhood = 3;
%offset means: how many values in x to set to average (near the idx of the
% detected peak) (default=2)
offset=3;
for i=1:length(drop_indices)
    idx = drop_indices(i)
    abs_dLp = setToAvgNeighborhood(abs_dLp, idx, neighborhood, offset);
end

figure;
plot(abs_dLp_old,'-r','linewidth',2);hold on;
plot(abs_dLp,'--b', 'linewidth',2)
legend('absDiffLp before', 'absDiffLp after')


%next step fit to the filtered curve abs_dLp
formula = 'a*exp(-((x-b)/c)^2)';%gaussian fit
[general_fitresult, x, general_gof, ft] = fitLpCurve(abs_dLp,formula );

fitted_curve =  general_fitresult(x);
figure;
plot(x,abs_dLp);hold on;
plot(x, fitted_curve)


%un-trend old_abs_dLp
scalingFactor=150;
%abs_dLp_filtered = (abs_dLp_old./fitted_curve.')/scalingFactor;
abs_dLp_filtered = (abs_dLp_old - fitted_curve.');


figure;
subplot(121);
plot(abs_dLp_old,'-b','linewidth',2);title('Before normalization');
xlim([0 length(abs_dLp_filtered)]);
subplot(122);
plot(abs_dLp_filtered,'-b','linewidth',2);title('After normalization');
xlim([0 length(abs_dLp_filtered)]);
