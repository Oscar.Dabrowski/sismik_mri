# SISMIK for MRI

## Description

- A k-space quality metric (QM) which can also be used to detect motion corrupted lines in k-space is proposed. It is based on the
computation of p-quasinorms (p<1) of each phase encoding line in k-space and subsequent computation of the abs of discrete derivatives
and thresholding. Additionnaly Hermitian (conjugate) symmetry can be leveraged to further assess k-space degradation.

- SISMIK: a deep convolutional neural network (CNN) model for motion parameter estimation (in-plane, rigid-body) in k-space implemented in Python (with the pyTorch library). Python scripts for training and inference are provided with examples.


### Requirements

- k-space QM: was tested on Matlab R2020a (should work on later versions as well).
- SISMIK: works with the following Python modules:
```{r}
torch: 2.1.2+cu121
torchvision: 0.16.2+cu121
torchinfo: 1.8.0
skimage: 0.22.0
numpy: 1.26.2
scipy: 1.8.0
h5py: 3.10.0
matplotlib: 3.5.1
```

Trainable on an NVIDIA GeForce RTX 2080 Ti (12GB VRAM) and 64 GB RAM (for loading the whole trainingset in RAM, which is by default to improve training speed, but an other dataloader, loading files from disk can be used, customDatasetUtils.py)




Datasets (PE75, coil-combined, windowSize=9):

- The trainingset (~20GB) can be downloaded at https://1drv.ms/u/s!AvFXCSL3xchFiN0Lith7d2vlPwawiQ?e=ajw9m2

- The validation set (~1.6GB) can be downloaded at https://1drv.ms/u/s!AvFXCSL3xchFiN0NsJjxISlAMqGSFw?e=ShceeR

- The testset (~600MB) can be downloaded at https://1drv.ms/u/s!AvFXCSL3xchFiN0MkDrGvTGoZterMQ?e=Lpu4ra


Datasets (PE90, coil-combined, windowSize=9):

- trainingset: https://1drv.ms/f/s!AvFXCSL3xchFiN1NRVHlJhpugpIp5w?e=4nFeKd

- validation set: https://1drv.ms/f/s!AvFXCSL3xchFiN1ODmZ4UtkJW1ITJA?e=kHyyug

- test set: https://1drv.ms/f/s!AvFXCSL3xchFiN1MILMx6L5f5248AQ?e=sVyJqD



Example of typical directory tree I would use:

```{r}
od@cibmws:~/data/sismik_datasets/combined/PE75$ tree
.
├── test
│   └── winSz9
│       └── test.pkl
├── train
│   └── winSz9
│       └── train.pkl
└── val
    └── winSz9
        └── val.pkl
```


## Pre-trained SISMIK models

Three pretrained models are provided for PE lines: 75,90 and 105 (DC at 129 for k-spaces of shape 256x256):

- SISMIK pretrained PE75: https://1drv.ms/u/s!AvFXCSL3xchFiN1bus5mcUAhHrkEBg?e=lO2gdn

- SISMIK pretrained PE90: https://1drv.ms/u/s!AvFXCSL3xchFiN1cihHP4qFYmhNR3Q?e=sEVOpg

- SISMIK pretrained PE105: https://1drv.ms/u/s!AvFXCSL3xchFiN1aFELgvVlEB9LMmw?e=bYFt6l



## Contents

- K-space QM

A demo of the k-space quality metric is provided in kspace_QM/demo_ksp_motion_detectability.m. It uses choreography-controlled (ChoCo) motion-corrupted in vivo acquisitions in which volunteers moved at different times during (classical T1-w Spin-Echo) acquisitions following the protocol described in [Choreography Controlled (ChoCo) brain MRI artifact generation for labeled motion-corrupted datasets](https://www.sciencedirect.com/science/article/pii/S1120179722020476)

which you can cite as:

```bibtex
@article{dabrowski2022choreography,
  title={Choreography Controlled (ChoCo) brain MRI artifact generation for labeled motion-corrupted datasets},
  author={Dabrowski, Oscar and Courvoisier, S{\'e}bastien and Falcone, Jean-Luc and Klauser, Antoine and Songeon, Julien and Kocher, Michel and Chopard, Bastien and Lazeyras, Fran{\c{c}}ois},
  journal={Physica Medica},
  volume={102},
  pages={79--87},
  year={2022},
  publisher={Elsevier}
}
```

- SISMIK

	- Training
	
	- Inference
		- Simulations
			- Simulated examples can be dowloaded from: https://1drv.ms/u/s!AvFXCSL3xchFiN1hdKoSvIXnH1-9Cw?e=JAsVbJ (multiple motion events)

		- In vivo
			- Chocodb single motion events (SE, T1-w) can be downloaded from: https://1drv.ms/u/s!AvFXCSL3xchFiN1V6pX3QpABR7C9CQ?e=yeN7gk
			- Chocodb multiple motion events (SE, t1-w) : https://1drv.ms/u/s!AvFXCSL3xchFiN4bACPELS0tbtdxFw?e=SwEFCq 
			  (suggested path to match path conventions we use, extract .zip in : /home/<username>/git/sismik_mri/data/inVivo/chocodb_v2/ )


## Citing

If you use this project, or part of it, in your research or work, please cite it as follows:

- SISMIK paper:
O. Dabrowski et al., "SISMIK for brain MRI: Deep-learning-based motion estimation and model-based motion correction in k-space," in IEEE Transactions on Medical Imaging, doi: 10.1109/TMI.2024.3446450.

```bibtex
@ARTICLE{dabrowski2024sismik,
  author={Dabrowski, Oscar and Falcone, Jean-Luc and Klauser, Antoine and Songeon, Julien and Kocher, Michel and Chopard, Bastien and Lazeyras, François and Courvoisier, Sébastien},
  journal={IEEE Transactions on Medical Imaging}, 
  title={SISMIK for brain MRI: Deep-learning-based motion estimation and model-based motion correction in k-space}, 
  year={2024},
  keywords={Biomedical imaging;Measurement;Image reconstruction;Imaging;Mathematical models;Magnetic resonance imaging;Encoding;Deep learning;in-vivo;motion correction;motion estimation brain MRI},
  doi={10.1109/TMI.2024.3446450}}
```

- ChoCo paper:
  - see above (in §Contents section).
```bibtex
@article{dabrowski2022choreography,
  title={Choreography Controlled (ChoCo) brain MRI artifact generation for labeled motion-corrupted datasets},
  author={Dabrowski, Oscar and Courvoisier, S{\'e}bastien and Falcone, Jean-Luc and Klauser, Antoine and Songeon, Julien and Kocher, Michel and Chopard, Bastien and Lazeyras, Fran{\c{c}}ois},
  journal={Physica Medica},
  volume={102},
  pages={79--87},
  year={2022},
  publisher={Elsevier}
}
```


## License

- This project is licensed under the MIT License.

Copyright (c) 2024 University of Geneva (UNIGE), Univesity Hospitals of Geneva (HUG), EPFL, University of Lausanne (UNIL) and Lausanne University Hospital (CHUV)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

authors: Dabrowski, Oscar and Falcone, Jean-Luc and Klauser, Antoine and Songeon, Julien and Kocher, Michel and Chopard, Bastien and Lazeyras, François and Courvoisier, Sébastien