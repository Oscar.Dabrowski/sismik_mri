function varargout = genLegendRat(leg_in)
    for i = 1:numel(leg_in)
        [N, D] = rat(leg_in(i));
        if D == 1
            varargout{i} = ['L_{', num2str(N), '}'];
        else
            varargout{i} = ['L_{', num2str(N), '/', num2str(D), '}'];
        end
    end
    varargout{numel(leg_in)+1} = 'Random Chance';
    varargout{numel(leg_in)+2} = 'Location';
    varargout{numel(leg_in)+3} = 'southeast';
end