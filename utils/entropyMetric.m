function E = entropyMetric(img)
%ENTROPYMETRIC Summary of this function goes here
%  cf. Prof. David Atkinson papers
B = abs(img);
%normalize between 0 and 1
B = (B-min(B(:)))/(max(B(:))-min(B(:)));
% max(B(:))
% min(B(:))
B_max = sqrt(sum(B(:).^2));
E = (B/B_max).*log(B/B_max);
E(isnan(E)) = 0;
E = -sum(E(:));
end

