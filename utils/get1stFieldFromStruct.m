function sData = get1stFieldFromStruct(structPath)


sData = load( structPath );
fldName = fieldnames(sData);
fldName = fldName{1};
sData=getfield(sData, fldName);

end

