function y = normalize01(x)
    y = (x-min(x(:)))/(max(x(:))-min(x(:)));
end