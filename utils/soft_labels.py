import numpy as np

def generate_soft_labels(sequence_length, event_position, spread=10, scale=1.0):
    x = np.arange(sequence_length)
    soft_labels = np.exp(-np.square(x - event_position) / (2 * spread ** 2))
    soft_labels = soft_labels / np.max(soft_labels) * scale  
    return soft_labels
