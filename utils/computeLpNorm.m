function pnorm = computeLpNorm(ksp, p)


    m  = mean(abs(ksp),3);
    pnorm = log(vecnorm(m,p));
    pnorm = (pnorm-min(pnorm)) / (max(pnorm)-min(pnorm));


end

