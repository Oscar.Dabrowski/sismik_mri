function [angle, peValue] = extractAngleAndPE(filename, anglePattern)
    %use  '_angle(\d+)deg_' for some of my files ...
    
    if  ~exist('anglePattern','var')
        anglePattern = '_(\d+)deg_';
    end
    
    pePattern = 'PE(\d+)';

    
    angleMatch = regexp(filename, anglePattern, 'tokens');
    if ~isempty(angleMatch)
        angle = str2double(angleMatch{1}{1}); 
    else
        angle = NaN; 
    end

    peMatch = regexp(filename, pePattern, 'tokens');
    if ~isempty(peMatch)
        peValue = str2double(peMatch{1}{1}); 
    else
        peValue = NaN; 
    end
end