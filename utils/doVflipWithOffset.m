function [vflipDC, offsetDC]  = doVflipWithOffset(v, offset, offsetDC_in)
%DOVFLIP Summary of this function goes here
%align with DC and then try with given offset (offset can be a negative
%value, for right vs left directions)
%
%input v: a vector of Lp norm values (i.e. Lp norms of ksp phase lines)

vflipDC = fliplr(v);

if nargin==2
    %find index of DC (it is nessessaritly the max value!)
    idx_dc_v = find(v == max(v));
    idx_dc_vflip = find(vflipDC == max(vflipDC));
    offsetDC = idx_dc_v-idx_dc_vflip;
else
    offsetDC=offsetDC_in;
end

%translate a 1D vector -> equiv to translating a '1d image' so it is ok to
%use imtranslate
vflipDC = imtranslate(vflipDC, [offsetDC+offset 0]);

end

