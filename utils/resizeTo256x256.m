function [ksp_nomo_resized,C] = resizeTo256x256(ksp_nomo,C, C_cond, sliceIdx, targetRows,targetCols )
%RESIZETO256X256 Summary of this function goes here
%   returns a tensor of shape nbRows x nbCols x nbCoils x nbSlices
%with only the sliceIdx slice sized but for each coil in nbCoils (the rest
%are 0s)

if ~exist('targetRows','var')
    targetRows = 256;
end
if ~exist('targetCols','var')
    targetCols = 256;
end

nb_coils = size(ksp_nomo,3);
nb_slices = size(ksp_nomo,4);
%only the slice to be corrupted(but all coils for this slice)
%will be put in there (the others are zero)
tmp = zeros(targetRows,targetCols, nb_coils, nb_slices);
tmp_C = zeros(targetRows,targetCols, nb_coils, nb_slices);

parfor j=1:nb_coils
    %j
    %disp('size after reshape:')
    %size(twix_se_dataset_preproc_pad_crop(ksp_nomo(:,:,j,sliceIdx),targetRows,targetCols))
    tmp(:,:,j,sliceIdx) = twix_se_dataset_preproc_pad_crop(ksp_nomo(:,:,j,sliceIdx),targetRows,targetCols);
    if C_cond
        tmp_C(:,:,j,sliceIdx) = reshapeC2targetRowsCols(C(:,:,j,sliceIdx),targetRows,targetCols);
    end
end

ksp_nomo_resized = tmp;

if C_cond
    C = tmp_C;
end
if isa(C,'double')
    C = single(C);
end
end

