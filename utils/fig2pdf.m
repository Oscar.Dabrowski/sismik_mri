function fig2pdf(fname,dstDir, figHnd)
%FIG2PDF Summary of this function goes here
%   must always give fname and dstDir,
%optionnal args in varargin: handle to fig
    fprintf('nargin = %d \n',nargin);
    if length(fname) < 3
        error('!!! filename MUST be at least 3 characters !!!');
    end
    if ~strcmpi(fname(end-2:end), 'pdf')
        fname = [fname '.pdf'];
    end
    
    if nargin > 2
        exportgraphics(figHnd,fullfile(dstDir,fname),'ContentType','vector')
    else
        exportgraphics(gcf,fullfile(dstDir,fname),'ContentType','vector')
    end
end
%dummy minimal reproducible example
% x = linspace(0,1,100);
% fig1 = figure; plot(sin(2*pi*3*x));
% fig2pdf('f1_','C:\Users\odabr\OneDrive\Documents\exportPDFTest',fig1);