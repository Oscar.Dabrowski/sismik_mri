function labelsForNUFFT = compute_traj_singleEvt( theta_pred,tx_pred,ty_pred, PELine )
%COMPUTE_TRAJ_SINGLEEVT: outputs the NUFFT trajectory and translations
%trajectory for rotation correction with NUFFT and translation correction
%by conjugate phase ramps, respectively. (These labels trajectories are meant to be
%provided to the "NUFFT reconstruction function" )
%
% Input args:
% -----------
% - theta_pred, tx_pred,ty_pred: SISMIK motion params. predictions
%
% - PELine: the "empirical" phase encoding line corresponding to motion onset
% (assuming single motion event here)
%
% Output args:
% ------------
% - labelsForNUFFT: trajectory that recon. func will use to correct mo.
%                 artifacts
%
%
% Note: assuming kpsace of shape 256x256 (see SISMIK MRI paper)

%% build labels traj

% Note: translations are as estimated by SISMIK: to correct (i.e., remove
% artifacts, the reconstruction function which will build the NUFFT from
% the rotation traj will start by cancelling the translations by applying
% conjugate phase ramps (i.e., exp(-j...)) -> in summary: no minus sign
% here !!!

theta = zeros(256,1);
theta(1:PELine) = 0;
theta(PELine:end) = theta_pred;

tx = zeros(1,256); 
tx(1:PELine) = 0;
tx(PELine:end) = tx_pred;

ty = zeros(1,256); 
ty(1:PELine) = 0;
ty(PELine:end) = ty_pred;

labelsForNUFFT.theta=theta;
labelsForNUFFT.tx=tx;
labelsForNUFFT.ty=ty;

end

