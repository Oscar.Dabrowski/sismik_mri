function [ img_recon_lsqr, img_recon_nufftAdj ] = NUFFT_recon( ksp_corrupted, labels, doDispNUFFTGrid, maxIter)
%NUFFT_RECON Summary of this function goes here
%   Detailed explanation goes here

if ~isvector(labels.theta) || ~isvector(labels.tx) || ~isvector(labels.ty)
    error('!!!!!!!!!! ERROR: motion parameters should be vectors !!!!!!!!!!!');
end

if ~exist('doDispNUFFTGrid','var')
    doDispNUFFTGrid=false
end

if ~exist('maxIter','var')
    maxIter = 8;
end


N = size(ksp_corrupted,1);
M = N;

%Note: keep sign for theta
theta = labels.theta;
%Note: opposite sign for motion-correction of translations
tx = -labels.tx;
ty = -labels.ty;

if isrow(theta)
    theta=theta.';
end
if ~isrow(tx)
    tx=tx.';
end
if ~isrow(ty)
    ty=ty.';
end


[kx ky]=meshgrid(-N/2:N/2-1);
k_complex = kx+1j*ky;
k_complex = k_complex.*exp(1j*deg2rad(theta));

kx = (real(k_complex(:))/N)*2*pi;
ky = (imag(k_complex(:))/N)*2*pi;

if doDispNUFFTGrid
    nufftGrid_fig = dispNUFFTGrid( kx, ky );
end


%% phase correction from estimated translational motion trajectories (based on Fourier shift theorem)

phase_corr = phase_corr_ksp(tx,ty, M,N, 1,N);

%phase_corr is a matrix that should be point-wise multiplied to cancel
%translational motion parameters and it is computed by the above function
%phase_corr_ksp(...)
ksp_corrupted = ksp_corrupted.*phase_corr;


%% NUFFT recon. from estimated rotational trajectory (based on Fourier rotation theorem)

nufft_traj = nufft_init([ky(:),kx(:)], [N,N], [8,8], 2*[N,N],[N/2,N/2]);

img_recon_nufftAdj = nufft_adj(ksp_corrupted(:),nufft_traj);

WM = speye(length(ksp_corrupted(:)));

[img_recon_lsqr,flag,relres,iter,resvec,lsvec] = lsqr(@(x,flag)nufft_operator(x,flag,nufft_traj,N,WM),WM*ksp_corrupted(:),[],maxIter);

img_recon_lsqr = reshape(img_recon_lsqr,[N N]);


end