clc;clear all;close all;

addpath('../../utils')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Demo showing IN VIVO (single mo event) motion estimation with SISMIK and
% motion correction with Fourier theorems
% (i.e., ''model-based'' using conjugate phase termsn and NUFFT)
% the script useses SISMIK estimations and translation ''prior''
% (see SISMIK paper) and reconstructs both versions with and without prior
%  then chooses the best, i.e. the one with lowest entropy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Note: after running setup.m you might need to run:
% ir_mex_build.m (>>> from irt dir)
%to re-build MEX files on your system

%( or in the worst case, re-download and extract Fessler's toolbox from:
% http://web.eecs.umich.edu/~fessler/irt/fessler.tgz )

addpath('irt/');
run('irt/setup.m');


% set params here ...
%for example -  considering the naming conventions we used in this project
%% 
subject_str = 'chocodb_230425_1530'% 'chocodb_230502_1445
rot_angle = 3;
PELine = 75;
doDispNUFFTGrid=false;%show NUFFT motion trajectory grid
maxIter = 6;% for iteratorve NUFFT, typically not more than 10 or 12
%% 

dataDir = '/home/od/git/sismik_mri/data/inVivo/chocodb_v2/';
subjectDir = [ subject_str '/SE_TR500ms_TE12ms_1MoEvt' ];
file = ['chocodb_' num2str(rot_angle) 'deg_PE' num2str(PELine) '.mat'];


fullPath = fullfile(fullfile(dataDir, subjectDir), file)

ksp_struct = load(fullPath)
kc = ksp_struct.kc;
nb_slices = size(kc,3);%3rd index should be slices
size(kc)

%load sismik estimations for this subject (single motion event)
sismik_pred_file = [ '/home/od/git/sismik_mri/data/results/inference/invivo/' subject_str '_' num2str(rot_angle) 'deg_PE' num2str(PELine) '_sismik_predictions.h5' ]
sismik_pred_angle = h5read(sismik_pred_file, '/angle')
%only one of the 2 in-plane translation ''counts'' for moco
sismik_pred_translation = h5read(sismik_pred_file, '/translation')
sismik_pred_PELine = h5read(sismik_pred_file, '/PELine')


%note: minus: because empirically seen that the relevant translation 
%has opposite sign
translation_prior = -0.5*sismik_pred_angle


%construct estimated motion traj from sismik estimated labels 
%(the 'multipleEvt' func also works for single evt i.e. if the different sismik_pred...
% all contain 1 est.)
%Note: one translation can be ignored since does not have a noticeable
%impact on moco (cf ChoCo i vivo experiments)
motionTraj = compute_relative_traj_multipleEvt( sismik_pred_angle, sismik_pred_translation, sismik_pred_PELine );
motionTraj_translPrior = compute_relative_traj_multipleEvt( sismik_pred_angle, translation_prior, sismik_pred_PELine );

img_recon_lsqr = zeros(size(kc));
img_recon_nufftAdj = zeros(size(kc));
img_recon_lsqr_translPrior = zeros(size(kc));
img_recon_nufftAdj_translPrior = zeros(size(kc));


tic;
[ ksp_corrupted_all, img_corrupted_all ] = prepareChocoForNUFFT_allSlices( kc );
 
for sliceIdx =1:1:nb_slices
    ksp_corrupted = ksp_corrupted_all(:,:,sliceIdx);
    
    [ img_recon_lsqr(:,:,sliceIdx), img_recon_nufftAdj(:,:,sliceIdx) ] = ...
        NUFFT_recon( ksp_corrupted, motionTraj, doDispNUFFTGrid, maxIter);
    
    [ img_recon_lsqr_translPrior(:,:,sliceIdx), img_recon_nufftAdj_translPrior(:,:,sliceIdx) ] = ...
        NUFFT_recon( ksp_corrupted, motionTraj_translPrior, doDispNUFFTGrid, maxIter);
    
    fprintf('Reconstructing slice %d ... \n', sliceIdx);
end
toc

example_slice = 7;
img_corrupted_ = normalize01(abs(fftshift(img_corrupted_all(:,:,example_slice))));
img_restored_ =  normalize01(abs(img_recon_lsqr(:,:,example_slice)));
img_restored_translPrior =  normalize01(abs(img_recon_lsqr_translPrior(:,:,example_slice)));

entropy_corrupted =  entropyMetric(img_corrupted_);
entropy_restored =  entropyMetric(img_restored_);
entropy_resotred_translPrior =  entropyMetric(img_restored_translPrior);

fprintf( 'Entropy of corrupted img: %.2f\n',entropy_corrupted);
fprintf( 'Entropy of restored img: %.2f\n',entropy_restored);
fprintf( 'Entropy of restored img (transl. prior) : %.2f\n', entropy_resotred_translPrior);

%%  select best moco
if entropy_restored < entropy_resotred_translPrior
   img_restored_best = img_restored_;
   entropy_restored_best = entropy_restored;
else
    img_restored_best = img_restored_translPrior;
    entropy_restored_best = entropy_resotred_translPrior;
end
%%

figure;
set(gcf, 'Position', [100, 100, 1300, 700]);
subplot(121);
imshow(rot90( img_corrupted_, 1), [0 quantile(img_corrupted_(:),0.98)]);colormap('gray');
title(['Corrupted (entropy: ' num2str(entropy_corrupted) ')' ], 'fontsize', 16);
subplot(122);
imshow(rot90( img_restored_best, 1), [0 quantile(img_restored_best(:),0.98)]);colormap('gray');
title(['Restored (entropy: ' num2str(entropy_restored_best) ')'], 'fontsize', 16);
