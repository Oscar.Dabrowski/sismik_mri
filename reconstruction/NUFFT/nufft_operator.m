function x = nufft_operator(y,transp_flag,st,N,WM)
    if strcmp(transp_flag,'transp')     
        x = nufft_adj(WM'*y,st);
        x = x(:);

    elseif strcmp(transp_flag,'notransp')
        x = nufft(reshape(y,N,N),st);
        x = WM*x(:);
    end
end