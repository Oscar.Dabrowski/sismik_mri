function phase_corr = phase_corr_ksp(tx,ty, M,N, idx_start,idx_end)
%PHASE_CORR_KSP : phase correction in k-space: phase_corr = phase_corr_ksp(tx,ty, M,N, idx_start,idx_end)
%   returns a matrix MEANT TO BE point-wise multiplied with a k-space block to
%   correct for phase shifts in ksp/translation in img space
% tx,ty : scalars OR vectors of translational parameters 
% M,N : max rows,max cols
% idx_start, idx_end : (optionnal) start/end of ksp block (row start/end)


if nargin == 4
    idx_start = 1;
    idx_end = M;
end

ky = (-M/2:M/2-1)/M;
kx = (-N/2:N/2-1)/N;

phase_x = exp(-1j*2*pi*kx.*tx);
%transpose and point-wise mult. ty in case arg ty is a vector of estimated translations
%like that tx and ty may either be scalars of vectors
phase_y = exp(-1j*2*pi*ky.'.*ty.');

phase_corr = phase_x.*phase_y;
phase_corr = phase_corr(idx_start:idx_end,:);

end