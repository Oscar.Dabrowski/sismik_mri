function motionTraj = compute_relative_traj_multipleEvt( theta_sismik,ty_sismik, PE_lines )
%This function outpus a motion trajectory (angle and two translations in
%the motionTraj struct) that can be used to correct kspaces corrupted my
%multiple motion events. The motionTraj is computed from sismik predictions
%and the correspondign phase encoding lines given in the PE_lines variable
% !!! Note: "relative" in the sense that it is relative wrt an arbitrary zero
% but note that SISMIK predictions are relative wrt each previous phase
% line regions, e.g. if subject moved from 0 to 3 deg then from 3 to 2, the
% predicted angle for the "3 to 2 segment" would be -1 deg.
%
%
% Input args:
% -----------
% - theta_sismik: array of rot angles (pred by sismik) for each evt in order
%
% - ty_sismik: array of translations (pred by sismik) for each evt in order (corresp to respective
%               theta in same position (theta array above))
%
% - PE_list: list of phase lines corresponding to motion events
%
% Output args:
% ------------
% - motionTraj : struct containing 3 vectors : angular traj and 2 translation
%                 trajectories
%


N=256;
theta_traj=zeros(1,N);
ty_traj=zeros(1,N);
tx_traj=zeros(1,N);
relative_theta = 0;
relative_ty=0;

nb_evt = length(PE_lines);

for i=1:nb_evt
    if i==nb_evt
        idx_range = PE_lines(i):N;
    else
        idx_range = PE_lines(i):PE_lines(i+1);
    end
    relative_theta = relative_theta+theta_sismik(i);
    relative_ty = relative_ty+ty_sismik(i);
    
    theta_traj(idx_range)=relative_theta;
    ty_traj(idx_range)=relative_ty;
end

motionTraj.theta=theta_traj;
motionTraj.ty=ty_traj;
motionTraj.tx=tx_traj;%typically ignored since no perceptible impact
end