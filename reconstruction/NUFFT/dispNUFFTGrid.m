function nufftGrid_fig = dispNUFFTGrid( kx, ky )
%DISPNUFFTGRID plot NUFFT trajectory (2D) for information
%   

    nufftGrid_fig = figure;%figure('Position', get(0, 'Screensize'));
    
    scatter(kx,ky);
    xlabel('kx','fontsize',15);ylabel('k_y','fontsize',15);
    title('NUFFT grid coordinates (in radians)', 'fontsize',18);

    %in radians
    xlim([-1.1*pi, 1.1*pi]);
    ylim([-1.1*pi, 1.1*pi]);

    xticks([-pi, -pi/2, 0, pi/2, pi]);
    xticklabels({'-\pi', '-\pi/2', '0', '\pi/2', '\pi'});
    xlabel('kx','fontsize',15);

    yticks([-pi, -pi/2, 0, pi/2, pi]);
    yticklabels({'-\pi', '-\pi/2', '0', '\pi/2', '\pi'});
    ylabel('ky','fontsize',15);

    hYLabel = get(gca, 'YLabel');
    y_label_position = get(hYLabel, 'Position');

    text_rotation = 90; 
    text(y_label_position(1), y_label_position(2), 'Phase encoding', ...
         'Rotation', text_rotation, 'HorizontalAlignment', 'center', ...
         'VerticalAlignment', 'middle','FontSize',14);

     set(gca, 'FontSize', 15);
end

