clc;clear all;close all;

addpath('../../utils')
addpath('irt/');
run('irt/setup.m');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%This script contains the following demo : 
%  motion correction of artefacts caused by multiple motion events
% (example for one in vivo ChoCo subject with 3 motion events)



%% 
subject_str = 'chocodb_231122_1530'
doDispNUFFTGrid=false;%show NUFFT motion trajectory grid
maxIter = 6;% for iteratorve NUFFT, typically not more than 10 or 12
%% 

dataDir = '/home/od/git/sismik_mri/data/inVivo/chocodb_v2/';
subjectDir = [ subject_str '/SE_TR500ms_TE12ms_3MoEvt' ];
file = [subject_str '_multiMo3evt_v1.mat'];
fullPath = fullfile(fullfile(dataDir, subjectDir), file)
%%
fprintf('Reading data file: %s ... \n', fullPath);
%%
ksp_struct = load(fullPath)
kc = ksp_struct.kc;
nb_slices = size(kc,3);%3rd index should be slices
size(kc)


%get sismik predictions (computed with
%/home/od/git/sismik_mri/SISMIK/inference/sismik_predict.py )
resultsDir = '/home/od/git/sismik_mri/data/results/inference/invivo/';
sismik_pred_file = fullfile(resultsDir,[ subject_str '_3evt_PE75_90_105.h5' ]);
%info: fields in this .h5 file ('datasets')
dataInfo = h5info(sismik_pred_file); dataInfo.Datasets.Name
%%
fprintf('Reading predictions from file: %s ... \n', sismik_pred_file);
%%
sismik_pred_PELine = h5read(sismik_pred_file, '/PELine');%effective PE lines
sismik_pred_angle = h5read(sismik_pred_file, '/angle');
sismik_pred_translation = h5read(sismik_pred_file, '/translation');
translation_prior = -0.5*sismik_pred_angle;
motionTraj = compute_relative_traj_multipleEvt( sismik_pred_angle, sismik_pred_translation, sismik_pred_PELine );
motionTraj_translPrior = compute_relative_traj_multipleEvt( sismik_pred_angle, translation_prior, sismik_pred_PELine );

%% display estimated motion trajectory
figure;
plot([1:length(motionTraj.theta)], motionTraj.theta, 'LineWidth',2);
hold on;
plot([1:length(motionTraj.ty)], motionTraj.ty, 'LineWidth',2)
legend('Angle (degrees)', 'Translation (pixels)')
xlim([1 length(motionTraj.theta)]);grid on;
title('Estimated motion trajectory')
%%

img_recon_lsqr = zeros(size(kc));
img_recon_nufftAdj = zeros(size(kc));
img_recon_lsqr_translPrior = zeros(size(kc));
img_recon_nufftAdj_translPrior = zeros(size(kc));


[ ksp_corrupted_all, img_corrupted_all ] = prepareChocoForNUFFT_allSlices( kc );


%%
tic;
[ ksp_corrupted_all, img_corrupted_all ] = prepareChocoForNUFFT_allSlices( kc );
 
for sliceIdx =1:1:nb_slices
    ksp_corrupted = ksp_corrupted_all(:,:,sliceIdx);
    
    [ img_recon_lsqr(:,:,sliceIdx), img_recon_nufftAdj(:,:,sliceIdx) ] = ...
        NUFFT_recon( ksp_corrupted, motionTraj, doDispNUFFTGrid, maxIter);
    
    [ img_recon_lsqr_translPrior(:,:,sliceIdx), img_recon_nufftAdj_translPrior(:,:,sliceIdx) ] = ...
        NUFFT_recon( ksp_corrupted, motionTraj_translPrior, doDispNUFFTGrid, maxIter);
    
    fprintf('Reconstructing slice %d ... \n', sliceIdx);
end
toc

example_slice = 6;
img_corrupted_ = normalize01(abs(fftshift(img_corrupted_all(:,:,example_slice))));
img_restored_ =  normalize01(abs(img_recon_lsqr(:,:,example_slice)));
img_restored_translPrior =  normalize01(abs(img_recon_lsqr_translPrior(:,:,example_slice)));

entropy_corrupted =  entropyMetric(img_corrupted_);
entropy_restored =  entropyMetric(img_restored_);
entropy_resotred_translPrior =  entropyMetric(img_restored_translPrior);

fprintf( 'Entropy of corrupted img: %.2f\n',entropy_corrupted);
fprintf( 'Entropy of restored img: %.2f\n',entropy_restored);
fprintf( 'Entropy of restored img (transl. prior) : %.2f\n', entropy_resotred_translPrior);

%%  select best moco
if entropy_restored < entropy_resotred_translPrior
   img_restored_best = img_restored_;
   entropy_restored_best = entropy_restored;
else
    img_restored_best = img_restored_translPrior;
    entropy_restored_best = entropy_resotred_translPrior;
end
%%

figure;
set(gcf, 'Position', [100, 100, 1300, 700]);
subplot(121);
imshow(rot90( img_corrupted_, 1), [0 quantile(img_corrupted_(:),0.98)]);colormap('gray');
title(['Corrupted (entropy: ' num2str(entropy_corrupted) ')' ], 'fontsize', 16);
subplot(122);
imshow(rot90( img_restored_best, 1), [0 quantile(img_restored_best(:),0.98)]);colormap('gray');
title(['Restored (entropy: ' num2str(entropy_restored_best) ')'], 'fontsize', 16);
