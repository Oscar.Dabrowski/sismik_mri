function [ ksp_corrupted_all, img_corrupted_all ] = prepareChocoForNUFFT_allSlices( ksp_corrupted )
%PREPARECHOCOFORNUFFT_ALLSLICES Summary of this function goes here
%   same as prepareChocoForNUFFT but with a loop on all slices, so 
% this time ksp is a tensor of nbslices*rows*cols

    ksp_corrupted_all = zeros(size(ksp_corrupted));
    img_corrupted_all = zeros(size(ksp_corrupted));

    for i = 1:size(ksp_corrupted,3)
        [ksp_corrupted_all(:,:,i), img_corrupted_all(:,:,i) ] = ...
            prepareChocoForNUFFT(ksp_corrupted(:,:,i));
    end

end

