function [ ksp_corrupted,img_corrupted ] = prepareChocoForNUFFT(ksp_corrupted)
% Input args:
% ----------
% - ksp_corrupted: matrix of rows*cols
%
%
% Output args:
% ------------
%
% - ksp_corrupted: "re-arranged" kspace in format expected by NUFFT func
% optionnally also outputs the ifft2 in img_corrupted (SINGLE slice)
%


% rearrange data to match the format expected by the NUFFT reconstruction
% function
ksp_corrupted = ksp_corrupted.';

img_corrupted = fftshift(ifft2(fftshift(ksp_corrupted)));
ksp_corrupted = fftshift(fft2(img_corrupted));%shift to match expect format 
img_corrupted = ifft2(ksp_corrupted);


end