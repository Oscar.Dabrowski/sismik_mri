function [theta_out, tx,ty] = genSimuTrajWithSpeed(speed, theta, cx_offset,cy_offset, idxPELine)
%theta is expected to be a vector (already with motion onset at the same idxPELine)

tx = zeros(1,256);
ty = zeros(1,256);

theta_out=theta;

if speed ~= 0
    convKernel = ones(1,speed)/speed;
    theta_conv = conv(theta, convKernel,'valid');
    
    % 2*length(convKernel) arbitrary just to cover conv range but not more
    % to avaoid border effects
    conv_range = (idxPELine - 2*length(convKernel)):(idxPELine + 2*length(convKernel));
    theta_out(conv_range) = theta_conv(conv_range);
end


%%need to recompute tx ty because of speed
cxy_offsets = [cx_offset;cy_offset];
for i=1:length(theta_out)
    A = [(-cosd(theta_out(i))+1) sind(theta_out(i)); -sind(theta_out(i)) (-cosd(theta_out(i))+1)];
    txty = inv(A)\cxy_offsets;
    tx(i) = txty(1);
    ty(i) = txty(2);
end
tx(isnan(tx)) = 0;
ty(isnan(ty)) = 0;

end

