#!/bin/bash
 
#SBATCH --partition=shared-cpu
#SBATCH --time=01:59:00
#SBATCH --cpus-per-task=12
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=5555 # in MB
#SBATCH -o myjob-%A_%a.out
#SBATCH --licenses=matlab@matlablm.unige.ch
#SBATCH --licenses=distrib_computing_toolbox@matlablm.unige.ch
 
module load MATLAB/2021b

#the variable you want to pass to matlab
job_array_index=${SLURM_ARRAY_TASK_ID}
 
BASE_MFILE_NAME=main_simu_indivCoils_slurm
#>>>>>>>>>>>>> change simuFile here before running a new sbatch <<<<<<<<<<<<<
#dir is hardcided in main_simu_slurm
simuFile="PE75_valset_cxHigh_cyLow_indivCoils.txt"
 
unset DISPLAY
 
echo "Running ${BASE_MFILE_NAME}.m on $(hostname)"
 
srun matlab -nodesktop -nosplash -nodisplay -r "${BASE_MFILE_NAME}($job_array_index,'${simuFile}')"

