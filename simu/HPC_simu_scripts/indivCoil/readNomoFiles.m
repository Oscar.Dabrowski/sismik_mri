function outputTxt = readNomoFiles(fn)

outputTxt={};
k=1;

fileID = fopen(fn);
tline = fgetl(fileID);
while ischar(tline)
    outputTxt{k}=tline;
    fprintf('line=%s \n',tline);
    tline = fgetl(fileID);
    if ~ischar(tline)
        break;
    end
    k=k+1;
end
fclose(fileID);


end

