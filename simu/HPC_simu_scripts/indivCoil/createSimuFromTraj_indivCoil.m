function [ksp_corrupted_coil, ksp_nomo_coil] = createSimuFromTraj_indivCoil( ksp_nomo_allCoils, simuParamsStruct)

rng('shuffle');

%REM: variable offset (windowSIze) is taken when the data is loaded
%see pytorch dataloader (in my module customDatasetUtils.py ->
%CustomDataset_v2 )

labels_sim = simuParamsStruct.labels_sim;
sliceIdx = simuParamsStruct.sliceIdx;
coilIdx = simuParamsStruct.coilIdx;
interpType = simuParamsStruct.interpType;


%% NoMo special testset
if ( abs(sum(labels_sim.theta(:))) + abs(sum(labels_sim.tx(:))) + abs(sum(labels_sim.ty(:))) ) == 0
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    disp('>>>>> !!!!! NOMO -> no simu -> taking raw nomo ksp lines !!!!! <<<< ');
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    
    %transpose to keep same format as simulated slices roth simuRIgid...
    ksp_nomo_coil = ksp_nomo_allCoils(:,:,coilIdx,sliceIdx).';
    %there is no corruption here but we still keep this field for consistency
    ksp_corrupted_coil = ksp_nomo_coil;
   
else
    disp('>>> INFO: regular simu. with rot. (in createSimuFromTraj_v2) ...');
    
    %param order: (ksp, labels, sliceIdx, coil, interpType)
    [ksp_corrupted_coil, ksp_nomo_coil] = ...
        simuRigidMulticoil_latest_withPreRot_indivCoil( ...
        single(ksp_nomo_allCoils), ...
        labels_sim, ...
        sliceIdx, ...
        coilIdx, ...
        interpType);
    
    %% create x,y <-> example,label pair for trainingset
    ksp_corrupted_coil = fftshift(ksp_corrupted_coil);
    ksp_nomo_coil = fftshift(ksp_nomo_coil);
end


end
