function main_simu_indivCoils_slurm(SLURM_ID, simuParamsFile)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    rng('shuffle');%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %read a simulation file (txt file) that contains the simu parameters
    simuFileDir = '/home/users/d/dabrowsk/scripts/git/ksp_moco_od_phd/simu_multicoil/latest_indivCoil/simuParamFiles_inidvCoils/'
    simuParams = readSimuFile(fullfile(simuFileDir,simuParamsFile));
    
    disp(simuParams);
    
    genSimuDataset1evtMultiSpeed_indivCoil(simuParams, SLURM_ID);
    
    %redisplay params at end of script
    disp(['>>>>>>>>> simuFile was: ' , simuParamsFile ]);
    disp(simuParams);
end
