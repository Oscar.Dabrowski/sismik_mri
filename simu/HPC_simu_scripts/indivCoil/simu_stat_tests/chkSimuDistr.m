clc;clear all;close all;

%%

%dataDir ='/data2/OD/twix_db_files/simu_1evt_multiSpeed/latest2/PE75/test/';
dataDir = '/data2/OD/twix_db_files/simu_1evt_multiSpeed/latest2/PE75/test/';

%f = dir(fullfile(dataDir,'*.mat'))
%recursive all subdirs
f = dir(fullfile(dataDir, '**/*.mat'));
Nsim=50;

k=1;
for i=1:length(f)
    
    tmp = load( fullfile(f(i).folder, f(i).name));
    for j=1:Nsim
        theta(k) = getEffectiveVal( unique(tmp.simu_struct(j).labels_sim.theta));
        tx(k)= getEffectiveVal(unique(tmp.simu_struct(j).labels_sim.tx));
        ty(k)= getEffectiveVal(unique(tmp.simu_struct(j).labels_sim.ty));
        cx(k)=tmp.simu_struct(j).labels_sim.cx_offset;
        cy(k)=tmp.simu_struct(j).labels_sim.cy_offset;
        k=k+1
    end

end

fntSz = 16;

fig1=figure;
subplot(131);
histogram(theta,'normalization', 'probability')
xlabel('Degrees');
ylabel('Empirical probability');
xticks([-5 -2.5 0 2.5 5]);
title('Angle','fontsize',fntSz);
g=gca;
g.FontSize = 14;
subplot(132);
histogram(tx,'normalization', 'probability');
xlabel('Pixels (or mm)');
ylabel('Empirical probability');
xlim([-5 5]);xticks([-5 -3 -1 0 1 3 5]);
title('Tx','fontsize',fntSz);
g=gca;
g.FontSize = 14;
subplot(133);
histogram(ty,'normalization', 'probability');
xlabel('Pixels (or mm)');
ylabel('Empirical probability');
xlim([-5 5]);xticks([-5 -3 -1 0 1 3 5]);
title('Ty','fontsize',fntSz);
g=gca;
g.FontSize = 14;


fig2=figure;
subplot(121);
histogram(cx,'normalization', 'probability');
xlabel('Pixels (or mm)');
ylabel('Empirical probability');
title('Cx','fontsize',fntSz);
g=gca;
g.FontSize = 14;
subplot(122);
histogram(cy,'normalization', 'probability');
xlabel('Pixels (or mm)');
ylabel('Empirical probability');
title('Cy','fontsize',fntSz);
g=gca;
g.FontSize = 14;

saveas(fig1, 'fig1.fig');
saveas(fig2, 'fig2.fig');
