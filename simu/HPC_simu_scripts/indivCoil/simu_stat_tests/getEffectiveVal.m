function v = getEffectiveVal(u)
abs_u=abs(u);
idx = find( abs_u == max(abs_u) );
v=u(idx);
end

