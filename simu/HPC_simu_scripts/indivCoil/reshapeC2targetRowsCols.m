function C_out = reshapeC2targetRowsCols(C,targetRows,targetCols)
%RESHAPEC2TARGETROWSCOLS Summary of this function goes here
%   Detailed explanation goes here
    
%pad too much on purpose
C_pad = padarray(C, [targetRows targetCols],'both');
half_rows = targetRows/2;
half_cols = targetCols/2;
M=size(C_pad,1);N=size(C_pad,2);
midRows = M/2+1;midCols = N/2+1;
C_out = C_pad( (midRows-half_rows):(midRows+half_rows-1), (midCols-half_cols):(midCols+half_cols-1) );
end

