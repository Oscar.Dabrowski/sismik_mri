function [ksp_corr_coil, ksp_nomo_coil] = simuRigidMulticoil_latest_withPreRot_indivCoil(ksp, labels, sliceIdx, coil, interpType)
%Description: motion corruption on a SINGLE COIL ELEMENT of a slice
%   input: 
%       ksp: the multicoil nomo kspace on which simulation is to be applied
%       labels: simulation labels (rotation and translations)
%       sliceIdx: the index of the slice to corrupt
%       coil: the index of the coil to corrupt in the slice sliceIdx
%   output: 
%        ksp_corr_coil: corrupted coil k-space 
%        ksp_nomo_coil: motion-free coil k-space 
%       (for corresponding coil and slice idx given in params) 
%
% Additionnal info about labels:
%labels (gen. outside this func) must contains the following fields:
% simu.theta (vector)
% simu.tx  (vector)
% simu.ty  (vector)
% simu.cx_offset (scalar)
% simu.cy_offset (scalar)

if ischar(ksp) || isstring(ksp) %is ksp was passed as a filename e.g. '/a/b/c/ksp_nomo.mat'
    ksp_noMo = load(ksp);
    ksp_noMo = ksp_noMo.kspace;
else
    ksp_noMo = ksp;
end

ksp_noMo=permute(ksp_noMo,[2 1 3 4]);
M=size(ksp_noMo,1); N=size(ksp_noMo,2);

cx_offset = labels.cx_offset;
cy_offset = labels.cy_offset;

rowCenter = (M/2)+1-cx_offset;
colCenter = (N/2)+1-cy_offset;

theta = labels.theta;

nb_coils = size(ksp_noMo,3);

%cxy_offsets = [cx_offset;cy_offset];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% pre-rotate : may be put in a function later

unique_thetas = unique(theta);
preRot_coil_matrix = zeros(nb_coils, length(unique_thetas), M, N);%stores images (not ksp) of each rotated coil element

size(preRot_coil_matrix)

%REM: these loop should be fast under the assumption of only a few unique thetas
%valid in the case of one (or maybe a few) motion events
for i=1:length(unique_thetas)
    img_noMo_coil = fftshift( ifft2(fftshift(ksp_noMo(:,:,coil,sliceIdx))) );
    %output of imrotateCustom is in image space
    %must store in the preRot 'external' var not temporary var for
    %parfor
    [preRot_coil_matrix(coil,i,:,:),~] = imrotateCustom(img_noMo_coil,unique_thetas(i),rowCenter,colCenter,interpType);  
    %store kspace in preRot tensor
    %do not do that here... parfor seems to ignore this instruction ?!?
    %preRot_coil_matrix(coil,i,:,:) = fftshift(fft2(preRot_coil_matrix(coil,i,:,:)));
i
end

%%


img_noMo_coil = fftshift( ifft2(fftshift(ksp_noMo(:,:,coil,sliceIdx))) );

ksp_nomo_coil = fftshift(fft2(img_noMo_coil));
ksp_corr_coil = ksp_nomo_coil;


%1 angle for each phase encode line (some are 0 or almost 0 i.e. the
%resting, or 'nomo', positions)

for i=1:length(unique_thetas)
    idx = theta == unique_thetas(i);%logical indexing: faster than 'find'
    ksp_rot_coil = fftshift(fft2(squeeze(preRot_coil_matrix(coil,i,:,:))));%whole rotated coil kspace

    %idx : positions of same angles (thetas) i.e. a 'block of the same
    %rotation angle' to be place at the same idx in the corrupted ksp
    ksp_corr_coil(idx,:) = squeeze(ksp_rot_coil(idx,:));
end



end

