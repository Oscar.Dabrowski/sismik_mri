function sliceArray = readSlicesPerAcq(fn)

%each line of fn is a vector of slices that have been screened (manually) and considered nomo
%and each line are slices of a given acquisition (same line as in twix_nomo_acq.txt that should be located in the same folder

%outputTxt={};
k=1;

fileID = fopen(fn);
tline = fgetl(fileID);
while ischar(tline)
    %outputTxt{k}=tline;
    sliceArray{k} = str2num(tline) %each line is potentially a vector
    fprintf('line=%s \n',tline);
    tline = fgetl(fileID);
    if ~ischar(tline)
        break;
    end
    k=k+1;
end
fclose(fileID);


end

