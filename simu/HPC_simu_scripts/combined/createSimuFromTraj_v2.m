function [ksp_corrupted_combined, ksp_nomo_slice] = createSimuFromTraj_v2( ksp_nomo_combined, ksp_nomo, C, simuParamsStruct)

rng('shuffle');

%REM: variable offset (windowSIze) is taken when the data is loaded
%see pytorch dataloader (in my module customDatasetUtils.py ->
%CustomDataset_v2 )

labels_sim = simuParamsStruct.labels_sim;
sliceIdx = simuParamsStruct.sliceIdx;
interpType = simuParamsStruct.interpType;


%% NoMo special testset
if ( abs(sum(labels_sim.theta(:))) + abs(sum(labels_sim.tx(:))) + abs(sum(labels_sim.ty(:))) ) == 0
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    disp('>>>>> !!!!! NOMO -> no simu -> taking raw nomo ksp lines !!!!! <<<< ');
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    
    %transpose to keep same format as simulated slices roth simuRIgid...
    ksp_nomo_slice = ksp_nomo_combined(:,:,sliceIdx).';
    %there is no corruption here but we still keep this field for consistency
    ksp_corrupted_combined = ksp_nomo_slice;
   
else
    disp('>>> INFO: regular simu. with rot. (in createSimuFromTraj_v2) ...');

     %>>>>> this simuRigid function version should be much faster (v2)
     %comb = combined with espirit
     [ksp_corrupted_combined, ~, ksp_nomo_slice] = ...
         simuRigidMulticoil_latest_withPreRot_v2( ...
         single(ksp_nomo), ...
         double(C), ...
         labels_sim, ...
         sliceIdx, ...
         interpType);

    %comb = combined with espirit
    %[ksp_corrupted_combined, ~, ksp_nomo_slice] = ...
    %    simuRigidMulticoil_latest( ...
    %    single(ksp_nomo), ...
    %    double(C), ...
    %    labels_sim, ...
    %    sliceIdx, ...
    %    interpType);
    
    
    %%%%%ksp_corrupted_combined_all(:,:,i) = fftshift(ksp_corrupted_combined);
    %%%%%ksp_nomo_slices(:,:,i) = fftshift(ksp_nomo_slice);
    
    %% create x,y <-> example,label pair for trainingset
    ksp_corrupted_combined = fftshift(ksp_corrupted_combined);
    ksp_nomo_slice = fftshift(ksp_nomo_slice);
end


end
