function main_simu_slurm(SLURM_ID, simuParamsFile)
  simuFileDir = '/home/users/d/dabrowsk/scripts/git/ksp_moco_od_phd/simu_multicoil/combined/simuParamFiles/'
  simuParams = readSimuFile(fullfile(simuFileDir,simuParamsFile));
  disp(simuParams);
  pause(3.5);
  genSimuDataset1evtMultiSpeed_v2(simuParams, SLURM_ID);
end
