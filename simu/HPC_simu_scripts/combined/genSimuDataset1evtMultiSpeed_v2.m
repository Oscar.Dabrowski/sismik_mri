function genSimuDataset1evtMultiSpeed_v2(simuParams, SLURM_ID)
%GENSIMUDATASET1EVTMULTISPEED 
% simuParams : struct with simulation parameters
%   this function generates a dataset of simulated corrupted brain slices
%(ESPIRiT combined from multiple coils)
%
%SLURM_ID: optional (used on SLURM HPC)

dstDir=simuParams.dstDir
nomoFiles=simuParams.nomoFiles
nomoSlices = simuParams.nomoSlices

%%
if exist('SLURM_ID','var')
  dstDir = fullfile(dstDir,[ 'SLURM_ID_' num2str(SLURM_ID) ] );
  simuParams.dstDir=dstDir;
end

mkdir(dstDir)%makes destination dir if not exists

%REM: the nomoFiles var gets transformed into a cellarray of file
%acquisitinos since we dont care anymore of the main nomoFileName
%containing all the sub-filenames
nomoFiles = readNomoFiles(nomoFiles)

%REM: sliceIdx_perAcq is a cellarray with a vector of slices which are the
%nomo slices corresponding to each line of the acquisition files in nomoFiles
sliceIdx_perAcq = readSlicesPerAcq( nomoSlices );

if exist('SLURM_ID','var')
    nb_nomoFiles = 1
    nomoFiles = nomoFiles(SLURM_ID)
    sliceIdx_perAcq = sliceIdx_perAcq(SLURM_ID)
else
    nb_nomoFiles = length(nomoFiles)
end

%%
for i=1:nb_nomoFiles
    
    if exist('SLURM_ID','var')
        acqID=SLURM_ID;
    else
        acqID = i;
    end
    
    fn = nomoFiles{ i }
    slicesForCurrentAcq = sliceIdx_perAcq{ i }
    nbSlices = length(slicesForCurrentAcq)
    
    parfor j=1:nbSlices
        sliceIdx = slicesForCurrentAcq(j)
        % (->  due to parfor: cannot write sliceIdx to simuParams struct
        % here)
        genDataset_subfunction(acqID, fn, sliceIdx,  simuParams);
    end
end
end

