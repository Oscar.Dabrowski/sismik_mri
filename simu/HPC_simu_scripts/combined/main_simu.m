function main_simu(simuParamsFile)
    params = readSimuFile(simuParamsFile);
    disp(params);
    pause(3.5);
    
    genSimuDataset1evtMultiSpeed_v2(params);%, SLURM_ID)
    
    %redisplay params at end of script
    disp(['>>>>>>>>> simuFile was: ' , simuParamsFile ]);
    disp(params);
    
end