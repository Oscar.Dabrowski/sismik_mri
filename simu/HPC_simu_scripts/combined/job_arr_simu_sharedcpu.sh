#!/bin/bash
 
#SBATCH --partition=shared-cpu
#SBATCH --time=02:45:00
#SBATCH --cpus-per-task=14
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=9998 # in MB
#SBATCH -o myjob-%A_%a.out
#SBATCH --licenses=matlab@matlablm.unige.ch
#SBATCH --licenses=distrib_computing_toolbox@matlablm.unige.ch
 
module load MATLAB/2021b

#the variable you want to pass to matlab
job_array_index=${SLURM_ARRAY_TASK_ID}
 
BASE_MFILE_NAME=main_simu_slurm
#>>>>>>>>>>>>> change simuFile here before running a new sbatch <<<<<<<<<<<<<
#dir is hardcided in main_simu_slurm
simuFile="PE85_trainingset_cxHigh_cyLow.txt"
 
unset DISPLAY
 
echo "Running ${BASE_MFILE_NAME}.m on $(hostname)"
 
srun matlab -nodesktop -nosplash -nodisplay -r "${BASE_MFILE_NAME}($job_array_index,'${simuFile}')"

