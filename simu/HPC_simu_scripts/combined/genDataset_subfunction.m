function genDataset_subfunction( ID, fn, sliceIdx,  simuParams)
%% func signature: genDataset_subfunction( ID, fn, sliceIdx,  simuParams)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rng('shuffle');%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~isfield(simuParams,'distrType')
    distrType = 'gauss';%special case for 'old' simparam txt file without the 'distrType' specified
else
    distrType = simuParams.distrType;
end

%save this parameter in simuParams (could not do it in parfor before)
simuParams.sliceIdx=sliceIdx;

srcDir=simuParams.srcDir
dstDir=simuParams.dstDir
% nomoFiles=simuParams.nomoFiles
% nomoSlices = simuParams.nomoSlices
Nsim = simuParams.Nsim
idxPhaseLine = simuParams.idxPhaseLine

switch distrType
    case 'gauss'
        disp('>>>>>>> Gaussian distribution') 
        %gaussian std for the rotation angle
        MU_theta = simuParams.MU_theta;
        SIGMA_theta = simuParams.SIGMA_theta
        %center of rotation standard deviations
        MU_cx1 = simuParams.MU_cx1
        MU_cx2 = simuParams.MU_cx2
        MU_cy1 = simuParams.MU_cy1
        MU_cy2 = simuParams.MU_cy2
        SIGMA_cx1 = simuParams.SIGMA_cx1
        SIGMA_cx2 = simuParams.SIGMA_cx2
        SIGMA_cy1 = simuParams.SIGMA_cy1
        SIGMA_cy2 = simuParams.SIGMA_cy2
    case 'uniform'
        disp('>>>>>>> uniform distribution') 
        MIN_theta = simuParams.MIN_theta
        MAX_theta = simuParams.MAX_theta
        MIN_cx = simuParams.MIN_cx
        MAX_cx = simuParams.MAX_cx
        MIN_cy = simuParams.MIN_cy
        MAX_cy = simuParams.MAX_cy
    otherwise
        error('!!!!!!!!!!! ERROR: unknown distribution !!!!!!!!!!!')
end

%interpolation tpe
interpType= simuParams.interpType

%"speed": actually "inverse convention" of a physical 'speed':
%0 -> fastest, 2 -> slower etc.
%(the number is the size of the 1d conv kernel to implement speed, so 
%0 and 1 are the same i.e. "instantaneous motion", i.e., no convolution
%speed will be generated uniformly from a uniform distr between 0 and
%maxSpeed (which is in practive the "slowest" i.e. largest conv kernel
maxSpeed = simuParams.maxSpeed
speedRange = [0:maxSpeed];
speedRange(speedRange==1) = [];%remove '1' since here 0 == 1 !

%direction of motion : startAtZero=true means simulation starts at angle=0
%and rises to theta at idxPhaseLine
%( otherwise (if false) starts at theta and drops to 0 at idxPhaseLine )
startAtZero= simuParams.startAtZero;

nb_phaseLines = simuParams.nb_phaseLines
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fullfn = fullfile(srcDir, fn);

%read .mat file to get the nomo data used for simulation
%output is always 256x256 (cropped and/or padded if was needed)
%--->>> do read once here because it is slow !
[ksp_nomo_combined, ksp_nomo, C] = readNoMoKspMatForDatasetGen(fullfn, sliceIdx);

idxDC = nb_phaseLines/2 +1;

%filename for the dataset of this ID
fileName = fullfile(dstDir,['ID_' num2str(ID) '_' genCurrTimestamp() '_slice' num2str(sliceIdx) '.mat'] );
%prealloc for speed
simu_struct(Nsim).dummy = [];

for simuIdx = 1:Nsim
    
    theta = zeros(1,nb_phaseLines);
    
    
    switch distrType
        case 'gauss'
            %generate randomly thetaVal
            thetaVal = normrnd(MU_theta,SIGMA_theta)
            %gen cx and cy
            cx_offset = bimodal_gaussian(1, MU_cx1, SIGMA_cx1, MU_cx2, SIGMA_cx2)%normrnd(MU_cx,SIGMA_cx)
            cy_offset = bimodal_gaussian(1, MU_cy1, SIGMA_cy1, MU_cy2, SIGMA_cy2)%normrnd(MU_cy,SIGMA_cy)
        case 'uniform'
            thetaVal = rand()*(MAX_theta-MIN_theta)+MIN_theta
            cx_offset = rand()*(MAX_cx-MIN_cx)+MIN_cx
            cy_offset = rand()*(MAX_cy-MIN_cy)+MIN_cy
    end

    
    %gen random speed (uniformly)
    speed = randsample(speedRange,1)

    if startAtZero
        theta(idxPhaseLine:end) = thetaVal;
    else
        theta(1:idxPhaseLine) = thetaVal;
    end
    
    %[theta, tx,ty] : these outputs are all VECTORS (ridigid-body motion param trajectory for each PE line)
    %-> since 1 motion event: on "side" of the trajectory is filled with zeros and the other is with theta 
    %with a possible transition depending on speed.
    [theta, tx,ty] = genSimuTrajWithSpeed(speed, theta, cx_offset,cy_offset, idxPhaseLine);
    
    labels_sim.theta = theta %vector
    labels_sim.tx = tx %vector
    labels_sim.ty = ty %vector
    labels_sim.cx_offset=cx_offset %scalar
    labels_sim.cy_offset=cy_offset %scalar
    
    
    %% create simulations
    simuParams.labels_sim=labels_sim;%store labels to pass them to createSimuFromTraj_... function below..
    simuParams.speed=speed;
    
    %generates simulation and saves it do dstDir with hash code as name like <hash>.mat
    [ksp_corrupted_combined, ksp_nomo_slice] = createSimuFromTraj_v2(ksp_nomo_combined, ksp_nomo, C, simuParams);
    
    simu_struct(simuIdx).ksp_corrupted_combined = ksp_corrupted_combined;
    simu_struct(simuIdx).ksp_nomo_slice = ksp_nomo_slice;
    simu_struct(simuIdx).simuParamsStruct = simuParams;
    simu_struct(simuIdx).labels_sim = labels_sim;
    
    fprintf('simu#%d\n',simuIdx);
end

simu_struct = rmfield(simu_struct, 'dummy');
%save all the N simulations into 1 .mat file
save(fileName, 'simu_struct','-v7.3');

end