function [ksp_nomo_combined, ksp_nomo, C] = readNoMoKspMatForDatasetGen(file,sliceIdx)

fprintf('>>>>>> file=%s\n',file);

targetRows = 256;
targetCols = 256;

k=load(file);
kc = k.kc;
size(kc)

ksp_nomo = k.ks;
C = k.C;
%mask = k.mask;
nb_coils = size(ksp_nomo,3)
nb_slices = size(ksp_nomo,4)

C_cond = size(C,1) ~= targetRows || size(C,2) ~= targetCols;

%for 'nomo' special testset
ksp_nomo_combined = kc;
    
    
%% chk if need to resize
if (size(kc,1) ~= targetRows) || (size(kc,2) ~= targetCols)
    
    ksp_nomo_combined=zeros(targetRows,targetCols,nb_slices);
    
    ksp_nomo_resized = zeros( targetRows,targetCols,  nb_coils, nb_slices);
    C_resized = zeros( targetRows,targetCols, nb_coils, nb_slices);

    [ksp_nomo_resized_onlySliceIdx, C_resized_onlySliceIdx] = ...
        resizeTo256x256(ksp_nomo,C, C_cond, sliceIdx, targetRows,targetCols );

    %all the coils for only the sliceIdx-th slice
    ksp_nomo_resized(:,:,:,sliceIdx) = ksp_nomo_resized_onlySliceIdx(:,:,:,sliceIdx);

    C_resized(:,:,:,sliceIdx) = C_resized_onlySliceIdx(:,:,:,sliceIdx);

    %
    C=C_resized;
    ksp_nomo=ksp_nomo_resized;

    disp('C size after reshape:')
    size(C)
    disp('ksp nomo shape after resize:')
    size(ksp_nomo)
    
    %for 'nomo' special testset
    %for nomo testset we need nomo combined
    for tmp_slice=1:nb_slices
        ksp_nomo_combined(:,:,tmp_slice) = twix_se_dataset_preproc_pad_crop(kc(:,:,tmp_slice),targetRows,targetCols);
    end
    %ksp_nomo_combined = kc_resized;
    
    disp('size ksp nomo combined:');
    size(ksp_nomo_combined)
end


end

