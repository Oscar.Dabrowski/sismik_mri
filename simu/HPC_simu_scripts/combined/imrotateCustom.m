function [out,Rout] = imrotateCustom(I,theta,rowCenter,colCenter,interpType,outSize)
%theta: in degrees
%typical call (without optionnal args): [out,Rout] = imrotateCustom(I,theta);
[M N] = size(I);
if nargin == 2
    rowCenter = M/2+1;
    colCenter = N/2+1;
    interpType = 'nearest';
    outSize = imref2d( size(I) );
elseif nargin == 4 
    interpType = 'nearest';
    outSize = imref2d( size(I) );
elseif nargin == 5
    outSize = imref2d( size(I) );
end
% theta
% rowCenter
% colCenter
% interpType
tX = rowCenter;
tY = colCenter;
tTranslationToCenterAtOrigin = [1 0 0; 0 1 0; -tX -tY,1];
tTranslationBackToOriginalCenter = [1 0 0; 0 1 0; tX tY,1];
tRotation = [cosd(theta) -sind(theta) 0; sind(theta) cosd(theta) 0; 0 0 1];
tformCenteredRotation = tTranslationToCenterAtOrigin*tRotation*tTranslationBackToOriginalCenter;
tformCenteredRotation = affine2d(tformCenteredRotation);
[out,Rout] = imwarp(I,tformCenteredRotation,'interp',interpType, 'OutputView', outSize);

end