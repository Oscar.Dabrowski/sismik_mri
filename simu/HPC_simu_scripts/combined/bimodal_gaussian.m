function output = bimodal_gaussian(num_samples, mu1, sigma1, mu2, sigma2)
    if num_samples == 1
        if rand() > 0.5
            output = normrnd(mu1, sigma1);
        else
            output = normrnd(mu2, sigma2);
        end
        return;
    end

    if mod(num_samples, 2) ~= 0
        num_samples1 = floor(num_samples / 2);
        num_samples2 = num_samples - num_samples1;
    else
        num_samples1 = num_samples / 2;
        num_samples2 = num_samples / 2;
    end

    gaussian1 = normrnd(mu1, sigma1, [1, num_samples1]);
    gaussian2 = normrnd(mu2, sigma2, [1, num_samples2]);
    
    output = [gaussian1 gaussian2];
end