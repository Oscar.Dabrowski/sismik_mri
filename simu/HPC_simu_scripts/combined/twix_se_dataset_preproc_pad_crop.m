function ksp_out = twix_se_dataset_preproc_pad_crop(ksp_in,targetRows,targetCols)
%TWIX_SE_DATASET_PREPROC_PAD_CROP : resize ksp_in (input kspace) to 256x256
% and makes sure that the DC is centered at (129,129)
%   REM: in general all collected T1 SE twix have 320 freq encoding lines
%   but the number of phase lines is variable and can be even or odd (e.g.
%   commonly seen values are
%   270, 234, 261, 243 or even 225 sometimes 

M = size(ksp_in,1); N = size(ksp_in,2);
if nargin==1
    targetRows=256;
    targetCols=256;
end
mid_x = targetRows/2; 
mid_y=targetCols/2;
    
%pad too much on purpose
ksp_pad = padarray(ksp_in, [targetRows targetCols],'both');
ksp_mag_pad = abs(ksp_pad);
[idx_dc_x, idx_dc_y] = find(ksp_mag_pad == max(ksp_mag_pad(:)));

ksp_out = ksp_pad( (idx_dc_x-mid_x):(idx_dc_x+mid_x-1),   (idx_dc_y-mid_y):(idx_dc_y+mid_y-1) );
end