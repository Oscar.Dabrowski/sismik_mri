function parameters = readSimuFile(simuParamsFile)

    fid = fopen(simuParamsFile, 'r');
    if fid == -1
        error('Could not open file %s', simuParamsFile);
    end
    data = textscan(fid, '%s %s', 'Delimiter', ' ');
    fclose(fid);
    parameters = struct;
    for i = 1:numel(data{1})
        paramName = data{1}{i};
        paramValue = data{2}{i};
        if isnan(str2double(paramValue))
            parameters.(paramName) = paramValue;
        else
            parameters.(paramName) = str2double(paramValue);
        end
    end
    
end

