clc;clear all;close all;
%%

chocoNomo_srcDir =  '/home/od/data/noMo/';
chocoFixedSuffix = '_se_first_noMo_notrigger_TR500ms_TE12ms_start_noMo.dat.mat';
srcDir = '/home/od/MRI_DATA/Lp_simu_latest/';
dstDir =  '/home/od/MRI_DATA/Lp_simu_latest_bicubic/';
mkdir(dstDir)

%pre-load for speed 
chocoDico = containers.Map;
chocoDico('chocodb_230425_1530') = load(fullfile(chocoNomo_srcDir, 'chocodb_230425_1530_se_first_noMo_notrigger_TR500ms_TE12ms_start_noMo.dat.mat'))
chocoDico('chocodb_230427_1930') = load(fullfile(chocoNomo_srcDir, 'chocodb_230427_1930_se_first_noMo_notrigger_TR500ms_TE12ms_start_noMo.dat.mat'))
chocoDico('chocodb_230502_1445')= load(fullfile(chocoNomo_srcDir, 'chocodb_230502_1445_se_first_noMo_notrigger_TR500ms_TE12ms_start_noMo.dat.mat'))

files = dir([srcDir '*.mat']);
nb_files = length(files)

relevant_slices = 5:8
%'standard' (empirical) center offset for all simu
cx_offset = 35;
cy_offset = 0;
interpType='bicubic';%or nearest or sinc
startAtZero = true;

ksp_corrupted_combined_all = zeros(256,256, length(relevant_slices));

tic
for i=1:nb_files
    folder_ = files(i).folder;
    fileName_ = files(i).name;
    ff = fullfile(folder_, fileName_)
    [speed, angle, PE] = extractSimuParamsFromFn(ff);
    params(i).ff=ff;
    params(i).speed=speed;
    params(i).angle=angle;
    params(i).PE=PE;

    chocoStr = extractChocoSubstring(fileName_)
    data_nearest=load(ff);
    
    %ff_choco = [fullfile(chocoNomo_srcDir,chocoStr ) , chocoFixedSuffix]
    data_forBicubicSimu = chocoDico(chocoStr)
    C = data_forBicubicSimu.C;
    %kc=data.kc;
    ks=data_forBicubicSimu.ks;
    clear data_forBicubicSimu;

    

    simu_labels =  getSimuLabels(angle,speed,cx_offset,cy_offset, PE, startAtZero);
    cnt=1;
    for sliceIdx = relevant_slices
        [ksp_corrupted_combined, ksp_corrupted, ksp_nomo_slice] = ... 
            simuRigidMulticoil_latest_withPreRot_v2(ks,C, simu_labels, sliceIdx, interpType);
        ksp_corrupted_combined_all(:,:,cnt)=ksp_corrupted_combined;
        cnt=cnt+1;
    end

    data_new = data_nearest;
    data_new.simuParams = params(i);
    data_new.nomoChocSubjectString=chocoStr;
    data_new.ksp_corrupted_combined_bicubic_all=ksp_corrupted_combined_all;

    dstFf = fullfile(dstDir, fileName_);
    save(dstFf, 'data_new', '-v7.3');
    %data_new.ksp_corrupted_combined_all = %results of simu 
end
toc



%%

% helper functions

function chocoStr = extractChocoSubstring(fileName)
    % delim =  "_2023"
    chocoPattern = '(choco[^_]+_[^_]+_[^_]+)(?=_2023)';
    chocoMatch = regexp(fileName, chocoPattern, 'tokens');
    
    if ~isempty(chocoMatch)
        chocoStr = chocoMatch{1}{1};
    else
        chocoStr = '';
    end
end

function simu_labels =  getSimuLabels(thetaVal,speed,cx_offset,cy_offset, idxPELine, startAtZero )
    theta = zeros(1,256);
    if startAtZero
        theta(idxPELine:end) = thetaVal;
    else
        theta(1:idxPELine) = thetaVal;
    end   
    [theta_out, tx,ty] = genSimuTrajWithSpeed(speed, theta, cx_offset,cy_offset, idxPELine);
    simu_labels.theta=theta_out;
    simu_labels.cx_offset = cx_offset;
    simu_labels.cy_offset = cy_offset;
    simu_labels.tx=tx;
    simu_labels.ty=ty;
end

function [speed, angle, PE] = extractSimuParamsFromFn(fileName)
    % extract speed val
    speedPattern = 'speed(\d+)';
    speedMatch = regexp(fileName, speedPattern, 'tokens');
    if ~isempty(speedMatch)
        speed = str2double(speedMatch{1}{1});
    else
        speed = NaN;
    end

    %Extract the angle value, taking into account the underscore as decimal point
    anglePattern = 'theta(\d+)_?(\d*)deg';
    angleMatch = regexp(fileName, anglePattern, 'tokens');
    if ~isempty(angleMatch)
        anglePart1 = angleMatch{1}{1};
        anglePart2 = angleMatch{1}{2};
        if isempty(anglePart2)
            angle = str2double(anglePart1);
        else
            angle = str2double([anglePart1, '.', anglePart2]);
        end
    else
        angle = NaN;
    end

    %Extract PE value
    PEPattern = 'PE(\d+)';
    PEMatch = regexp(fileName, PEPattern, 'tokens');
    if ~isempty(PEMatch)
        PE = str2double(PEMatch{1}{1});
    else
        PE = NaN; 
    end
end
