clc;clear all;close all;
%%

N=256;
x=abs(phantom(N)).*exp(1j*angle(phantom(N)));%to add a phase so that we use a complex phantom image
ksp = fftshift(fft2(x));
ksp_corrupted = ksp;

% figure;
% subplot(121);
% imagesc(abs(x));colormap('gray');colorbar;
% title('magnitude');
% subplot(122);
% imagesc(angle(x));colormap('gray');colorbar;
% title('phase');

%% demo params for 1 motion event

PE_line = 90;
theta = -3.5;%degrees
rowCenter = -15;
colCenter = 33;
interpType = 'bicubic';
x_rot = imrotateCustom(x, theta, rowCenter, colCenter, interpType);
ksp_rot = fftshift(fft2(x_rot));

ksp_corrupted(:,PE_line:end) = ksp_rot(:,PE_line:end);

x_corrupted = ifft2(ksp_corrupted);

%% 

figure;

subplot(121);
imshow(abs(x));
title('no motion');
subplot(122);
imshow(abs(x_corrupted));
title('motion');

