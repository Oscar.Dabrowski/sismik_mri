function data = readSubjFile(f)
    % Open the file in text mode
    fid = fopen(f, 'rt');
    if fid == -1
        error('Cannot open file: %s', f);
    end
    
    
    data = {};
    line = fgetl(fid);
    while ischar(line)
        data{end+1} = line; 
        line = fgetl(fid); 
    end
    
    
    fclose(fid);
end
