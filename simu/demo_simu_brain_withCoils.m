clc;clear all;close all;

%%

srcDir = '/home/od/data/noMo/';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% -------->  choose multi-coil acquisition file here:
chocoSubject = 'chocodb_230425_1530_se_first_noMo_notrigger_TR500ms_TE12ms_start_noMo.dat.mat';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
f=fullfile(srcDir, chocoSubject)


data = load(f)
C = data.C;
%kc=data.kc;
ks=data.ks;
clear data;

relevant_slices = 5:8

N=256;

idxPELine = 75;
speed=0;
thetaVal=-3.5;
cx_offset = 35;
cy_offset = 0;
interpType='bicubic';%or nearest or sinc
startAtZero = true;

theta = zeros(1,N);
if startAtZero
    theta(idxPELine:end) = thetaVal;
else
    theta(1:idxPELine) = thetaVal;
end


[theta_out, tx,ty] = genSimuTrajWithSpeed(speed, theta, cx_offset,cy_offset, idxPELine);
simu_labels.theta=theta_out;
simu_labels.cx_offset = cx_offset;
simu_labels.cy_offset = cy_offset;
simu_labels.tx=tx;
simu_labels.ty=ty;

ksp_corrupted_combined_all = zeros(length(relevant_slices),256,256);
img_all=zeros(size(ksp_corrupted_combined_all));
i=1;
tic
for sliceIdx = relevant_slices
    %this function simulates motion corruption of 1 slice (but leveraging multiple
    %coils for this to be done in a realistic manner)
    % %note use: ks since we use coils
    [ksp_corrupted_combined, ksp_corrupted, ksp_nomo_slice] = ... 
        simuRigidMulticoil_latest_withPreRot_v2(ks,C, simu_labels, sliceIdx, interpType);
    ksp_corrupted_combined_all(i, :,:)=ksp_corrupted_combined;
    
    img= rot90(fliplr(flipud(abs(ifft2(ksp_corrupted_combined)))),-1);
    img= (img-min(img(:)))/( quantile(img(:),0.985) - min(img(:)));
    img_all(i,:,:)=img;
    i=i+1;

end
toc

%%

figure('Name','Motion trajectory')
plot(theta_out, 'linewidth', 2);
hold on;
plot(tx, 'linewidth', 2);
plot(ty, 'linewidth', 2);

figure;
montage(permute(img_all,[2 3 1]))