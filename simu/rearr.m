function ksp = rearr(ksp)
    nbs=size(ksp,3);
    for i=1:nbs
        ksp(:,:,i) = fftshift(ksp(:,:,i)).';
    end
end

