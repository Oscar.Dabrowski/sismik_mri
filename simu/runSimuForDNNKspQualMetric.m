clc;clear all;clc;

%%
addpath('/home/od/git/sismik_mri/utils/');

rng('shuffle');

startPE = 5;
nbCPU = 12;

PELines = [ startPE:startPE+nbCPU ];

tic;
parfor PELine = PELines
    simuFullAcq(PELine);
end

