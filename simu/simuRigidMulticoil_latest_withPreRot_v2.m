function [ksp_corrupted_combined, ksp_corrupted, ksp_nomo_slice] = simuRigidMulticoil_latest_withPreRot_v2(ksp,C, labels, sliceIdx, interpType)
%SIMURIGID Summary of this function goes here
%   ksp: the multicoiil nomo kspace on which simulation is to be applied
%   C: coil profiles pre-computed with e-spirit : use to recombine the
%i  ndividually corrupted coil kspaces
%
%labels are given (generated outside this function) e.g. ny genSimuTraj
%and the function simuRigidMulticoil_v2 will perform the motion corruption
%with these labels
%
%the labels struct must contains the following fields:
% simu.theta (vector)
% simu.tx  (vector)
% simu.ty  (vector)
% simu.cx_offset (scalar)
% simu.cy_offset (scalar)

if ischar(ksp)%is ksp was passed as a filename e.g. '/a/b/c/ksp_nomo.mat'
    ksp_noMo = load(ksp);
    ksp_noMo = ksp_noMo.kspace;
else
    ksp_noMo = ksp;
end

ksp_noMo=permute(ksp_noMo,[2 1 3 4]);
M=size(ksp_noMo,1); N=size(ksp_noMo,2);

%initialisation
ksp_corrupted = ksp_noMo;
ksp_nomo_slice = ksp_noMo;%also recon the nomo slice for comparison


cx_offset = labels.cx_offset;
cy_offset = labels.cy_offset;

rowCenter = (M/2)+1-cx_offset;
colCenter = (N/2)+1-cy_offset;

theta = labels.theta;

nb_coils = size(ksp_noMo,3);

%cxy_offsets = [cx_offset;cy_offset];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% pre-rotate : may be put in a function later

unique_thetas = unique(theta);
preRot_coil_matrix = zeros(nb_coils, length(unique_thetas), M, N);%stores images (not ksp) of each rotated coil element

size(preRot_coil_matrix)

%REM: these loop should be fast under the assumption of only a few unique thetas
%valid in the case of one (or maybe a few) motion events
for i=1:length(unique_thetas)
    for coil=1:nb_coils
        img_noMo_coil = fftshift( ifft2(fftshift(ksp_noMo(:,:,coil,sliceIdx))) );
        %output of imrotateCustom is in image space
        %must store in the preRot 'external' var not temporary var for
        %parfor
        [preRot_coil_matrix(coil,i,:,:),~] = imrotateCustom(img_noMo_coil,unique_thetas(i),rowCenter,colCenter,interpType);  

        coil
    end
    i
end

%%


%simulate corruption on each coil
%then combine each coil using coil profiles into a slice

parfor coil=1:nb_coils %foreach coil : simulate all motion
    
    img_noMo_coil = fftshift( ifft2(fftshift(ksp_noMo(:,:,coil,sliceIdx))) );
    
    ksp_corr_coil = fftshift(fft2(img_noMo_coil));
    
    %1 angle for each phase encode line (some are 0 or almost 0 i.e. the
    %resting, or 'nomo', positions)
    
    for i=1:length(unique_thetas)
        
        
        idx = theta == unique_thetas(i);%logical indexing: faster than 'find'
        ksp_rot_coil = fftshift(fft2(squeeze(preRot_coil_matrix(coil,i,:,:))));%whole rotated coil kspace
        
        %idx : positions of same angles (thetas) i.e. a 'block of the same
        %rotation angle' to be place at the same idx in the corrupted ksp
        ksp_corr_coil(idx,:) = squeeze(ksp_rot_coil(idx,:));
        
    end
    %put the coil where it belongs 
    ksp_corrupted(:,:,coil,sliceIdx) = ksp_corr_coil;
    
    coil
end

%% 

%C = getEspiritMaps_libmri(kspace(:,:,:,sl_idx),eigThresh);
C = permute(C(:,:,:,sliceIdx),[2 1 3 4]) ;
size(C)
I = cell(1,nb_coils);
I_nomo= cell(1,nb_coils);
for coil_idx = 1:nb_coils
    I{coil_idx} = ifft2(fftshift(ksp_corrupted(:,:,coil_idx,sliceIdx)));
    I_nomo{coil_idx} = fftshift(ifft2(fftshift(ksp_noMo(:,:,coil_idx,sliceIdx))));
end

I_vect=[I{:}];
I_nomo_vect = [I_nomo{:}];

I_vect=reshape(I_vect,[M N nb_coils]);
I_nomo_vect=reshape(I_nomo_vect,[M N nb_coils]);

I_rec_vect=sum(I_vect.*conj(C),3)./sum(abs(C).^2,3);
I_rec_vect(isnan(I_rec_vect))=0;

I_nomo_rec_vect=sum(I_nomo_vect.*conj(C),3)./sum(abs(C).^2,3);
I_nomo_rec_vect(isnan(I_nomo_rec_vect))=0;
    
%%
  
%this is the one to be used for training ML for the slice corrupted version
ksp_corrupted_combined = fft2(I_rec_vect);
ksp_nomo_slice = fft2(I_nomo_rec_vect);
end

