clc;clear all;close all;

%%
addpath('/home/od/git/sismik_mri/utils/')

dataDir = '/data2/OD/Lp_things/chocoSq_toChk/'
dstDir = '/data2/OD/Lp_things/chocoSq_toChk/256x256/'
f=dir(fullfile(dataDir, '*.mat'))

tic

for i = 1:length(f)
    
    ff = fullfile(f(i).folder, f(i).name)
    data = load(ff);
    ksp_nomo = data.ks;
    C_nomo = data.C;
    nb_coils = size(ksp_nomo, 3)
    nb_slices = size(ksp_nomo, 4)
    ks = zeros(256,256,nb_coils, nb_slices);
    C = zeros(256,256,nb_coils, nb_slices);
    for sliceIdx = 1:nb_slices
        [ksp_nomo_resized,C_resized] = resizeTo256x256(ksp_nomo,C_nomo, true, sliceIdx, 256,256 );
        ks(:,:,:,sliceIdx) = ksp_nomo_resized(:,:,:,sliceIdx);
        C(:,:,:,sliceIdx) = C_resized(:,:,:,sliceIdx);
    end
    
    
    %save ...
    %%% TODO
    [a b c] = fileparts( ff );
    newName = [ b '_C_ks_256x256_.mat' ] 
    save(fullfile(dstDir, newName), 'C', 'ks', '-v7.3');
    %del tmp vars ...
    clear ks;
    clear C;
end

toc