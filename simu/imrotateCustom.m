function [out,Rout] = imrotateCustom(I,theta,rowCenter,colCenter,interpType,outSize)
%Note: code may need to be cleaned/optimized 
%Note: for discrete since interp: this function uses myrotate_mb.m -> so
%may need to addpath(...)
%theta: in degrees
%typical call (without optionnal args): [out,Rout] = imrotateCustom(I,theta);
[M N] = size(I);
Rout=NaN;%this output is probably never needed and might be supressed later
if nargin == 2
    rowCenter = M/2+1;
    colCenter = N/2+1;
    interpType = 'nearest';
    outSize = imref2d( size(I) );
elseif nargin == 4 
    interpType = 'nearest';
    outSize = imref2d( size(I) );
elseif nargin == 5
    outSize = imref2d( size(I) );
end
if theta ~= 0
    % theta
    % rowCenter
    % colCenter
    % interpType
    if strcmp(interpType,'sinc')
        %addpath('C:\git\k-space-motion-artifact-correction\yaroslavsky\Advanced-Image-Processing-Lab-Interface\Image resampling\');
        disp('>>>>>> doing imrotateCustom with dsinc interpolation ...');
        %% compute tx ty params 
        idealRowCenter = M/2+1; cy_offset = idealRowCenter-rowCenter;
        idealColCenter = N/2+1; cx_offset = idealColCenter-colCenter;
        cxy_offsets = [cx_offset;cy_offset];
        for i=1:length(theta)
            A = [(-cosd(theta(i))+1) sind(theta(i)); -sind(theta(i)) (-cosd(theta(i))+1)];
            txty = inv(A)\cxy_offsets;
            tx(i) = txty(1);
            ty(i) = txty(2);
        end
        tx(isnan(tx)) = 0;
        ty(isnan(ty)) = 0;

        %[sx sy] = size(I);
        sx=M;sy=N;
        phase_x = exp(1j*2*pi*tx*([1:sx]/sx));
        phase_y = exp(1j*2*pi*ty*([1:sy]/sy));

        %% off-center rotation
        rot_dsinc_real = myrotate_mb(real(I), -theta);
        rot_dsinc_imag = myrotate_mb(imag(I), -theta);
        rot_dsinc = rot_dsinc_real+1j*rot_dsinc_imag;

        kc_rot_txy = fftshift(fft2(rot_dsinc));

        kc_rot_final = kc_rot_txy.*phase_x.'.*phase_y;
        out = ifft2(fftshift(kc_rot_final));
        %%%%%%%%%%%%%%%%%
    else
        tX = rowCenter;
        tY = colCenter;
        tTranslationToCenterAtOrigin = [1 0 0; 0 1 0; -tX -tY,1];
        tTranslationBackToOriginalCenter = [1 0 0; 0 1 0; tX tY,1];
        tRotation = [cosd(theta) -sind(theta) 0; sind(theta) cosd(theta) 0; 0 0 1];
        tformCenteredRotation = tTranslationToCenterAtOrigin*tRotation*tTranslationBackToOriginalCenter;
        tformCenteredRotation = affine2d(tformCenteredRotation);
        [out,Rout] = imwarp(I,tformCenteredRotation,'interp',interpType, 'OutputView', outSize);
    end
else
    out=I;%nothing is changed if theta==0
end
end