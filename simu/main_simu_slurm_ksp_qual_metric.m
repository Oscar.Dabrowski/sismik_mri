function main_simu_slurm_ksp_qual_metric(SLURM_ID, dataset)
%SLURM_ID will play the role of PE line

%specify the SLURM_IDs like
  %5,20,35,50,65,80,95,110,140, 155, 170, 185, 200, 215, 230, 245
rng('shuffle');

addpath('/home/users/d/dabrowsk/simu_postdoc/scripts/sismik_mri/utils/');

if SLURM_ID==110
  nb_cpu=9
else
  nb_cpu = 14;
end

PEStart = SLURM_ID;

PEEnd = PEStart+nb_cpu;
if PEEnd > 250
    PEEnd = 250;
end

PELines = PEStart:PEEnd

parfor PELine=PELines

    simuFullAcq_yggdrasil(PELine, dataset)

end

PELines
end
