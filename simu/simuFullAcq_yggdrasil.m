function simuFullAcq_yggdrasil(PELine, dataset)
% dataset : string : e.g. 'train' or 'val' or 'test'

%%% NOTE: finally i will only simulate a training set and a validation set on OTHER subjects than the tr set
%% the 'test' set will be choco in vivo
%
% i abandonnmed the idea of a simulated val set on same acq as training because of no real difference )guaranteed to work so no use
switch dataset
case 'train'
nb_samples=100
  offset=0
  subjectsFile = 'subjects_train.txt'
case 'val'
  nb_samples=20
  offset=1000
  subjectsFile = 'subjects_val.txt'
case 'test' % not gonna use this finally ... -> will use in vivo for the unique and true test set
  nb_samples=25
  offset=10000
  subjectsFile = 'subjects_test.txt'
otherwise
  error('no dataset recognized: should be train, val or test')
end

  
rng(PELine+offset);

%'standard' (empirical) center offset for all simu
cx_offset = 35;
cy_offset = 0;
startAtZero = true;
speed = 0;

angles = randn(1,nb_samples)*2;%[ 0 0.2 0.3 0.5 1 2 3 4];


%%%%%% chocoNomo_srcDir =  '/home/users/d/dabrowsk/nomoData_chocoSubjectsOnly/';

dstDir =  [ '/home/users/d/dabrowsk/simu_postdoc/data/simu_ksp_qual_metric_forDNN/' dataset '/' ];
mkdir(dstDir)

pNorm = 0.5;



%%%%%%%% TODO
%pre-load for speed


files = readSubjFile(subjectsFile);
nbSubjects = length(files)

emptySubjectStruct.name = '';
emptySubjectStruct.data = [];

chocoSubjects = repmat(emptySubjectStruct, [1 nbSubjects]);


%format :
%chocoSubjects(1).name = <string>
%chocoSubjects(1).data = load(...

for subjIdx = 1:nbSubjects

 [_folder _name _ext] = fileparts( files{subjIdx} )
 chocoSubjects(subjIdx).name = _name
 chocoSubjects(subjIdx).data = load(files{subjIdx})
end
			      



for angleIdx = 1:length(angles)

    for subjIdx = 1:nbSubjects


        curr_angle = angles(angleIdx);
        currAngleStr = num2str(curr_angle);

        currSubj = chocoSubjects(subjIdx).data;
        currSubjName = chocoSubjects(subjIdx).name;
        C = currSubj.C;
        ks = currSubj.ks;
        nbSlices = size(ks,4);

        ksp_corrupted_combined_all_nearest = zeros(256,256, nbSlices);

        rnd = rand();
        if rnd > 0.5
            speed = 2;
        else
            speed = 0;
        end

        simu_labels =  getSimuLabels(curr_angle,speed,cx_offset,cy_offset, PELine, startAtZero);



        %need all slices for nearest because ksp 'nearest' used for ksp
        %quality metric
        for sliceIdx=1:nbSlices

            [ksp_corrupted_combined_nearest, ksp_corrupted_nearest, ksp_nomo_slice_nearest] = ...
                simuRigidMulticoil_latest_withPreRot_v2(ks,C, simu_labels, sliceIdx, 'nearest');

            ksp_corrupted_combined_all_nearest(:,:,sliceIdx)=fftshift(ksp_corrupted_combined_nearest).';

        end

        out.nomoFile = currSubjName;
        out.angle = curr_angle;
        out.PELine = PELine;
        out.pnorm = computeLpNorm(ksp_corrupted_combined_all_nearest, pNorm);
        out.kspMeanAbs = mean(abs(ksp_corrupted_combined_all_nearest),3);

        %out.ksp_corrupted_combined_all_nearest = ksp_corrupted_combined_all_nearest;
        out.simu_labels = simu_labels;
        
        rndVal= randi([1e6]);
        fn = [ currSubjName '_simu' '_angle' currAngleStr 'deg'  '_PE' num2str(PELine) '_speed' num2str(speed) '_' 'cx_offset' num2str(cx_offset) '_cy_offset' num2str(cy_offset) '_rndIdx' num2str(rndVal) ]
        save( fullfile(dstDir,[fn '.mat']), 'out', '-v7.3')
        clear out;
        
    end

    angleIdx
end




%%

%%


    function chocoStr = extractChocoSubstring(fileName)
        % delim =  "_2023"
        chocoPattern = '(choco[^_]+_[^_]+_[^_]+)(?=_2023)';
        chocoMatch = regexp(fileName, chocoPattern, 'tokens');

        if ~isempty(chocoMatch)
            chocoStr = chocoMatch{1}{1};
        else
            chocoStr = '';
        end
    end

    function simu_labels =  getSimuLabels(thetaVal,speed,cx_offset,cy_offset, idxPELine, startAtZero )
        theta = zeros(1,256);
        if startAtZero
            theta(idxPELine:end) = thetaVal;
        else
            theta(1:idxPELine) = thetaVal;
        end
        [theta_out, tx,ty] = genSimuTrajWithSpeed(speed, theta, cx_offset,cy_offset, idxPELine);
        simu_labels.theta=theta_out;
        simu_labels.cx_offset = cx_offset;
        simu_labels.cy_offset = cy_offset;
        simu_labels.tx=tx;
        simu_labels.ty=ty;
    end

    function [speed, angle, PE] = extractSimuParamsFromFn(fileName)
        % extract speed val
        speedPattern = 'speed(\d+)';
        speedMatch = regexp(fileName, speedPattern, 'tokens');
        if ~isempty(speedMatch)
            speed = str2double(speedMatch{1}{1});
        else
            speed = NaN;
        end

        %Extract the angle value, taking into account the underscore as decimal point
        anglePattern = 'theta(\d+)_?(\d*)deg';
        angleMatch = regexp(fileName, anglePattern, 'tokens');
        if ~isempty(angleMatch)
            anglePart1 = angleMatch{1}{1};
            anglePart2 = angleMatch{1}{2};
            if isempty(anglePart2)
                angle = str2double(anglePart1);
            else
                angle = str2double([anglePart1, '.', anglePart2]);
            end
        else
            angle = NaN;
        end

        %Extract PE value
        PEPattern = 'PE(\d+)';
        PEMatch = regexp(fileName, PEPattern, 'tokens');
        if ~isempty(PEMatch)
            PE = str2double(PEMatch{1}{1});
        else
            PE = NaN;
        end
    end


end
