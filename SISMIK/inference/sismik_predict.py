import os,sys,time
sys.path.append('../')

import argparse
import re
from inferUtils import *
from models_latest import *
from brainThresh_custom import *

#############################################################################################################################
##### This script is for making predictions with SISMIK models.
#############################################################################################################################
# we recommend downloading the pretrained models (see README.md)
# Below, we provide examples showing how to call this script with command-line arguments
#  for in vivo or simulated motion-corrupted k-spaces
#  these example show calls with a proposed directory hierarchy as shown in README.md, i.e.
#  in your home (e.g. /home/od/ for me) git clone the git repo in /home/od/git/sismik_mri/ then
#  observed the proposed examples to see where we put pretrained models, data and results (in /home/od/git/sismik_mri/data/...)
#
#  Remarks:
#   - here we use model instances based on the 'SISMIK_v1' model
#   - for k-spaces corrupted with multiple motion events, we specify one SISMIK instance
#         for each PELine specified (comma-separated) (see example calls below)
#   - ideally, try to read all the comments if any doubts
#
# For questions please write to: oscar.dabrowski@unige.ch and look at the README for how to cite the SISMIK and ChoCo papers.
#############################################################################################################################
#############################################################################################################################

#example calls:
#
#>>>>>>> single mo evt (in vivo):
#
#python3 sismik_predict.py --modelPaths /home/od/git/sismik_mri/data/pretrained_models/SISMIK_pretrained_PE75_winSz9/ --PELines 75 --windowSize 9 --corruptedKsp /home/od/git/sismik_mri/data/inVivo/chocodb_v2/chocodb_230425_1530/SE_TR500ms_TE12ms_1MoEvt/chocodb_4deg_PE75.mat --isSimu 0 --dstDir /home/od/git/sismik_mri/data/results/inference/invivo/ --useSISMIKv1 1
#
#
#
#>>>>>> multi-mo evts (simulated):
#
#python3 sismik_predict.py --modelPaths /home/od/git/sismik_mri/data/pretrained_models/SISMIK_pretrained_PE75_winSz9/,/home/od/git/sismik_mri/data/pretrained_models/SISMIK_pretrained_PE90_winSz9/,/home/od/git/sismik_mri/data/pretrained_models/SISMIK_pretrained_PE105_winSz9 --PELines 75,90,105 --windowSize 9 --corruptedKsp /home/od/git/sismik_mri/data/simu/multipleMoEvt/multi_mo_event_exp/PE75_PE90_PE105/multiMoSimuNearest_3evt_v5.mat --isSimu 1 --dstDir /home/od/git/sismik_mri/data/results/inference/simu/ --useSISMIKv1 1 --dstFn chocodb_3evt_PE75_90_105
#
#
#
#>>>>>> in vivo multi-motion example:
#
#python3 sismik_predict.py \
#    --modelPaths /home/od/git/sismik_mri/data/pretrained_models/SISMIK_pretrained_PE75_winSz9/,\
#/home/od/git/sismik_mri/data/pretrained_models/SISMIK_pretrained_PE90_winSz9/,\
#/home/od/git/sismik_mri/data/pretrained_models/SISMIK_pretrained_PE105_winSz9 \
#    --PELines 74,91,104 \
#    --windowSize 9 \
#    --corruptedKsp /home/od/git/sismik_mri/data/inVivo/chocodb_v2/chocodb_231122_1530/SE_TR500ms_TE12ms_3MoEvt/chocodb_231122_1530_multiMo3evt_v1.mat \
#    --isSimu 0 \
#    --dstDir /home/od/git/sismik_mri/data/results/inference/invivo/ \
#    --useSISMIKv1 1 \
#    --dstFn chocodb_231122_1530_3evt_PE75_90_105
#
#(same as above. in one line:
#python3 sismik_predict.py --modelPaths /home/od/git/sismik_mri/data/pretrained_models/SISMIK_pretrained_PE75_winSz9/,/home/od/git/sismik_mri/data/pretrained_models/SISMIK_pretrained_PE90_winSz9/,/home/od/git/sismik_mri/data/pretrained_models/SISMIK_pretrained_PE105_winSz9/ --PELines 74,91,104 --windowSize 9 --corruptedKsp /home/od/git/sismik_mri/data/inVivo/chocodb_v2/chocodb_231122_1530/SE_TR500ms_TE12ms_3MoEvt/chocodb_231122_1530_multiMo3evt_v1.mat --isSimu 0 --dstDir /home/od/git/sismik_mri/data/results/inference/invivo/ --useSISMIKv1 1 --dstFn chocodb_231122_1530_3evt_PE75_90_105 


if __name__=='__main__':

    ############################## CUDA ##################################
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    devName=torch.cuda.get_device_name(device)
    print('----------------------------------------------------------------')
    print('------> running on device: ', device, ', device name: ', devName)
    print('----------------------------------------------------------------')
    time.sleep(1.0)
    ############################### ADUC ##################################

    #little lambda to parse PELines string
    convert_to_array = lambda s: np.array(list(map(int, s.split(','))))
    convert_to_list = lambda s: list(map(int, s.split(',')))

    #lambda to extract target angle value from filename (for ChoCo in vivo acquisisions in which subjects tried to achieve
    #a target angle -> epi_labels are the actual labels that subjects effectively performed.)
    extract_angle = lambda filename: re.search(r'(\d+)deg', filename).group(1) if re.search(r'(\d+)deg', filename) else None
    extract_ChoCoDirPattern = lambda path: re.search(r'chocodb_[^/]+_[^/]+', path).group() if re.search(r'chocodb_[^/]+_[^/]+', path) else None
    replace_chocodb = lambda original_string, new_substring: original_string.replace('chocodb', new_substring)
    
    parser = argparse.ArgumentParser(description = 'Inference with SISMIK')

    #modelPaths can be : one string with a single model for 1 mo event or multiple models paths for multiple mo events
    parser.add_argument('--modelPaths', type = str)#without filename (default is checkpoint.pth)
    parser.add_argument('--modelName', type = str, default='checkpoint.pth')
    #-----> PELines: vector of (possibly one) corrupted phase lines (i.e., if there are 3 motion events with motion onset 
    #at PE lines 75, 90 and 105 then the cmd-line-argument should be: --PELines 75,90,105 (NO SPACES, only commas between PE lines !!!)
    parser.add_argument('--PELines', type = str)# <---- comma separated ! no spaces ! e.g. : --PELines 75,90,105

    parser.add_argument('--windowSize', type = int)
    parser.add_argument('--corruptedKsp', type=str)#FULL PATH TO CORRUPTED KSPACE ACQUISITION (either simu or in vivo)
    parser.add_argument('--isSimu', type=int, choices=[0, 1])
    parser.add_argument('--dstDir', type = str)#destination directory in which to save everything
    #dstFn : DeStinatioN FileName
    parser.add_argument('--dstFn', type=str, default=None)#if specified, dstFn overrides other naming conventions we have

    #parser.add_argument('--x_format', type = str)
    #parser.add_argument('--y_format', type = str)
    #parser.add_argument('--nbConvLayers',type=int, default=7)#number of conv kernels (variable nb kernels in latest SISMIK model)
    #parser.add_argument('--nbconvFilters', type=int, default=128)#will set this number as the number of filters for ALL conv layers

    #sismik internal params: usually can be kept by default (but must of course match actual trained model params...)
    parser.add_argument('--ks', type=int, default=3)#(conv) kernel size
    parser.add_argument('--nb_channels_in', type=int, default=2)#default: real and imaginary
    parser.add_argument('--dropoutProb', type=float, default=0.25)
    #!!!!!!!! >>> COMMA-SEPARATED like PELINES -> because will be converted to array by lambda func convert_to_array  (NO spaces) !!!!!
    parser.add_argument('--nb_kernels', type=str, default='128,128,128,128,128,128,128')#7 conv layers with 128 conv filters/layer
    parser.add_argument('--nb_neuronsDense', type=str, default='256,256,3')
    #!!!!!!!! sismik v1 is first version that worked the best up to now -> see models_latest.py 
    parser.add_argument('--useSISMIKv1', type=int, choices=[0, 1], default=1)#overrides nb_kernels, nb_neurons...                    
    parser.add_argument('--coilConfig', type=str, default='espirit')#can be espirit (i.e. combined coils) or coil

    args = parser.parse_args()
    args.offset = args.windowSize//2
    args.isSimu = bool(args.isSimu)

    os.makedirs(args.dstDir, exist_ok=True)
    
    print('\n>>>>> argparse got args: ', args)

    fn = args.corruptedKsp
    dispErrors=False
    
    if args.isSimu:
        ksp_corrupted_combined, labels =loadSimuFile_noSlMsk(fn)
        #sliceMask, outliers, s, std_outliers, img_filt, img_thresh
        sliceMask = torch.from_numpy( getValidSlices_brain(ksp_corrupted_combined)[0] )
        relative_angles=labels['relative_angles']
        relative_translations=labels['relative_ty']
        labels['angles']=relative_angles
        labels['translations']=relative_translations
        dispErrors=True
    else:
        ksp_corrupted_combined=loadInvivoFile(fn)
        #for inVivo, coil-combined, -> we consider that all slices are valid
        #hence mask is simply a vector of ones of the number of slices
        sliceMask = torch.from_numpy( getValidSlices_brain(ksp_corrupted_combined)[0] )#torch.ones(ksp_corrupted_combined.shape[0])
        #ChoCo empirical labels valid for one session (multiple in vivo acquisitions of the same subject)
        labelsDir = os.path.dirname(fn)#same dir as all ksp for a given subject    
        if 'epi_labels.mat' in os.listdir(labelsDir):
            epiLabels_name = os.path.join(labelsDir,'epi_labels.mat')#all in vivo labels are named epi_labels.mat by convention
            with h5py.File(epiLabels_name, 'r') as file:
                labels = hdf5_to_dict(file)['epi_labels']
                #assuming that the target angle value is in filename in the format *_xdeg_*.mat
                choco_target_angle=extract_angle(fn)
            #only valid for our "target 2,3,4" single-motion experiments where labels are stored sequentially in epi_labels.mat
            if choco_target_angle is not None:
                #for single motion events
                angleIdxLabel=int(choco_target_angle)-2 #2deg -> idx=0 , 3deg -> idx=1 4deg-> idx=2
                translIdxLabel=angleIdxLabel#translation is associated with angle
                dispErrors=True#only displayable if found choco_target_angle numeric value in filename
            
    print('>>> debug >>> sliceMask:', sliceMask, ', (#valid sl. = ', torch.sum(sliceMask),')')

    modelPaths = args.modelPaths.split(',')#assuming each model (comma separated) matches the corresp PELine in PELines (same order!)
    PELines = convert_to_list(args.PELines)
    #REMARK: assuming same hyperparams (i.e. nb kernels and neurons) for each model (instance) if multiple models (i.e. for multiple mo evts)
    args.nb_kernels = convert_to_list(args.nb_kernels)
    args.nb_neuronsDense = convert_to_list(args.nb_neuronsDense)

    predictions={'angle':[],'translation':[],'PELine':[], 'errors_angle':[], 'errors_translation' : []}

    modelIdx = 0

    #because of Matlab's 1-based indexing (because using matlab convention when refering to PE lines)
    #i.e. if we consider 1 mo event at line 75, we use the sismik model 75 and give 75 as cmd-line arg
    # - (see script call example at the beginning of this script) -
    #but the effective line for python will be 75-1=74 
    PELines = list(np.array(PELines) -1)
    
    for PELine in PELines:
        ###
        #load sismik model the path provided as cmd-line arg
        if bool(args.useSISMIKv1):
            #sismik 'v1' is first version that worked the best up to now -> see models_latest.py
            model = getSISMIKModel(modelPath=modelPaths[modelIdx],v1=True)
        else:
            #allows varying nb layers and kernels to try other architectures
            model = getSISMIKModel(modelPath=modelPaths[modelIdx],
                                   ks=args.ks,
                                   nb_phase_rows=args.windowSize,
                                   nb_channels_in=args.nb_channels_in,
                                   dropoutProb=args.dropoutProb,
                                   nb_kernels=args.nb_kernels,
                                   nb_neuronsDense=args.nb_neuronsDense,
                                   modelName = args.modelName)
        model = model.cuda().eval()
        idxRange = computeIdxRangeFromOffset(offset=args.windowSize//2, PELine=PELine)
        with torch.no_grad():
            #predict...
            preds = predictFromModel_any(model=model,
                                         ksp_corrupted_combined=ksp_corrupted_combined.clone(),
                                         PELine=PELine,
                                         windowSize=args.windowSize,
                                         sliceMask=sliceMask,
                                         device=device,
                                         isSimu=args.isSimu)
            angle_pred = torch.median(preds[:,0]).cpu().item()
            transl_pred = torch.median(preds[:,2]).cpu().item()
            #debug
            print('----------------- (Debug) ----------------')
            print('Angle predictions (all):', preds[:,0])
            print('Transl predictions (all):', preds[:,2])
            print('------------------------------------------')
            #
            print('\n\n\n')
            print('-********************************************************************-')
            print('----------------- Results --------------------------------------------')
            print('----------------------------------------------------------------------')
            print('Angle preds for PELine=', PELine,' :',"{:.2f}".format(angle_pred))
            print('Translation preds for PELine=', PELine,' :', "{:.2f}".format(transl_pred))
            if dispErrors:
                if args.isSimu:
                    angle_error = abs(angle_pred-labels['angles'][modelIdx].squeeze())
                    transl_error = abs(transl_pred-labels['translations'][modelIdx].squeeze())
                    print('( Simu labels (relative, all):')
                    print('Angles:', ["{:.2f}".format(ang[0]) for ang in labels['angles']])
                    print('Translations:', ["{:.2f}".format(tr[0]) for tr in labels['translations']])
                    print(')')
                    print('Angle absolute error (degrees):', "{:.2f}".format(angle_error))
                    print('Translations abs error (pixels):', "{:.2f}".format(transl_error))
                else:
                    print('DEBUG: ->>>>>>>>>>>>>>>>>>>>>')
                    
                    angle_error = abs(angle_pred-labels['theta'].squeeze()[angleIdxLabel])
                    transl_error = abs(transl_pred-labels['tx'].squeeze()[translIdxLabel])
                    print('ANGLE error=',angle_error)
                    #Single motion events (1 EPI labels (training session) : ChoCo protocol assumes subjects
                    #performed almost same mo. params for all acquisisions in same MRI session)
                    print('epi_labels (all):')
                    print(' - angles (target 2,3,4 deg):',
                          "{:.2f}".format(labels['theta'].squeeze()[0]),'deg,',
                          "{:.2f}".format(labels['theta'].squeeze()[1]),'deg,',
                          "{:.2f}".format(labels['theta'].squeeze()[2]), 'deg,')
                    print(' - translation (for each above angle):',
                          "{:.2f}".format(labels['tx'].squeeze()[0]),'deg,',
                          "{:.2f}".format(labels['tx'].squeeze()[1]),'deg,',
                          "{:.2f}".format(labels['tx'].squeeze()[2]), 'deg,')
                    #NOTE: for SINGLE motion events : angleIdxLabel and translIdxLabel
                    print('Angle absolute error (degrees):', "{:.2f}".format(angle_error))
                    print('Translations abs error (pixels):', "{:.2f}".format(transl_error))
            print('---------------------------------------------------------------------')
            print('-*********************************************************************-')
            print('\n\n')

            predictions['angle'].append(angle_pred)
            predictions['translation'].append(transl_pred)
            predictions['PELine'].append(PELine)
            if dispErrors:
                predictions['errors_angle'].append(angle_error)
                predictions['errors_translation'].append(transl_error)
            
            modelIdx+=1

    chocoDirId = extract_ChoCoDirPattern(fn)
    print('>>>>> Regexp found chocoDirId in filename:', chocoDirId, '(make sure it is correct) <<<<<')
    if len(PELines)==1:
        #if dstFn arg was user defined it will override:
        if args.dstFn is not None:
            dstFn= args.dstFn+'.pth'
        else:#try to extract a relevant filename from the base input filename...
            dstFn = os.path.splitext(os.path.basename(fn))[0]+'_sismik_predictions.pth'
            dstFn = replace_chocodb(dstFn, chocoDirId)#this is to avoid having 2 times the 'chocodb' string in the name
    else:
        dstFn= args.dstFn+'.pth' if args.dstFn is not None else 'multiple_mo_evt.pth' 
    torch.save(predictions, os.path.join(args.dstDir,dstFn))

    dstFn_h5 = os.path.join(args.dstDir,dstFn).split('.')[0]+'.h5'
    with h5py.File(dstFn_h5, 'w') as file:
        for key, value in predictions.items():
            file.create_dataset(key, data=value)
        
    print('>>>>> Results saved in: ', os.path.join(args.dstDir,dstFn), '\n and:', dstFn_h5)
    
    '''
        #e.g. call for 3 mo events: >>> python3 multiMoEvt_sismik_pred.py 3 multiMoSimuNearest_3evt_v4.mat

    dstDir = os.getcwd()
    lambda_normalize = lambda x : (x-torch.min(x))/(torch.max(x)-torch.min(x))
    torchvision.utils.save_image(lambda_normalize(torch.abs((ksp_corrupted_combined[5,:,:]))), os.path.join(dstDir, '_abs_ifft2_testSlice5.png'))
    torchvision.utils.save_image(lambda_normalize(torch.abs((ksp_corrupted_combined[7,:,:]))), os.path.join(dstDir, '_abs_ifft2_testSlice7.png'))
    torchvision.utils.save_image(lambda_normalize(torch.abs((ksp_corrupted_combined[8,:,:]))), os.path.join(dstDir, '_abs_ifft2_testSlice8.png'))

    '''
    
    
    
