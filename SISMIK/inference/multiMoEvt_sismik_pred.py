import os,sys,time

from inferUtils import *

###############################################################################
#e.g. call: python3 multiMoEvt_sismik_pred.py 3 multiMoSimuNearest_3evt_v4.mat
###############################################################################


if __name__=='__main__':

    print('Entered in', __name__)
    
    ############################## CUDA ##################################
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    devName=torch.cuda.get_device_name(device)
    print('----------------------------------------------------------------')
    print('------> running on device: ', device, ', device name: ', devName)
    print('----------------------------------------------------------------')
    time.sleep(1.0)
    ############################### ADUC ##################################


    #Note: some params are a bit hardcoded for quick tests...
    
    if int(sys.argv[1])==2:
        PELines = [ 75, 90]
        fn = '../../data/multi_mo_event_exp/PE75_PE90/'+sys.argv[2]#e.g.: multiMoSimuNearest_2evt_v2.mat'
    elif int(sys.argv[1]) == 3:
        PELines = [ 75, 90, 105]
        fn = '../../data/multi_mo_event_exp/PE75_PE90_PE105/'+sys.argv[2]#e.g.: multiMoSimuNearest_3evt_v4.mat'
    else:
        raise ValueError('>>>>>>>> ERROR: only 2 or 3 accepted as 1st arg (number of mo events) <<<<<<<<')
        
    windowSize=9
    #modelSize = 'medium'

    ksp_corrupted_combined, labels =loadSimuFile_noSlMsk(fn)
    sliceMask = torch.ones(15)#special case for these simu with 15 slices

    
    for PELine in PELines:
        print('debug: pe=',PELine)
        #model = getSISMIKModel_best(PELine, offset)
        model = model.cuda()
        model.eval()
        idxRange = computeIdxRangeFromOffset(offset=windowSize//2, PELine=PELine)
        print(idxRange)
        with torch.no_grad():
            #predictFromModel_any(model, ksp_corrupted_combined,PELine, windowSize, sliceMask, device, isSimu=True):
            preds = predictFromModel_any(model=model,
                                         ksp_corrupted_combined=ksp_corrupted_combined.clone(),
                                         PELine=PELine,
                                         windowSize=windowSize,
                                         sliceMask=sliceMask,
                                         device=device)
        angle_pred = torch.median(preds[:,0])
        ty_pred = torch.median(preds[:,2])
        print('-----------------------------------------------')
        print('Angle preds for PELine=', PELine,' :',angle_pred)
        print('Ty preds for PELine=', PELine,' :', ty_pred)
        print('-----------------------------------------------')


        #e.g. call for 3 mo events: >>> python3 multiMoEvt_sismik_pred.py 3 multiMoSimuNearest_3evt_v4.mat

    dstDir = os.getcwd()
    lambda_normalize = lambda x : (x-torch.min(x))/(torch.max(x)-torch.min(x))
    torchvision.utils.save_image(lambda_normalize(torch.abs((ksp_corrupted_combined[5,:,:]))), os.path.join(dstDir, '_abs_ifft2_testSlice5.png'))
    torchvision.utils.save_image(lambda_normalize(torch.abs((ksp_corrupted_combined[7,:,:]))), os.path.join(dstDir, '_abs_ifft2_testSlice7.png'))
    torchvision.utils.save_image(lambda_normalize(torch.abs((ksp_corrupted_combined[8,:,:]))), os.path.join(dstDir, '_abs_ifft2_testSlice8.png'))
