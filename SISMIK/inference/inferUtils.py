import sys, os, time
sys.path.append('../')
from customDatasetUtils import *
from custom_utils import *
from models_latest import *
import re
import pickle

###############################################

def loadSimuFile_noSlMsk(fn):
    simuFile = h5py.File(fn, 'r')
    print(simuFile.keys())
    labels = hdf5_to_dict(simuFile['labels'])
    ksp_corrupted_combined = torch.tensor(
        np.array(simuFile['ksp_corrupted_combined_all']['real'])) + 1j*torch.tensor(np.array(simuFile['ksp_corrupted_combined_all']['imag']))
    #sliceMask = torch.tensor(np.array(simuFile['sliceMask']))
    return ksp_corrupted_combined, labels#, sliceMask


def getSISMIKModel(modelPath,
                   ks=3,
                   nb_phase_rows=9,
                   nb_channels_in=2,
                   dropoutProb=0.25,
                   nb_kernels=[128,128,128,128,128,128,256],
                   nb_neuronsDense=[256,256,3],
                   modelName = 'checkpoint.pth',
                   v1=False):
    if v1:
        print('>>>>>>>> loading sismik v1 <<<<<<<<<<')
        model=SISMIK_v1()
    else:
        model = SISMIK(ks = ks,
                       nbr = nb_phase_rows,
                       nb_ch_in=nb_channels_in,
                       dropoutProb=dropoutProb,
                       nb_kernels=nb_kernels,
                       nb_neuronsDense=nb_neuronsDense)
    loadModel(model, os.path.join(modelPath,modelName))
    return model.eval()


#should work with any (i.e. simu or invivo)
def predictFromModel_any(model, ksp_corrupted_combined,PELine, windowSize, sliceMask, device, isSimu):
    offset=windowSize//2
    print('isSimu=', isSimu)
    if isSimu:
        ksp_corrupted_combined = torch.permute(ksp_corrupted_combined,dims=(0,2,1))
    idxRange = computeIdxRangeFromOffset(offset=offset, PELine=PELine)
    mapsDir = getNormalizationMapsDir()
    #print('PELine=',PELine, 'slMsk=', sliceMask)
    return predictFromModel(model, idxRange, ksp_corrupted_combined, sliceMask, mapsDir, PELine, offset, device)


def normalizeMinMax(x):
    x = (x-x.min())/(x.max()-x.min())
    return x

def loadModel(model, modelFile):
    checkpoint=torch.load(modelFile)
    model.load_state_dict(checkpoint['model_state'])

def doOcclusion(x,i,j,value=0):
    #assume does not yet have 2 channels, still in complex form read from matfile
    x[:,i,j]=value
    return x

def createOcclusionMask(x,q):
    #m=torch.max(x)
    qq = torch.quantile(x,q)
    mask = x >= qq
    return mask.float()
    
def createOcclusionMask2(x,val):
    mask = x >= val
    return mask.float()
    
def repmat(x,n, d=0):
    return torch.stack([x] * n, dim=d)


def getChocoSubjectNameFromIdx(chocoIdx):
    tmp = ['chocodb_230425_1530', 'chocodb_230427_1930', 'chocodb_230502_1445', 'chocodb_230530_1400', 'chocodb_230516_1715']
    return tmp[chocoIdx-1]#+1 because of pythons 0-based indexing


def getChocoDataFileName(chocoDir, angleVal, PELine):
    tol=5
    #in the case a PELine close two the 2 lines used in the invivo tests
    #sometimes e.g. subject 2 (choco 19h30) is rahter 74 than 75 but
    #the filenames are still named '75'
    if PELine in range(75-tol, 75+tol):
        PELine=75
    elif PELine in range(90-tol, 90+tol):
        PELine=90
    else:
        raise ValueError('Not implemented for PE lines other than 75 or 90')
    
    if angleVal == 0:
        # Pattern for files with noMo and without "0deg" or "PELine"
        pattern = re.compile(rf'noMo.*\.mat$', re.IGNORECASE)
    else:
        # General pattern for other angleVals
        #pattern = re.compile(rf'{angleVal}deg.*_PE{PELine}\.mat$', re.IGNORECASE)
        pattern = re.compile(rf'{angleVal}deg.*_PE{PELine}(\.dat)?\.mat$', re.IGNORECASE)
    matching_files = []
    
    for dirpath, _, filenames in os.walk(chocoDir):
        for filename in filenames:
            if pattern.search(filename):
                full_path = os.path.join(dirpath, filename)
                matching_files.append(full_path)

    return matching_files


def hdf5_to_dict(hdf5_file):
    result = {}
    for key, value in hdf5_file.items():
        if isinstance(value, h5py.Dataset):
            result[key] = np.array(value)
        elif isinstance(value, h5py.Group):
            result[key] = hdf5_to_dict(value)
    return result


def loadSimuFile(fn, coilType='combined'):
    simuFile = h5py.File(fn, 'r')
    print(simuFile.keys())
    labels = hdf5_to_dict(simuFile['labels'])
    if coilType=='combined':
        ksp_corrupted_combined = torch.tensor(
            np.array(simuFile['ksp_corrupted_combined_all']['real']))
        + 1j*torch.tensor(np.array(simuFile['ksp_corrupted_combined_all']['imag']))
    else:
        raise ValueError('!!!!!!!!! loadSimuFile not implemented for indiv. coil !!!!!')
    #expecting multiple slices, hence (binary) mask to specify relevant ones
    sliceMask = torch.tensor(np.array(simuFile['sliceMask']))
    return ksp_corrupted_combined, labels, sliceMask


def loadInvivoFile(fn):
    invivoFile = h5py.File(fn, 'r')
    print('>>>>> in loadInvivoFile: ','fn=',fn,', keys=',invivoFile.keys())
    #labels = hdf5_to_dict(invivoFile['labels'])
    ksp_corrupted_combined = torch.tensor(
        np.array(invivoFile['kc']['real'])) + 1j*torch.tensor(np.array(invivoFile['kc']['imag']))
    #multiple slices, as for simu, but all slices are by default relevant
    #(i.e., make sure it is the case before)
    #sliceMask = torch.tensor(np.array(invivoFile['sliceMask']))
    return ksp_corrupted_combined


def getSliceIdxFromMask(binaryMask):
    return torch.nonzero(binaryMask.squeeze() == 1, as_tuple=False).squeeze(1)


def predictFromModel(model, idxRange, ksp_corrupted_comb, sliceMask, mapsDir, PELine, offset, device):
    #sliceIdx: selected slices from sliceMask
    
    sliceIdx = getSliceIdxFromMask( sliceMask )
    print('valid slice indexes: ', sliceIdx)
    #ksp_corrupted_comb = testdataObj.ksp_corrupted_combined

    #print('ksp_corrupted_comb.shape = ',ksp_corrupted_comb.shape)
    #ksp_corrupted_comb = torch.permute( ksp_corrupted_comb[sliceIdx,:,:], dims = (0,1,2))#permute or not permute?

    ###ksp_corrupted_comb = torch.permute( ksp_corrupted_comb[:,idxRange,:].unsqueeze(0), dims = (1,0,2,3))
    ksp_corrupted_comb = torch.permute( ksp_corrupted_comb[:,idxRange,:].unsqueeze(0), dims = (1,0,2,3))[sliceIdx,:,:]
    
    x_real = torch.real(ksp_corrupted_comb)
    x_imag = torch.imag(ksp_corrupted_comb)
    x = torch.cat((x_real,x_imag),dim=1).float()
    #Normalize
    x = normalizeByMuStdMaps(x=x.cuda(), x_format='real_imag', PELine=PELine, offset=offset, device=device)#, mapsDir = mapsDir)
    #model=model.cuda()
    prediction = model(x)
    return prediction
