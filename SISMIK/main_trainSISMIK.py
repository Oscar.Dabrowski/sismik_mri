import numpy as np
import matplotlib
import matplotlib.pyplot as plt
#Agg : to avoid any graphics display if sismik is run from GUI
matplotlib.use('Agg')
from numpy import linalg
from skimage import data
from skimage.color import rgb2gray
import torch
from torch import optim, nn
from torch.nn import functional as F
import sys, os, time
from torchinfo import summary
from torch.utils.data import Dataset, DataLoader
import torchvision
from torchvision import datasets
from torchvision.transforms import ToTensor
import h5py
import pickle
import argparse
from custom_utils import *
from customDatasetUtils import *
from models_latest import *
######################################################################

#Note:
#instead of 'window_size', I used the 'offset' terminology:
#window_size == offset*2+1
#I did not modify it but it should be mentioned because the
#winSize terminology is used in the SISMIK paper and is a
#more straightforward terminology

######################################################################

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

devName=torch.cuda.get_device_name(device)

print('----------------------------------------------------------------')
print('------> running on device: ', device, ', device name: ', devName)
print('----------------------------------------------------------------')
time.sleep(1.0)
######################################################################

    
def trainFunc(train_dataloader, model, optimizer, surrogateLossFunc, current_epoch, offset, x_format, y_format, PELine):
    num_batches = len(train_dataloader)
    acc_sLossTrain = 0
    acc_corrTheta_train = 0
    acc_corrTx_train = 0
    acc_corrTy_train = 0
    accLoss_train_theta = 0
    accLoss_train_tx = 0
    accLoss_train_ty = 0
    cnt=0
    for batch_idx, batch in enumerate(train_dataloader):
            
        x_train = batch[0].float().to(device)
        y_train = batch[1].float().to(device)
        
        #normalized minibatch by mu std maps
        x_train = normalizeByMuStdMaps(x_train,x_format, PELine, offset, device)
        
        #last minibatch may have a different size due to
        #not being a multiple of the total trainingset size
        current_minibatch_size = batch[0].shape[0]
        
        out_train = model(x_train).squeeze()

        sloss_train = surrogateLossFunc(out_train,y_train)
        
        sloss_train.backward()
        optimizer.step()
           
        with torch.no_grad():
            #if nonscalar output
            if y_format == 'theta_tx_ty':
                accLoss_train_theta += surrogateLossFunc(out_train[:,0], y_train[:,0]).detach().item()
                accLoss_train_tx += surrogateLossFunc(out_train[:,1], y_train[:,1]).detach().item()
                accLoss_train_ty += surrogateLossFunc(out_train[:,2], y_train[:,2]).detach().item()
            elif y_format == 'tx_ty':
                accLoss_train_tx += surrogateLossFunc(out_train[:,0], y_train[:,0]).detach().item()
                accLoss_train_ty += surrogateLossFunc(out_train[:,1], y_train[:,1]).detach().item()
                
            acc_sLossTrain += sloss_train.detach().item()
            #output of corrcoef may contain in principle a tuple (accumulating) of 3 values:
            #acc_corrTheta, ...corrTx, ...corrTy
            #depenging on y_format some of them may be equal to 0
            acc_corrTheta_train, acc_corrTx_train, acc_corrTy_train = computeCorrcoef(out_train, y_train, y_format,
                                                                       acc_corrTheta_train,
                                                                       acc_corrTx_train,
                                                                       acc_corrTy_train)
                
        print(f"Epoch {current_epoch}, Batch {batch_idx+1}/{num_batches}")
        if current_epoch == 0 and cnt == 0:
            #easier to call the function there with a training example x from dataloader
            saveModelArchInfo(model, x_train, args.dstDir)
        cnt+=1
        #endfor
    return tuple2pack( model=model,
                       optimizer=optimizer,
                       acc_sLossTrain=acc_sLossTrain/cnt,
                       acc_corrTheta_train=acc_corrTheta_train/cnt,
                       acc_corrTx_train=acc_corrTx_train/cnt,
                       acc_corrTy_train=acc_corrTy_train/cnt,
                       accLoss_train_theta = accLoss_train_theta/cnt,
                       accLoss_train_tx = accLoss_train_tx/cnt,
                       accLoss_train_ty = accLoss_train_ty/cnt,
                       cnt=cnt )

#compute either validation or test loss (code should be same for both)
def valOrTestFunc(valOrTest_dataloader,
                  model,
                  lossFunc,
                  offset,
                  x_format,
                  y_format,
                  PELine,
                  current_epoch,
                  returnRandomMinibatch=True):
    accLoss_valOrTest = 0.0
    accCorrelTheta_valOrTest = 0.0
    accCorrelTx_valOrTest = 0.0
    accCorrelTy_valOrTest = 0.0
    accLoss_valOrTest_theta = 0.0
    accLoss_valOrTest_tx = 0.0
    accLoss_valOrTest_ty = 0.0
    cnt = 0
    mb = None
    with torch.no_grad():
        for batch_idx_, batch_ in enumerate(valOrTest_dataloader):
            x_valOrTest = batch_[0].float().to(device)
            y_valOrTest = batch_[1].float().to(device)
            x_valOrTest = normalizeByMuStdMaps(x_valOrTest,x_format, PELine, offset, device)
            out_valOrTest = model(x_valOrTest).squeeze()
            accLoss_valOrTest += lossFunc(out_valOrTest,y_valOrTest).detach().item()

            accCorrelTheta_valOrTest, accCorrelTx_valOrTest, accCorrelTy_valOrTest = computeCorrcoef(
                out_valOrTest,
                y_valOrTest,
                y_format,
                accCorrelTheta_valOrTest,
                accCorrelTx_valOrTest,
                accCorrelTy_valOrTest)

            #if nonscalar output
            if y_format == 'theta_tx_ty':
                accLoss_valOrTest_theta += surrogateLossFunc(out_valOrTest[:,0], y_valOrTest[:,0]).detach().item()
                accLoss_valOrTest_tx += surrogateLossFunc(out_valOrTest[:,1], y_valOrTest[:,1]).detach().item()
                accLoss_valOrTest_ty += surrogateLossFunc(out_valOrTest[:,2], y_valOrTest[:,2]).detach().item()
            elif y_format == 'tx_ty':
                accLoss_valOrTest_tx += surrogateLossFunc(out_valOrTest[:,0], y_valOrTest[:,0]).detach().item()
                accLoss_valOrTest_ty += surrogateLossFunc(out_valOrTest[:,1], y_valOrTest[:,1]).detach().item()
            
            print('>>>> val or test computation ... epoch#', current_epoch , ', batch#', batch_idx_)
            
            if cnt==0 and returnRandomMinibatch:
                mb = (x_valOrTest, y_valOrTest, out_valOrTest)
            cnt += 1
    cnt = float(cnt)
    return tuple2pack( accLoss_valOrTest=accLoss_valOrTest/cnt,
                       accCorrelTheta_valOrTest=accCorrelTheta_valOrTest/cnt,
                       accCorrelTx_valOrTest=accCorrelTx_valOrTest/cnt,
                       accCorrelTy_valOrTest=accCorrelTy_valOrTest/cnt,
                       accLoss_valOrTest_theta = accLoss_valOrTest_theta/cnt,
                       accLoss_valOrTest_tx = accLoss_valOrTest_tx/cnt,
                       accLoss_valOrTest_ty = accLoss_valOrTest_ty/cnt,
                       mb=mb)

def computeCorrcoef(out_, y_, y_format, acc_corrTheta_, acc_corrTx_, acc_corrTy_):
    if y_format == 'theta':
        corrTheta = computeCorrcoef1(out_.detach(), y_.detach())
        acc_corrTheta_ += corrTheta
    elif y_format == 'tx':
        corrTx = computeCorrcoef1(out_.detach(), y_.detach())
        acc_corrTx_ += corrTx
    elif y_format == 'ty':
        corrTy = computeCorrcoef1(out_.detach(), y_.detach())
        acc_corrTy_ += corrTy
    elif y_format == 'tx_ty':
        corrTx, corrTy = computeCorrcoef2(out_.detach(), y_.detach())
        acc_corrTx_ += corrTx
        acc_corrTy_ += corrTy
    elif y_format == 'theta_tx_ty':
        corrTheta, corrTx, corrTy = computeCorrcoef3(out_.detach(), y_.detach())
        acc_corrTheta_ += corrTheta
        acc_corrTx_ += corrTx
        acc_corrTy_ += corrTy
    return acc_corrTheta_, acc_corrTx_, acc_corrTy_



if __name__=='__main__':

    
    parser = argparse.ArgumentParser(description = 'main deepnet training script (single phase enc. line (PELine))')

    parser.add_argument('--epochs', type = int, default = 100)
    parser.add_argument('--minibatch_size', type = int, default = 128)
    #nb // workers for trainDataloader
    parser.add_argument('--numworkers_train', type = int, default = 16)
    parser.add_argument('--numworkers_val', type = int, default = 2)
    parser.add_argument('--numworkers_test', type = int, default = 2)
    #no default value on purpose to force specification
    parser.add_argument('--windowSize', type = int)
    parser.add_argument('--lr', type = float, default = 1e-6)
    parser.add_argument('--L2_reg', type = float, default = 1e-6)#Tikhonov regularization
    parser.add_argument('--PELine_train', type = int, default = 75)
    parser.add_argument('--PELine_val', type = int, default = 75)
    parser.add_argument('--PELine_test', type = int, default = 75)
    parser.add_argument('--dstDir', type = str)#destination directory in which to save everything
    parser.add_argument('--srcDirTrain', type = str)
    parser.add_argument('--srcDirVal', type = str)
    parser.add_argument('--srcDirTest', type = str)
    parser.add_argument('--mapsDir', type = str)
    parser.add_argument('--x_format', type = str)
    parser.add_argument('--y_format', type = str)
    parser.add_argument('--model_type', type = str, default='ks3_medium')
    parser.add_argument('--prefetch_factor', type=int, default=6)
    #Fn := File name
    parser.add_argument('--trainFn', type=str, default='train.pkl')
    parser.add_argument('--valFn', type=str, default='val.pkl')
    parser.add_argument('--testFn', type=str, default='test.pkl')
    parser.add_argument('--coilConfig', type=str, default='espirit')#can be espirit (i.e. combined coils) or coil
    #!!!!!!!!! NOTE: these 2 last args sort of override the 'model_type' arg which should be left to 'medium'
    parser.add_argument('--nbConvLayers',type=int, default=6)#number of conv kernels (variable nb kernels in latest SISMIK model)
    parser.add_argument('--nbconvFilters', type=int, default=128)#will set this number as the number of filters for ALL conv layers
    
    #possible values for x_format:
    #'real_imag'
    #'abs'
    #'phase'

    #possible values for y_format:
    #'theta'
    #'tx'
    #'ty'
    #'theta_tx_ty'
    #'tx_ty'
    
    #possible values for model_type:  (TODO)
    #REM: ks stands for 'kernel size' (all kernel sizes are the same for a given CNN type)
    #
    # 'ks3_v1' -> approx 4M params 'classic' LeNet style that worked for offset=2
    # 'ks3_small' -> nb params approx ...
    # 'ks3_medim' -> nb params approx ...
    #
    
    #this line adds the args as variables with the name specified after the '--' and the value provided
    #in cmd-line, or with the default value if no cmd-line provided (or with None if no default value)
    args = parser.parse_args()
    args.offset = args.windowSize//2
    
    print('\n>>>>> argparse got args: ', args)
    
    time.sleep(3.0)#some time to check that args are correct ... still time to ctrl+C
    
    ########################################################################



    print('starting deepNet training ...')

    dstDir_logs = os.path.join(args.dstDir,'logs')
    #no need for a dstDir_figsTrain: train curves saved in dstDir directly
    #dstDir_figsTrain = os.path.join(args.dstDir,'train_figs')
    dstDir_figsVal = os.path.join(args.dstDir,'val_figs')
    dstDir_figsTest = os.path.join(args.dstDir,'test_figs')
    os.makedirs(args.dstDir, exist_ok=True)
    os.makedirs(dstDir_logs, exist_ok=True)
    #os.makedirs(dstDir_figsTrain, exist_ok=True)
    os.makedirs(dstDir_figsVal, exist_ok=True)
    os.makedirs(dstDir_figsTest, exist_ok=True)
    
    nb_phase_rows = args.windowSize

    nbConvLayers=args.nbConvLayers
    nbconvFilters = args.nbconvFilters
    
    if args.x_format == 'abs' or args.x_format == 'phase':
        nb_channels_in = 1
    elif args.x_format == 'real_imag' or args.x_format == 'abs_phase':
        nb_channels_in = 2
    elif args.x_format == 'real_imag_correl':
        nb_channels_in = 4
        
    if args.y_format == 'theta' or args.y_format == 'tx' or args.y_format == 'ty':
        nb_neurons_out = 1
    elif args.y_format == 'tx_ty':
        nb_neurons_out = 2
    elif args.y_format == 'theta_tx_ty':
        nb_neurons_out = 3
        
    
    #!!!!! NOTE: the 'model_type' args is essentially overrided by the nbConvLayers and nbconvFilters args ...
    #... so just use the medium one and nbConvLayers and nbconvFilters will modulate its size ...
    if args.model_type == 'ks3_medium':
        print('--------> model : ks3_medium using ', nbConvLayers, ' nbConvLayers and ' , nbconvFilters,' nbconvFilters.')
        model = SISMIK(ks = 3, nbr = nb_phase_rows,
                                        nb_ch_in=nb_channels_in,
                                        dropoutProb=0.25,
                                        nb_kernels=nbConvLayers*[nbconvFilters],
                                        nb_neuronsDense=[256,256,nb_neurons_out])
    elif args.model_type == 'ks3_large':
        model = SISMIK(ks = 3, nbr = nb_phase_rows,
                                        nb_ch_in=nb_channels_in,
                                        dropoutProb=0.25,
                                        nb_kernels=nbConvLayers*[nbconvFilters],
                                        nb_neuronsDense=[256,256,nb_neurons_out])
    #should probably forget this one...
    #elif args.model_type == 'ks7_v1':
    #    nb_kernels_arg = [64,64,96,96,128,128,128,128,128,128]
    #    model = SISMIK(ks = 7, nbr = nb_phase_rows,
    #                                    nb_ch_in=nb_channels_in,
    #                                    dropoutProb=0.25,
    #                                    nb_kernels=nb_kernels_arg,
    #                                    nb_neuronsDense=[256,128,nb_neurons_out])
    #    nbConvLayers=len(nb_kernels_arg)


    print('------------> using :', nbConvLayers, ' conv layers.')
    
    surrogateLossFunc = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr = args.lr, weight_decay=args.L2_reg)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min')

    trainDataset=CustomDataset_v2_preload(dataDir=args.srcDirTrain,
                                          datasetName=args.trainFn,
                                          offset=args.offset,
                                          PELine=args.PELine_train,
                                          x_format=args.x_format,
                                          y_format=args.y_format,
                                          coilConfig=args.coilConfig)
    valDataset=CustomDataset_v2_preload(dataDir=args.srcDirVal,
                                        datasetName=args.valFn,
                                        offset=args.offset,
                                        x_format=args.x_format,
                                        PELine=args.PELine_val,
                                        y_format=args.y_format,
                                        coilConfig=args.coilConfig)
    testDataset=CustomDataset_v2_preload(dataDir=args.srcDirTest,
                                         datasetName=args.testFn,
                                         offset=args.offset,
                                         PELine=args.PELine_test,
                                         x_format=args.x_format,
                                         y_format=args.y_format,
                                         coilConfig=args.coilConfig)
    train_dataloader = DataLoader(trainDataset,
                                  batch_size=args.minibatch_size,
                                  shuffle=True,
                                  num_workers = args.numworkers_train)#,
                                  #prefetch_factor=args.prefetch_factor,
                                  #pin_memory=True)
    val_dataloader = DataLoader(valDataset,
                                batch_size=args.minibatch_size,
                                shuffle=True,
                                num_workers = args.numworkers_val,
                                prefetch_factor=2,
                                pin_memory=True)
    test_dataloader = DataLoader(testDataset,
                                 batch_size=args.minibatch_size,
                                 shuffle=True,
                                 num_workers = args.numworkers_test)

    bestValLossSoFar = torch.inf

    minEpochThresh = 5 #min epoch to start saving model checkpoints or other data

    printerObj = custom_printer( x_format = args.x_format,
                                 y_format = args.y_format,
                                 logDir = dstDir_logs,
                                 dstDirAll = args.dstDir,
                                 dstDirTrain = args.dstDir,
                                 dstDirVal = dstDir_figsVal,
                                 dstDirTest = dstDir_figsTest)

    sendAll2device(device,
                   model=model,
                   surrogateLossFunc=surrogateLossFunc)
    
    ############################################################
    ##################### starting main training loop
    ############################################################
    
    for epoch in range(args.epochs):
        
        train_out = trainFunc(train_dataloader=train_dataloader,
                              model=model,
                              optimizer=optimizer,
                              surrogateLossFunc=surrogateLossFunc,
                              current_epoch=epoch,
                              offset=args.offset,
                              x_format=args.x_format,
                              y_format=args.y_format,
                              PELine=args.PELine_train)

        model = train_out['model']
        optimizer = train_out['optimizer']
        
        val_out = valOrTestFunc(valOrTest_dataloader=val_dataloader,
                            model=model,
                            lossFunc=nn.MSELoss().to(device),
                            offset=args.offset,
                            x_format=args.x_format,
                            y_format=args.y_format,
                            current_epoch=epoch,
                            PELine=args.PELine_val)

        test_out = valOrTestFunc(valOrTest_dataloader=test_dataloader,
                            model=model,
                            lossFunc=nn.MSELoss().to(device),
                            offset=args.offset,
                            x_format=args.x_format,
                            y_format=args.y_format,
                            current_epoch=epoch,
                            PELine=args.PELine_test)
        
        if val_out['accLoss_valOrTest'] < bestValLossSoFar and epoch >= minEpochThresh:
            bestValLossSoFar = val_out['accLoss_valOrTest']
            saveModel(model=model, optimizer=optimizer, epoch=epoch, dstDir=args.dstDir)
        
        # lr scheduler
        scheduler.step(val_out['accLoss_valOrTest'])
        printCurrentLr(optimizer, dstDir=args.dstDir)

        #save outputs to disk
        print2logfile(str(epoch)+','+str(train_out['acc_sLossTrain']), dstDir_logs, fn='logTrain_MSE.txt')
        print2logfile(str(epoch)+','+str(val_out['accLoss_valOrTest']), dstDir_logs, fn='logVal_MSE.txt')
        print2logfile(str(epoch)+','+str(test_out['accLoss_valOrTest']), dstDir_logs, fn='logTest_MSE.txt')
        
        #print all correlations anyways (some may be always 0 depending on the y_format but this avoids many elifs)
        print2logfile(str(epoch)+','+str(train_out['acc_corrTheta_train']), dstDir_logs, fn='logTrain_correlTheta.txt')
        print2logfile(str(epoch)+','+str(train_out['acc_corrTx_train']), dstDir_logs, fn='logTrain_correlTx.txt')
        print2logfile(str(epoch)+','+str(train_out['acc_corrTy_train']), dstDir_logs, fn='logTrain_correlTy.txt')
        
        print2logfile(str(epoch)+','+str(val_out['accCorrelTheta_valOrTest']), dstDir_logs, fn='logVal_correlTheta.txt')
        print2logfile(str(epoch)+','+str(val_out['accCorrelTx_valOrTest']), dstDir_logs, fn='logVal_correlTx.txt')
        print2logfile(str(epoch)+','+str(val_out['accCorrelTy_valOrTest']), dstDir_logs, fn='logVal_correlTy.txt')

        print2logfile(str(epoch)+','+str(test_out['accCorrelTheta_valOrTest']), dstDir_logs, fn='logTest_correlTheta.txt')
        print2logfile(str(epoch)+','+str(test_out['accCorrelTx_valOrTest']), dstDir_logs, fn='logTest_correlTx.txt')
        print2logfile(str(epoch)+','+str(test_out['accCorrelTy_valOrTest']), dstDir_logs, fn='logTest_correlTy.txt')

        #info: 'mb' field is a minibatch tuple returned by the validation function which contains
        #itsel a 3-tuple of (x,y,networkPred) so we care only about ther 2nd and 3rd elements of this tuple
        #info: train_out['acc_sLossTrain'] etc should already have been .detach() and .cpu()
        printerObj.updateInternalVars(epoch=epoch, mb_out_train=None, mb_y_train=None,
                                      mb_out_val=val_out['mb'][2], mb_y_val=val_out['mb'][1],
                                      mb_out_test=test_out['mb'][2], mb_y_test=test_out['mb'][1],
                                      trainLoss=train_out['acc_sLossTrain'],
                                      valLoss=val_out['accLoss_valOrTest'],
                                      testLoss=test_out['accLoss_valOrTest'],
                                      train_nonscalarLoss_theta=train_out['accLoss_train_theta'],
                                      val_nonscalarLoss_theta=val_out['accLoss_valOrTest_theta'],
                                      test_nonscalarLoss_theta=test_out['accLoss_valOrTest_theta'],
                                      train_nonscalarLoss_tx=train_out['accLoss_train_tx'],
                                      val_nonscalarLoss_tx=val_out['accLoss_valOrTest_tx'],
                                      test_nonscalarLoss_tx=test_out['accLoss_valOrTest_tx'],
                                      train_nonscalarLoss_ty=train_out['accLoss_train_ty'],
                                      val_nonscalarLoss_ty=val_out['accLoss_valOrTest_ty'],
                                      test_nonscalarLoss_ty=test_out['accLoss_valOrTest_ty'])
        printerObj.printAll()
        printerObj.saveTrainValTestLoss_lists()
        printerObj.saveMinibatches()

