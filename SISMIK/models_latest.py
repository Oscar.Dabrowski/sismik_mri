import torch
from torch import optim, nn
from torch.nn import functional as F
import numpy as np
import matplotlib.pyplot as plt
import sys, os, time

#1st model that worked 
class SISMIK_v1(nn.Module):
    #ks : kernel size
    #nbr : nb (phase) rows
    def __init__(self, ks = 3, nbr = 9, nb_ch_in=2, dropoutProb=0.25,
                 nb_kernels=[128]*6,
                 nb_neuronsDense=[256,256,3]):
        super(SISMIK_v1,self).__init__()
        #REM: padding = (kernel_size - 1) / 2
        #nb_ch_in should be changed according to x_format: abs or phase -> 1 channel, real_imag -> 2 channels
        #also the last value of nb_neuronsDense should match the y_format i.e. 1, 2 or 3 values,
        #theta or tx or ty or txty or thetatxty
        pad = int( torch.floor(torch.tensor((ks-1)/2)).item() )
        self.conv1 = nn.Conv2d(nb_ch_in, nb_kernels[0], kernel_size = (1,ks), stride=(1, 1), padding=(0, pad))
        self.conv2 = nn.Conv2d(nb_kernels[0], nb_kernels[1], kernel_size = (1,ks), stride=(1, 1), padding=(0,pad))
        self.conv3 = nn.Conv2d(nb_kernels[1], nb_kernels[2], kernel_size = (1,ks), stride=(1, 1), padding=(0,pad))
        self.conv4 = nn.Conv2d(nb_kernels[2], nb_kernels[3], kernel_size = (nbr,ks), stride=(1, 1), padding=(0,pad))
        self.conv5 = nn.Conv2d(nb_kernels[3], nb_kernels[4], kernel_size = (1,ks), stride=(1, 1), padding=(0,pad))
        self.conv6 = nn.Conv2d(nb_kernels[4], nb_kernels[5], kernel_size = (1,ks), stride=(1, 1), padding=(0,pad))
        
        self.maxpool2 = nn.MaxPool2d(2)
        self.avgpool2 = nn.AvgPool2d(2)
        self.maxpool2_1d = nn.MaxPool1d(kernel_size=2, stride=2)
        
        in_nbNeuronsDense = 128*nb_kernels[5]
        self.linear1 = nn.Linear(in_nbNeuronsDense, nb_neuronsDense[0])
        self.linear2 = nn.Linear(nb_neuronsDense[0],nb_neuronsDense[1])
        self.linear3 = nn.Linear(nb_neuronsDense[1],nb_neuronsDense[2])

        self.prelu = nn.PReLU()
        #self.bn1 = nn.BatchNorm1d(num_features=in_nbNeuronsDense)
        ##self.bn1024 = nn.BatchNorm1d(num_features=1024)
        ##self.bn128 = nn.BatchNorm2d(num_features=128)
        self.dropoutProb=dropoutProb
        self.dropout = nn.Dropout(dropoutProb)
        
    def forward(self,x):        
        x = self.prelu(self.conv1(x))
        x = self.prelu(self.conv2(x))
        x = self.prelu(self.conv3(x))
        x = self.prelu(self.conv4(x))
        x = self.prelu(self.conv5(x))
        x = self.prelu(self.conv6(x))
        x = self.maxpool2_1d(x.squeeze())
        x=x.view(x.shape[0],-1)
        x=self.dropout(x)
        x=self.prelu(self.linear1(x))
        x=self.prelu(self.linear2(x))
        x = self.linear3(x)
        return x


############## variable number of layers (SISMIK v1 has 7 conv layers)
class SISMIK(nn.Module):
    #ks : kernel size
    #nbr : nb (phase) rows
    #Architecture: center n//2 is the 'collapsing' conv kernel (nbr,ks)
    def __init__(self, ks = 3, nbr = 9, nb_ch_in=2, dropoutProb=0.25,
                 nb_kernels=[128]*7,
                 nb_neuronsDense=[256,256,3]):
        #Note: [128]*n produces a list of ints like [128,128, ...,128] n times
        super(SISMIK,self).__init__()
        #REM: padding = (kernel_size - 1) / 2
        #nb_ch_in should be changed according to x_format: abs or phase -> 1 channel, real_imag -> 2 channels
        #also the last value of nb_neuronsDense should match the y_format i.e. 1, 2 or 3 values,
        #theta or tx or ty or txty or thetatxty
        pad = int( torch.floor(torch.tensor((ks-1)/2)).item() )

        self.layers = nn.ModuleList()
        nb_nb_kernels = len(nb_kernels)
        #From pytorch doc : torch.nn.Conv2d(in_channels, out_channels, kernel_size, ... 
        for i in range(nb_nb_kernels):
            if i == nb_nb_kernels//2:
                kernelSize = (nbr,ks)
            else:
                kernelSize=(1,ks)
                
            if i==0:
                nb_kernels_in = nb_ch_in
            else:
                nb_kernels_in=nb_kernels[i-1]
            nb_kernels_out = nb_kernels[i]
            conv_layer = nn.Conv2d(nb_kernels_in, nb_kernels_out, kernel_size=kernelSize, stride=(1, 1), padding=(0,pad))
            setattr(self, f'conv{i + 1}', conv_layer)
            self.layers.append(conv_layer)
            print('in SISMIK variable nb layers: creating ' , f'conv{i + 1}')
            
        self.maxpool2 = nn.MaxPool2d(2)
        self.avgpool2 = nn.AvgPool2d(2)
        self.maxpool2_1d = nn.MaxPool1d(kernel_size=2, stride=2)
        
        in_nbNeuronsDense = 128*nb_kernels[-1]
        self.linear1 = nn.Linear(in_nbNeuronsDense, nb_neuronsDense[0])
        self.linear2 = nn.Linear(nb_neuronsDense[0],nb_neuronsDense[1])
        self.linear3 = nn.Linear(nb_neuronsDense[1],nb_neuronsDense[2])

        self.prelu = nn.PReLU()
        self.dropoutProb=dropoutProb
        self.dropout = nn.Dropout(dropoutProb)
        
    def forward(self,x):        
        for layer in self.layers:
            x = self.prelu(layer(x))
        x = self.maxpool2_1d(x.squeeze())
        x=x.view(x.shape[0],-1)
        x=self.dropout(x)
        x=self.prelu(self.linear1(x))
        x=self.prelu(self.linear2(x))
        x = self.linear3(x)
        return x
    
