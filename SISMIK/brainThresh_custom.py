import torch
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import h5py
import os,sys,time


#Input args:
#f: (string) full file path OR ( tensor of complex values )
#    (isinstance(f,str) is performed to check whichis passed)
#ksp_corrupted_fieldName: only used if f is a string (i.e. filename)
#
#Output args:
#outliers: list of irrelevant brain slice indexes
#s : brain area coverage (in %)
#std_outliers: std of img_filt to identify slice(s)
#             with essentially only noise, which are not
#             always detected by aread thresholds (i.e. area_tresh=0.38)
#             (sometimes the noise is binarized in a way that results in an
#             area coverage % similar to 'normal' brain slices)
#img_filt: lowpass filtered images before thresh
#img_thresh: thresholded (binarized) image masks
#
def getValidSlices_brain(f,ksp_corrupted_fieldName='kc'):
    
    ####################################################
    #empirical params.
    ####################################################

    r=18#Fourier filter radius
    
    #brain area percent threshold for binarization
    #(ideally anything below 0.05 would be noise)
    thresh=0.05

    #the 2 following are for discarding irrelevant/useless brain slices:
    area_thresh_low = 0.38 #meaning: any brain slice with less than 38% brain coverage will be discarded
    area_thresh_high = 0.75#meaning: any brain slice with more than 75% brain coverage will be discarded 
    std_thresh=0.14#standard deviation threshold (any below that are considered outliers)

    ####################################################

    normalize01=lambda x: (x-np.min(x))/(np.max(x)-np.min(x))

    if isinstance(f, str):
        with h5py.File(f, 'r') as file:
            ksp = np.array(file['kc'])
            ksp = ksp['real'] + 1j*ksp['imag']
    else:
        ksp=f #assuming that if f is not a string then the data itself was passed

    nb_slices=ksp.shape[0]
    nrows=ksp.shape[1]
    ncols=ksp.shape[2]
    crows=nrows//2
    ccols=ncols//2
    
    #holds the sum of thresholded pixels to compute the brain matter area
    s= np.zeros((nb_slices))
    
    #to detect the slice(s) with only noise are almost only...
    #this happends and it then has a very low std w.r.t others
    std_outliers=np.zeros((nb_slices))
    
    print('nb_slices=', nb_slices,',crows=',crows,',ccols=',ccols)
    
    xx,yy=np.meshgrid(range(nrows),range(ncols))
    
    mask=np.zeros((nrows,ncols))
    mask[(xx-crows)**2+(yy-ccols)**2<=r**2]=1
    mask=np.expand_dims(mask,axis=0)
    print('mask shape after expand dims=', mask.shape)
    mask3D=np.repeat(mask,nb_slices,axis=0)
    print('3D mask shape after repmat=', mask3D.shape)
    
    ksp_filt=ksp*mask3D
    img_filt = np.abs(np.fft.ifft2(ksp_filt))
    img_filt = np.array([normalize01(img) for img in img_filt])  
    
    img_thresh=np.zeros((nb_slices,ncols,nrows))
    for i in range(nb_slices):
        img_thresh[i,:,:]=img_filt[i,:,:]>thresh
        std_outliers[i]=np.std(img_filt[i,:,:])
        s[i]=np.sum(img_thresh[i,:,:])/(nrows*ncols) #expressed in percents of tot area
        
        
    idx_outlier_std= np.where(std_outliers<std_thresh)[0]
    idx_outlier_area_low=np.where(s<area_thresh_low)[0]
    idx_outlier_area_high=np.where(s>area_thresh_high)[0]
    
    #union to remove potential duplicates which are not useful
    outliers = np.union1d(idx_outlier_std, np.union1d(idx_outlier_area_low, idx_outlier_area_high))
    sliceMask = np.ones((nb_slices))
    sliceMask[outliers]=0
    return sliceMask, outliers, s, std_outliers, img_filt, img_thresh 


#img_tensor: 3D tensor of shape nslices*rows*cols
#            (if nslices is not in dim=0 then img_tensor is permuted)
#dstDir : if this optional arg is provided, then the mosaic is saved there
#fn: optional filename
def imshow_mosaic(img_tensor, dstDir=None, fn=None):
    dim_slice = np.argmin(img_tensor.shape)
    nslices = img_tensor.shape[dim_slice]
    rows_fig=int(np.floor(np.sqrt(nslices)))
    cols_fig=int(np.ceil(np.sqrt(nslices)))
    print('displaying mosaic on a ', rows_fig, ' * ' , cols_fig, ' grid.')
    #make sure that the smalles dim is first which is assumed
    #to be the slice dim
    img_shape=tuple(np.argsort(img_tensor.shape))
    img_tensor=np.transpose(img_tensor, img_shape )
    print('img shape (after permute) =', img_tensor.shape)
    fig = plt.figure()
    grid = ImageGrid(fig, 111,nrows_ncols=(rows_fig, cols_fig), axes_pad=0.16)
    for i in range(rows_fig*cols_fig):
        if i < img_tensor.shape[0]:
            grid[i].imshow(img_tensor[i,:,:])
            grid[i].axis('off')
    if dstDir is not None:
        plt.savefig(os.path.join(dstDir,fn if fn is not None else 'img_mosaic.png'))
    plt.show()
    return fig, grid
##############################################################

    
if __name__=='__main__':
    ### Demo
    
    ### e.g. /mnt/c/MRIDATA/choco/choco2/chocodb_230427_1930/twix_mat_extracted/s3_target4deg_PE75.mat
    f = sys.argv[1]

    sliceMask, outliers, s, std_outliers, img_filt, img_thresh = getValidSlices_brain(f,ksp_corrupted_fieldName='kc')
    
    print('sums for all slices : ',s)

    imshow_mosaic(img_thresh)
        
    plt.plot(s,'bo')
    plt.xlabel('slice#')
    plt.ylabel('%brain area')
    plt.show()
    
    plt.plot(std_outliers,'ro')
    plt.xlabel('slice#')
    plt.ylabel('std')
    plt.show()
    

    print('outliers:', outliers, ', sliceMask:', sliceMask)
    
