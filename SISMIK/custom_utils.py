import torch
from torch import optim, nn
from torch.nn import functional as F
from torchinfo import summary
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')#to avoid any graphics display if sismik is run from GUI
import sys, os, time
import pickle

def find_directory(start_dir, dir_name, max_levels=5):
    # Check up to max_levels upwards and downwards
    for i in range(-max_levels, max_levels + 1):
        path = os.path.join(start_dir, *(['..'] * -i if i < 0 else [''] * i), dir_name)
        if os.path.isdir(path):
            return os.path.abspath(path)
    return None


def shift_right(a, shift):
    shift = shift % len(a)
    return np.concatenate((np.zeros(shift), a[:-shift]))

def shift_left(a, shift):
    shift = shift % len(a)
    return np.concatenate((a[shift:], np.zeros(shift)))


def normxcorr1D_custom(f,g):
    assert f.ndim==1 and g.ndim==1
    N = f.size
    cc = np.zeros(N)
    for i in range(N):
        if i<=(N/2):
            offset = int( N-(i+N/2) )
            g_shift = shift_left(g,offset)
        else:
            offset = int( i-(N/2) )
            g_shift = shift_right(g,offset)
        cc[i] = (( (f-f.mean()) @ (g_shift-g_shift.mean()).T )/N )/(f.std()*g_shift.std())
    return cc

#adjacent rows
#this func expects np.arrays
#do any casts torch.tensor <-> np.array outside of it
def normxcorr_adj(f):
    assert f.ndim == 3
    chn, rows, cols = f.shape
    xcorr = np.zeros((chn,rows,cols))
    for i in range(1,rows):
        ky_real=f[0,i,:]
        ky_imag=f[1,i,:]
        ky_real_prev=f[0,i-1,:]
        ky_imag_prev=f[1,i-1,:]
        xcorr[0,i,:] = normxcorr1D_custom(ky_real,ky_real_prev)
        xcorr[1,i,:] = normxcorr1D_custom(ky_imag,ky_imag_prev)
    return xcorr

    
#mode estimation for continuous distribution
def estimate_mode(tensor):
    data = tensor.numpy()
    counts, bin_edges = np.histogram(data, bins=len(data))
    max_count_idx = np.argmax(counts)
    #bin center = mode
    mode_value = (bin_edges[max_count_idx] + bin_edges[max_count_idx+1]) / 2
    return mode_value


def unwrap_numpy_array(arr):
    while isinstance(arr, np.ndarray) and arr.size == 1:
        arr = arr.item()
    return arr

def unwrap_list(lst):
    while isinstance(lst, list) and len(lst) == 1: lst = lst[0]
    return lst

def getTargetIdx(targetAngle):
    #we used 3 different angles: 2,3,4 deg
    #so idx for 2deg is 2-2 = 0 , for 3deg: 3-2 = 1 and for 4deg : 4-2=2
    return int( targetAngle-2 )

def lp_detect(lp, **kwargs):
    if (
        "window_size" not in kwargs
        or "idxDC" not in kwargs
        or "threshold" not in kwargs
        or "offsetDC" not in kwargs
        or "offsetStart" not in kwargs
        or "offsetEnd" not in kwargs
    ):
        raise ValueError("Required parameters missing")

    window_size = kwargs["window_size"]
    idx_dc = kwargs["idxDC"]
    threshold = kwargs["threshold"]
    offset_dc = kwargs["offsetDC"]
    offset_start = kwargs["offsetStart"]
    offset_end = kwargs["offsetEnd"]
    diff_order = kwargs.get("diffOrder", 1)

    lp_tensor = torch.tensor(lp, dtype=torch.float32)
    window = torch.ones(window_size) / window_size
    filtered_signal = torch.nn.functional.conv1d(lp_tensor.view(1, 1, -1), window.view(1, 1, -1), padding=window_size//2).squeeze()

    dl_p = torch.diff(filtered_signal)

    if diff_order > 1:
        dl_p = torch.diff(dl_p)

    dl_p[:offset_start] = 0
    dl_p[-offset_end:] = 0
    dl_p[idx_dc - offset_dc:idx_dc + offset_dc] = 0

    abs_dl_p = torch.abs(dl_p)
    drop_indices = torch.where(abs_dl_p > threshold)[0]

    return drop_indices, abs_dl_p, dl_p


def sendAll2device(device, **kwargs):
    for k in kwargs.keys():
        kwargs[k]=kwargs[k].to(device)
#endSendAll2device

#Tupl2pack: returns a dictionary : packs the individual args into kwargs dict
#to be called like d = tuple2pack(arg1=arg1, arg2=arg2, ..., argn=argn)
def tuple2pack(**kwargs):
    return kwargs
#endTuple2pack

def hermitianSym(xfft):
    ''' checks i did:
    In [123]: torch.where(torch.abs(hermitianfft) == torch.max(torch.abs(hermitianfft)))
    Out[123]: (tensor([128]), tensor([128]))
    
    In [124]: torch.where(torch.abs(k) == torch.max(torch.abs(k)))
    Out[124]: (tensor([128]), tensor([128]))
    '''
    #np.roll(np.roll(np.fliplr(np.flipud(xfft)),1, axis=0),1, axis=1)
    return torch.roll(torch.roll(torch.fliplr(torch.flipud(xfft)),1, dims=0),1, dims=1)
#endHermitianSym


def saveModelArchInfo(model, dummy_input, dstDir):
    modelSummary = summary(model, input_size=dummy_input.shape)#(minibatch_size,nbch,nbr,nbc))
    with open(os.path.join(dstDir, 'torchinfo_model_summary.txt'), 'w') as f:
        print(modelSummary, file=f)
    with open(os.path.join(dstDir, 'raw_model_print.txt'), 'w') as f:
        print(model, file=f)
    #print2logfile(modelSummary, dstDir, fn='model_summary.txt')
#endSaveModelArchInfo

#Corrcoef functions either 1,2, or 3 to be called depending on:
#computeCorrcoef1 is when y_format is either theta OR tx OR ty (DNN gives 1 output prediction)
# " 2 is for 2 i.e. tx and ty
# " 3 is for when DNN preficts all 3 : theta,tx,ty

def computeCorrcoef1(out, y):
    if (out.shape[0] > 1) and (out.ndim >= 1):# minibatch_size:
        with torch.no_grad():
            both_ = torch.cat((out.unsqueeze(0),y.unsqueeze(0)),dim=0) #torch.cat((out[:,1].unsqueeze(0),y[:,1].unsqueeze(0)),dim=0)
            correl_ = torch.corrcoef(both_)[0,1]
            return correl_.detach().item()
    else:
        print('WARNING: corrcoef func returned zeros ...')
        return 0.0
#endComputeCorrcoef1

def computeCorrcoef2(out, y):
    if (out.shape[0] > 1) and (out.ndim > 1):# minibatch_size:
        with torch.no_grad():
            both_tx = torch.cat((out[:,1].unsqueeze(0),y[:,1].unsqueeze(0)),dim=0)
            both_ty = torch.cat((out[:,2].unsqueeze(0),y[:,2].unsqueeze(0)),dim=0)
            correl_tx_ = torch.corrcoef(both_tx)[0,1]
            correl_ty_ = torch.corrcoef(both_ty)[0,1]
            return correl_tx_.detach().item(), correl_ty_.detach().item()
    else:
        print('WARNING: corrcoef func returned zeros ...')
        return 0.0, 0.0
#endComputeCorrcoef2

def computeCorrcoef3(out, y):
    if (out.shape[0] > 1) and (out.ndim > 1):# minibatch_size:
        with torch.no_grad():
            both_theta = torch.cat((out[:,0].unsqueeze(0),y[:,0].unsqueeze(0)),dim=0)
            both_tx = torch.cat((out[:,1].unsqueeze(0),y[:,1].unsqueeze(0)),dim=0)
            both_ty = torch.cat((out[:,2].unsqueeze(0),y[:,2].unsqueeze(0)),dim=0)
            
            #print('>>>>>> CORRCOEF DEBUG: >>>>>:',  both_train_theta.shape)            
            correl_theta_ = torch.corrcoef(both_theta)[0,1]
            #acc_xcorr_theta = acc_xcorr_theta.item()
            
            correl_tx_ = torch.corrcoef(both_tx)[0,1]
            #acc_xcorr_tx = acc_xcorr_tx.item()
        
            correl_ty_ = torch.corrcoef(both_ty)[0,1]
            #acc_xcorr_ty = acc_xcorr_ty.item()

            return correl_theta_.detach().item(), correl_tx_.detach().item(), correl_ty_.detach().item()
    else:
        print('WARNING: corrcoef func returned zeros ...')
        return 0.0, 0.0, 0.0
#endComputeCorrcoef3


#normalize the whole batch to be between 0 and 1
def normalizeMinMax01(x):
    #0 is the batch index, x is supposed to be a 4D tensor with batch in the first dim
    return (x-torch.min(x,0)[0])/(torch.max(x,0)[0]-torch.min(x,0)[0])
#endNormalizeMinMax01


#ideally infoString contains jsut: epoch, trainLoss, valLoss
def print2logfile(infoString, dstDir, fn='log.txt'):
    with open(os.path.join(dstDir,fn), 'a') as f:
        f.write(infoString+'\n')
#endPrint2logfile

def printCurrentLr(opt, dstDir):
    with torch.no_grad():
        for g in opt.param_groups:
            #print('>>>> current lr = ',g['lr'])
            print2logfile(infoString=str(g['lr']), dstDir=dstDir, fn='lr.txt')
#endPrintCurrentLr


#ylabelText could be angle, or  tx or ty (pixels)
def pltYYhat_scalarOut(y_,out_,dstDir,currentEpoch,maxMiniBatchNb,namePrefix, ylabelText='angle (degrees)'):
    plt.plot(y_.detach().cpu()[0:maxMiniBatchNb],'-b')
    plt.plot(out_.detach().cpu()[0:maxMiniBatchNb],'--r')
    plt.xlabel('minibatch example ('+namePrefix+')')
    plt.ylabel(ylabelText)
    plt.ylim([-4.0, 4.0])
    plt.yticks(np.arange(-4,4,0.5))
    plt.xticks(np.arange(0,maxMiniBatchNb,5))
    plt.grid(True)
    plt.legend(['y','y^'])
    plt.savefig(os.path.join(dstDir,namePrefix+'_minibatch_e'+str(currentEpoch)+'.png'))
    plt.show()
    plt.close()
#endPltYYhat_scalarOut
    
def pltYYhat_angleTxTy(y_,out_,dstDir,currentEpoch,maxMiniBatchNb,namePrefix):
    plt.plot(y_[:,0].detach().cpu()[0:maxMiniBatchNb],'-r')
    plt.plot(y_[:,1].detach().cpu()[0:maxMiniBatchNb],'-g')
    plt.plot(y_[:,2].detach().cpu()[0:maxMiniBatchNb],'-b')
    plt.plot(out_[:,0].detach().cpu()[0:maxMiniBatchNb],'--r')
    plt.plot(out_[:,1].detach().cpu()[0:maxMiniBatchNb],'--g')
    plt.plot(out_[:,2].detach().cpu()[0:maxMiniBatchNb],'--b')
    plt.xlabel('minibatch example ('+namePrefix+')')
    plt.ylabel('angle, tx & ty (degrees, pixels)')
    plt.ylim([-4.0, 4.0])
    plt.yticks(np.arange(-4,4,0.5))
    plt.xticks(np.arange(0,maxMiniBatchNb,5))
    plt.grid(True)
    plt.legend(['y (angle)','y (tx)','y (ty)','y^ (angle)','y^ (tx)','y^ (ty)'])
    plt.savefig(os.path.join(dstDir,namePrefix+'_minibatch_e'+str(currentEpoch)+'.png'))
    plt.show()
    plt.close()
#endPltYYhat_angleTxTy


#setType: would be something like 'train,'val' or 'test'
#out and y vars: here expected to have 3 columns for theta,tx,ty
def pltYYhat_sepPlt_angleTxTy(y_,out_,dstDir,currentEpoch,maxMiniBatchNb,namePrefix,setType):
    yMinAngle = -4.0
    yMaxAngle = 4.0
    yTickAngle = 0.5
    
    yMinTxy = -3.0
    yMaxTxy = 3.0
    yTickTxy = 0.5

    mbTick = 5 #minibatch tick
    
    plt.plot(y_[:,0].detach().cpu()[0:maxMiniBatchNb],'-b')
    plt.plot(out_[:,0].detach().cpu()[0:maxMiniBatchNb],'--r')
    plt.xlabel('minibatch example '+setType)
    plt.ylabel('angle (degrees)')
    plt.ylim([yMinAngle, yMaxAngle])
    plt.yticks(np.arange(yMinAngle,yMaxAngle,yTickAngle))
    plt.xticks(np.arange(0,maxMiniBatchNb,mbTick))
    plt.grid(True)
    plt.legend(['y (angle)','y^ (angle)'])
    plt.savefig(os.path.join(dstDir,namePrefix+'_minibatch_theta_e'+str(currentEpoch)+'.png'))
    plt.show()
    plt.close()
    ##
    plt.plot(y_[:,1].detach().cpu()[0:maxMiniBatchNb],'-b')
    plt.plot(out_[:,1].detach().cpu()[0:maxMiniBatchNb],'--r')
    plt.xlabel('minibatch example '+setType)
    plt.ylabel('tx (pixels)')
    plt.ylim([yMinTxy, yMaxTxy])
    plt.yticks(np.arange(yMinTxy,yMaxTxy,yTickTxy))
    plt.xticks(np.arange(0,maxMiniBatchNb,mbTick))
    plt.grid(True)
    plt.legend(['y (tx)','y^ (tx)'])
    plt.savefig(os.path.join(dstDir,namePrefix+'_minibatch_tx_e'+str(currentEpoch)+'.png'))
    plt.show()
    plt.close()
    ##
    plt.plot(y_[:,2].detach().cpu()[0:maxMiniBatchNb],'-b')
    plt.plot(out_[:,2].detach().cpu()[0:maxMiniBatchNb],'--r')
    plt.xlabel('minibatch example '+setType)
    plt.ylabel('ty (pixels)')
    plt.ylim([yMinTxy, yMaxTxy])
    plt.yticks(np.arange(yMinTxy, yMaxTxy,yTickTxy))
    plt.xticks(np.arange(0,maxMiniBatchNb,mbTick))
    plt.grid(True)
    plt.legend(['y (ty)','y^ (ty)'])
    plt.savefig(os.path.join(dstDir,namePrefix+'_minibatch_ty_e'+str(currentEpoch)+'.png'))
    plt.show()
    plt.close()
#endPltYYhat_sepPlt_angleTxTy


def pltTrainValTestCurves(currentEpoch, trainLoss_list, testLoss_list, valLoss_list, dstDir, lossType='MSE'):
    epochTick = 25
    tickStep = 0.25
    max_trainTest = np.max( [ *trainLoss_list, *testLoss_list, *valLoss_list] )
        
    plt.plot(trainLoss_list,'-b')
    plt.plot(valLoss_list,'--r')
    plt.plot(testLoss_list,'--k')
    plt.grid(True)
    plt.yticks(np.arange(0.0, max_trainTest, tickStep))
    plt.ylim([0.0, max_trainTest])
    plt.xlim([0, currentEpoch])
    plt.xticks(np.arange(0,currentEpoch,epochTick))
    plt.xlabel('epoch')
    plt.ylabel(lossType)
    plt.legend(['train','val','test'])
    plt.savefig(os.path.join(dstDir,'train_val_test_curves_e'+str(currentEpoch)+'.png'))
    plt.show()
    plt.close()
#endPltTrainValTestCurves

def pltTrainValTestCurves_nonscalar(currentEpoch,
                                    trainLoss_list_theta,
                                    testLoss_list_theta,
                                    valLoss_list_theta,
                                    trainLoss_list_tx,
                                    testLoss_list_tx,
                                    valLoss_list_tx,
                                    trainLoss_list_ty,
                                    testLoss_list_ty,
                                    valLoss_list_ty,
                                    dstDir, lossType='MSE'):
    epochTick = 25
    tickStep = 0.25
    
    max_trainTest = np.max( [ *trainLoss_list_theta, *testLoss_list_theta, *valLoss_list_theta,
                              *trainLoss_list_tx, *testLoss_list_tx, *valLoss_list_tx,
                              *trainLoss_list_ty, *testLoss_list_ty, *valLoss_list_ty] )
        
    plt.plot(trainLoss_list_theta,'-b')
    plt.plot(valLoss_list_theta,'-r')
    plt.plot(testLoss_list_theta,'-k')
    plt.plot(trainLoss_list_tx,'--b')
    plt.plot(valLoss_list_tx,'--r')
    plt.plot(testLoss_list_tx,'--k')
    plt.plot(trainLoss_list_ty,'-.b')
    plt.plot(valLoss_list_ty,'-.r')
    plt.plot(testLoss_list_ty,'-.k')
    plt.grid(True)
    plt.yticks(np.arange(0.0, max_trainTest, tickStep))
    plt.ylim([0.0, max_trainTest])
    plt.xlim([0, currentEpoch])
    plt.xticks(np.arange(0,currentEpoch,epochTick))
    plt.xlabel('epoch')
    plt.ylabel(lossType)
    plt.legend(['train (angle)','val (angle)','test (angle)',
                'train (tx)','val (tx)','test (tx)',
                'train (ty)','val (ty)','test (ty)'])
    plt.savefig(os.path.join(dstDir,'train_val_test_curves_nonscalar_e'+str(currentEpoch)+'.png'))
    plt.show()
    plt.close()
#endPltTrainValTestCurves_nonscalar


def pltCorrelAllFromTxt(srcDirTrain, srcDirVal, srcDirTest, dstDir, y_format, currentEpoch):
    epochTick = 25
    os.makedirs(dstDir, exist_ok=True)#correlPlots dir
    trainCorrelNamePrefix = 'logTrain_correl'#e.g. logTrain_correlTheta.txt or logTrain_correlTx.txt etc.
    valCorrelNamePrefix = 'logVal_correl'
    testCorrelNamePrefix = 'logTest_correl'
    
    trxcorr_theta = np.genfromtxt(os.path.join(srcDirTrain,trainCorrelNamePrefix+'Theta.txt'),delimiter=',')[:,1]
    trxcorr_tx = np.genfromtxt(os.path.join(srcDirTrain,trainCorrelNamePrefix+'Tx.txt'),delimiter=',')[:,1]
    trxcorr_ty = np.genfromtxt(os.path.join(srcDirTrain,trainCorrelNamePrefix+'Ty.txt'),delimiter=',')[:,1]
    
    valxcorr_theta = np.genfromtxt(os.path.join(srcDirVal,valCorrelNamePrefix+'Theta.txt'),delimiter=',')[:,1]
    valxcorr_tx = np.genfromtxt(os.path.join(srcDirVal,valCorrelNamePrefix+'Tx.txt'),delimiter=',')[:,1]
    valxcorr_ty = np.genfromtxt(os.path.join(srcDirVal,valCorrelNamePrefix+'Ty.txt'),delimiter=',')[:,1]

    testxcorr_theta = np.genfromtxt(os.path.join(srcDirTest,testCorrelNamePrefix+'Theta.txt'),delimiter=',')[:,1]
    testxcorr_tx = np.genfromtxt(os.path.join(srcDirTest,testCorrelNamePrefix+'Tx.txt'),delimiter=',')[:,1]
    testxcorr_ty = np.genfromtxt(os.path.join(srcDirTest,testCorrelNamePrefix+'Ty.txt'),delimiter=',')[:,1]
    
    plt.plot(trxcorr_theta,'-b')
    if y_format != 'theta':
        plt.plot(trxcorr_tx,'--b')
        plt.plot(trxcorr_ty,'-.b')
    plt.plot(valxcorr_theta,'-r')
    if y_format != 'theta':
        plt.plot(valxcorr_tx,'--r')
        plt.plot(valxcorr_ty,'-.r')
    plt.plot(testxcorr_theta,'-k')
    if y_format != 'theta':
        plt.plot(testxcorr_tx,'--k')
        plt.plot(testxcorr_ty,'-.k')
    plt.xlabel('epoch')
    plt.ylabel('correlation')
    plt.grid(True)
    plt.yticks(np.arange(0.0, 1.0, 0.1))
    plt.ylim([0.0, 1.0])
    plt.xlim([0, currentEpoch])
    plt.xticks(np.arange(0,currentEpoch,epochTick))
    if y_format != 'theta':
        plt.legend(['train (angle)',
                    'train (tx)',
                    'train (ty)',
                    'val (angle)',
                    'val (tx)',
                    'val (ty)',
                    'test (angle)',
                    'test (tx)',
                    'test (ty)'])
    else:
        plt.legend(['train (angle)', 'val (angle)','test (angle)'])
    plt.show()
    plt.savefig(os.path.join(dstDir,'correl_train_val_test_e'+str(currentEpoch)+'.png'))
    plt.close()
#endPltCorrelAllFromTxt

class custom_printer:
    def __init__(self, x_format, y_format, dstDirAll, dstDirTrain, dstDirVal, dstDirTest, logDir,
                 maxMiniBatchNb = 64,
                 pltTrainValTestCurves = pltTrainValTestCurves,
                 pltYYhat_sepPlt_angleTxTy = pltYYhat_sepPlt_angleTxTy,
                 pltYYhat_scalarOut = pltYYhat_scalarOut,
                 pltCorrelAll=pltCorrelAllFromTxt,
                 pltTrainValTestCurves_nonscalar=pltTrainValTestCurves_nonscalar):
        self.x_format=x_format
        self.y_format=y_format
        self.logDir=logDir
        self.dstDirAll=dstDirAll
        self.dstDirTrain = dstDirTrain
        self.dstDirVal = dstDirVal
        self.dstDirTest = dstDirTest
        self.trainLoss_list = []
        self.testLoss_list = []
        self.valLoss_list = []
        
        self.train_nonscalarLoss_theta_list=[]
        self.val_nonscalarLoss_theta_list=[]
        self.test_nonscalarLoss_theta_list=[]
        self.train_nonscalarLoss_tx_list=[]
        self.val_nonscalarLoss_tx_list=[]
        self.test_nonscalarLoss_tx_list=[]
        self.train_nonscalarLoss_ty_list=[]
        self.val_nonscalarLoss_ty_list=[]
        self.test_nonscalarLoss_ty_list=[]

        self.currentEpoch = 0
        self.maxMiniBatchNb = maxMiniBatchNb
        self.nn_out = None
        self.y = None
        self.pltTrainValTestCurves = pltTrainValTestCurves
        self.pltYYhat_sepPlt_angleTxTy = pltYYhat_sepPlt_angleTxTy
        self.pltYYhat_scalarOut = pltYYhat_scalarOut
        self.pltCorrelAll=pltCorrelAllFromTxt
        self.pltTrainValTestCurves_nonscalar=pltTrainValTestCurves_nonscalar
    def updateInternalVars(self, epoch, mb_out_train, mb_y_train,
                           mb_out_val, mb_y_val,
                           mb_out_test, mb_y_test,
                           trainLoss, valLoss, testLoss,
                           train_nonscalarLoss_theta,val_nonscalarLoss_theta,test_nonscalarLoss_theta,
                           train_nonscalarLoss_tx,val_nonscalarLoss_tx,test_nonscalarLoss_tx,
                           train_nonscalarLoss_ty,val_nonscalarLoss_ty,test_nonscalarLoss_ty):
        
        self.trainLoss_list.append(trainLoss) 
        self.valLoss_list.append(valLoss)
        self.testLoss_list.append(testLoss)
        self.currentEpoch = epoch
        self.mb_out_train = None #mb_out_train.detach().cpu()
        self.mb_y_train = None #mb_y_train.detach().cpu()
        self.mb_out_val = mb_out_val.detach().cpu()
        self.mb_y_val = mb_y_val.detach().cpu()
        self.mb_out_test = mb_out_test.detach().cpu()
        self.mb_y_test = mb_y_test.detach().cpu()
        self.train_nonscalarLoss_theta_list.append(train_nonscalarLoss_theta)
        self.val_nonscalarLoss_theta_list.append(val_nonscalarLoss_theta)
        self.test_nonscalarLoss_theta_list.append(test_nonscalarLoss_theta)
        self.train_nonscalarLoss_tx_list.append(train_nonscalarLoss_tx)
        self.val_nonscalarLoss_tx_list.append(val_nonscalarLoss_tx)
        self.test_nonscalarLoss_tx_list.append(test_nonscalarLoss_tx)
        self.train_nonscalarLoss_ty_list.append(train_nonscalarLoss_ty)
        self.val_nonscalarLoss_ty_list.append(val_nonscalarLoss_ty)
        self.test_nonscalarLoss_ty_list.append(test_nonscalarLoss_ty)
        
    def saveTrainValTestLoss_lists(self):
        with open(os.path.join(self.dstDirTrain,'trainLoss_list.pkl'), 'wb') as f:
            pickle.dump(self.trainLoss_list,f)
        with open(os.path.join(self.dstDirVal,'valLoss_list.pkl'), 'wb') as f:
            pickle.dump(self.valLoss_list,f)
        with open(os.path.join(self.dstDirTest,'testLoss_list.pkl'), 'wb') as f:
            pickle.dump(self.testLoss_list,f)
            
    def saveMinibatches(self):
        with open(os.path.join(self.dstDirVal,'mb_out_val.pkl'), 'wb') as f:
            pickle.dump(self.mb_out_val,f)
        with open(os.path.join(self.dstDirVal,'mb_y_val.pkl'), 'wb') as f:
            pickle.dump(self.mb_y_val,f)
        with open(os.path.join(self.dstDirTest,'mb_out_test.pkl'), 'wb') as f:
            pickle.dump(self.mb_out_test,f)
        with open(os.path.join(self.dstDirTest,'mb_y_test.pkl'), 'wb') as f:
            pickle.dump(self.mb_y_val,f)

    def callPltCorrelAll(self):
        self.pltCorrelAll(srcDirTrain=self.logDir,
                          srcDirVal=self.logDir,
                          srcDirTest=self.logDir,
                          dstDir=os.path.join(self.dstDirAll,'correlPlots'),
                          currentEpoch=self.currentEpoch,
                          y_format = self.y_format)
        
    def printAll(self):
        if self.currentEpoch > 1:
            self.callPltCorrelAll()
        #call to plt functions ...
        self.pltTrainValTestCurves(currentEpoch = self.currentEpoch,
                              trainLoss_list=self.trainLoss_list,
                              testLoss_list = self.testLoss_list,
                              valLoss_list = self.valLoss_list,
                              dstDir = self.dstDirAll,
                              lossType='MSE')
        if self.y_format == 'theta':
            
            self.pltYYhat_scalarOut(y_ = self.mb_y_val, out_ = self.mb_out_val,
                               dstDir = self.dstDirVal,
                               currentEpoch=self.currentEpoch,
                               maxMiniBatchNb=self.maxMiniBatchNb,
                               namePrefix='val',
                               ylabelText='angle (degrees)')
            
            self.pltYYhat_scalarOut(y_ = self.mb_y_test, out_ = self.mb_out_test,
                               dstDir = self.dstDirTest,
                               currentEpoch=self.currentEpoch,
                               maxMiniBatchNb=self.maxMiniBatchNb,
                               namePrefix='test',
                               ylabelText='angle (degrees)')
            
        elif self.y_format == 'tx':
            self.pltYYhat_scalarOut(y_ = self.mb_y_val, out_ = self.mb_out_val,
                               dstDir = self.dstDirVal,
                               currentEpoch=self.currentEpoch,
                               maxMiniBatchNb=self.maxMiniBatchNb,
                               namePrefix='val',
                               ylabelText='tx (pixels)')
            self.pltYYhat_scalarOut(y_ = self.mb_y_test, out_ = self.mb_out_test,
                               dstDir = self.dstDirTest,
                               currentEpoch=self.currentEpoch,
                               maxMiniBatchNb=self.maxMiniBatchNb,
                               namePrefix='test',
                               ylabelText='tx (pixels)')
        elif self.y_format == 'ty':
            self.pltYYhat_scalarOut(y_ = self.mb_y_val, out_ = self.mb_out_val,
                               dstDir = self.dstDirVal,
                               currentEpoch=self.currentEpoch,
                               maxMiniBatchNb=self.maxMiniBatchNb,
                               namePrefix='val',
                               ylabelText='ty (pixels)')
            self.pltYYhat_scalarOut(y_ = self.mb_y_test, out_ = self.mb_out_test,
                               dstDir = self.dstDirTest,
                               currentEpoch=self.currentEpoch,
                               maxMiniBatchNb=self.maxMiniBatchNb,
                               namePrefix='test',
                               ylabelText='ty (pixels)')
        elif self.y_format == 'theta_tx_ty':
            self.pltYYhat_sepPlt_angleTxTy(y_ = self.mb_y_val,
                                      out_ = self.mb_out_val,
                                      dstDir=self.dstDirVal,
                                      currentEpoch=self.currentEpoch,
                                      maxMiniBatchNb=self.maxMiniBatchNb,
                                      namePrefix='val',
                                      setType='val')
            self.pltYYhat_sepPlt_angleTxTy(y_ = self.mb_y_test,
                                      out_ = self.mb_out_test,
                                      dstDir=self.dstDirTest,
                                      currentEpoch=self.currentEpoch,
                                      maxMiniBatchNb=self.maxMiniBatchNb,
                                      namePrefix='test',
                                      setType='test')
            if self.currentEpoch > 1:
                with open(os.path.join(self.dstDirAll,'train_nonscalarLoss_theta.pkl'), 'wb') as f:
                    pickle.dump(self.train_nonscalarLoss_theta_list,f)
                with open(os.path.join(self.dstDirAll,'train_nonscalarLoss_tx.pkl'), 'wb') as f:
                    pickle.dump(self.train_nonscalarLoss_tx_list,f)
                with open(os.path.join(self.dstDirAll,'train_nonscalarLoss_ty.pkl'), 'wb') as f:
                    pickle.dump(self.train_nonscalarLoss_ty_list,f)

                with open(os.path.join(self.dstDirAll,'val_nonscalarLoss_theta.pkl'), 'wb') as f:
                    pickle.dump(self.val_nonscalarLoss_theta_list,f)
                with open(os.path.join(self.dstDirAll,'val_nonscalarLoss_tx.pkl'), 'wb') as f:
                    pickle.dump(self.val_nonscalarLoss_tx_list,f)
                with open(os.path.join(self.dstDirAll,'val_nonscalarLoss_ty.pkl'), 'wb') as f:
                    pickle.dump(self.val_nonscalarLoss_ty_list,f)

                with open(os.path.join(self.dstDirAll,'test_nonscalarLoss_theta.pkl'), 'wb') as f:
                    pickle.dump(self.test_nonscalarLoss_theta_list,f)
                with open(os.path.join(self.dstDirAll,'test_nonscalarLoss_tx.pkl'), 'wb') as f:
                    pickle.dump(self.test_nonscalarLoss_tx_list,f)
                with open(os.path.join(self.dstDirAll,'test_nonscalarLoss_ty.pkl'), 'wb') as f:
                    pickle.dump(self.test_nonscalarLoss_ty_list,f)
                    
                self.pltTrainValTestCurves_nonscalar(currentEpoch=self.currentEpoch,
                                                     trainLoss_list_theta=self.train_nonscalarLoss_theta_list,
                                                     testLoss_list_theta=self.test_nonscalarLoss_theta_list,
                                                     valLoss_list_theta=self.val_nonscalarLoss_theta_list,
                                                     trainLoss_list_tx=self.train_nonscalarLoss_tx_list,
                                                     testLoss_list_tx=self.test_nonscalarLoss_tx_list,
                                                     valLoss_list_tx=self.val_nonscalarLoss_tx_list,
                                                     trainLoss_list_ty=self.train_nonscalarLoss_ty_list,
                                                     testLoss_list_ty=self.test_nonscalarLoss_ty_list,
                                                     valLoss_list_ty=self.val_nonscalarLoss_ty_list,
                                                     dstDir=self.dstDirAll)
        elif self.y_format == 'tx_ty':
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            print('in printObj: x_format tx_ty not implemented yet. ')
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            raise ValueError
