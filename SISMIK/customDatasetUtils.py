import numpy as np 
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')#to avoid any graphics display if sismik is run from GUI  
from numpy import linalg
from skimage import data
from skimage.color import rgb2gray
import torch
from torch import optim, nn
from torch.nn import functional as F
import sys, os, time
from torchinfo import summary
from torch.utils.data import Dataset, DataLoader
import torchvision
from torchvision import datasets
from torchvision.transforms import ToTensor
import h5py
import pickle
import glob
import custom_utils
#from custom_utils import normxcorr_adj, normxcorr1D_custom, shift_left, shift_right
from custom_utils import *

#Note:
#instead of 'window_size', I used the 'offset' terminology: window_size == offset*2+1
#I did not modify it but it should be mentioned because the winSize terminology is used in
#the SISMIK paper and is a more straightforward terminology


def getNormalizationMapsDir():
    #if 'args' not in globals():
    try:
        return args.mapsDir
    except NameError:
        kspNormDirFullPath = find_directory(start_dir=os.getcwd(),
                                            dir_name='kspace_normalization',
                                            max_levels=5)
        args = type('argsObject', (), {'mapsDir': kspNormDirFullPath})()
    #if args.mapsDir is not None:
    return args.mapsDir
    #else:
    #    #default value
    #    return '/data2/OD/kspace_normalization/muStdMaps/v7_3_format/'

def getMuStdMaps(device):
    normalizationMapsDir = getNormalizationMapsDir()
    mu_real= torch.tensor(np.array(h5py.File(os.path.join(normalizationMapsDir, 'mu_real.mat'),'r')['mu_real'])).T.to(device)
    mu_imag= torch.tensor(np.array(h5py.File(os.path.join(normalizationMapsDir, 'mu_imag.mat'),'r')['mu_imag'])).T.to(device)
    std_real= torch.tensor(np.array(h5py.File(os.path.join(normalizationMapsDir, 'std_real.mat'),'r')['std_real'])).T.to(device)
    std_imag= torch.tensor(np.array(h5py.File(os.path.join(normalizationMapsDir, 'std_imag.mat'),'r')['std_imag'])).T.to(device)
    return mu_real, mu_imag, std_real, std_imag

#def computePERange(PELine, offset):
#    #'new' offset range
#    PERange = range(PELine-offset, PELine+offset+1)
#    return PERange

#info: computeIdxRangeFromOffset is imported form customDatasetUtils.py

#normalize minimatch by precomputed (on nomo trainingset) mu and std maps
def normalizeByMuStdMaps(x, x_format, PELine, offset, device):
    mu_real, mu_imag, std_real, std_imag = getMuStdMaps(device=device)
    PERange = computeIdxRangeFromOffset(offset=offset, PELine=PELine)
    if x_format == 'real_imag' or x_format == 'real_imag_correl':
        #Note: of x_format is real_imag_correl -> correl channels (real and imag)
        #are assumed to be the last ones (third, fourth) and they are already normalized
        #hence we do not touch them
        x[:,0,:,:] = (x[:,0,:,:]-mu_real[PERange,:])/std_real[PERange]
        x[:,1,:,:] = (x[:,1,:,:]-mu_imag[PERange,:])/std_imag[PERange]
        return x
    elif x_format == 'abs_phase':
        mu_abs = torch.abs(mu_real+1j*mu_imag)
        std_abs = torch.abs(std_real+1j*std_imag)
        x[:,0,:,:] = (x[:,0,:,:]-mu_abs[PERange,:])/std_abs[PERange,:]
        # probably -> dont normalize the phase?
        return x
    elif x_format == 'abs':
        mu_abs = torch.abs(mu_real+1j*mu_imag)
        std_abs = torch.abs(std_real+1j*std_imag)
        x[:,0,:,:] = (x[:,0,:,:]-mu_abs[PERange,:])/std_abs[PERange,:]
        return x
    elif x_format == 'phase':
        #no need to normalize because phase is roughly between -pi and pi which should be
        # and acceptable range for a deepnet
        return x
    else:
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print('!!!!!!!!!! UNKNOWN x_format ----> error raised !!!!!!!!!!!!!')
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        raise ValueError


def get_mat_files(directory):
    #also look into subdirectories
    pattern = os.path.join(directory, '**/*.mat')
    mat_files = glob.glob(pattern, recursive=True)  
    return mat_files

def computeIdxRangeFromOffset(offset, PELine):
    return torch.arange(PELine-offset, PELine+offset+1, 1)

def saveModel(model, optimizer, epoch, dstDir, checkpoint_name='checkpoint.pth'):
    checkpoint = {
        'nb_epochs_finished': epoch + 1,
        'model_state': model.state_dict(),
        'optimizer_state': optimizer.state_dict()
    }
    torch.save(checkpoint, os.path.join(dstDir, checkpoint_name))
#endSaveModel


class CustomDataset_v2(Dataset):
    def __init__(self, dataDir, offset, PELine, x_format, y_format):
        self.offset = offset
        self.PELine=PELine
        #training example format i.e. real+imag, abs only, phase only etc.
        self.x_format = x_format
        self.y_format = y_format
        #number of rows = 2*offset (because before and after) and +1 for central line
        self.nrows_phase = 2*offset+1
        
        #labels embedded in each example file 
        self.training_examples_dir = dataDir
        
        self.training_example_files = os.listdir(self.training_examples_dir)
        self.training_example_files = [os.path.join(self.training_examples_dir, f) for f in self.training_example_files]

        
    def __len__(self):
        #nb files in the directory 'training_examples_path'
        return len(self.training_example_files)

    def __getitem__(self, idx):
        #print('>>> im trying to load:'+self.training_example_files[idx])
        matfile = h5py.File(self.training_example_files[idx], 'r')
        
        #print(matfile.keys())
        idxRange = computeIdxRangeFromOffset(offset=self.offset, PELine=self.PELine) 
        x_real = torch.tensor(np.array(matfile['ksp_corrupted_combined']['real'])).T[idxRange,:]
        x_imag = torch.tensor(np.array(matfile['ksp_corrupted_combined']['imag'])).T[idxRange,:]

        #need to add channels dimension betfore cat
        x_real=x_real.view(1,x_real.shape[0],x_real.shape[1])
        x_imag=x_imag.view(1,x_imag.shape[0],x_imag.shape[1])
        
        #REM: always convert first to np.array for efficienfy reasons

        theta = torch.tensor(np.array(matfile['labels_sim']['theta']))[idxRange,:].view(self.nrows_phase)
        tx = torch.tensor(np.array(matfile['labels_sim']['tx']))[idxRange,:].view(self.nrows_phase)
        ty = torch.tensor(np.array(matfile['labels_sim']['ty']))[idxRange,:].view(self.nrows_phase)
        
        #take only the non-zero angle. assuming 1 motion event
        theta = theta[ torch.where(torch.abs(theta) == torch.max(torch.abs(theta))) ]
        tx = tx[ torch.where(torch.abs(tx) == torch.max(torch.abs(tx))) ]
        ty = ty[ torch.where(torch.abs(ty) == torch.max(torch.abs(ty))) ]
        if torch.sum(theta) == 0:
            theta=torch.tensor(0.0)
        if torch.sum(tx) == 0:
            tx=torch.tensor(0.0)
        if torch.sum(ty) == 0:
            ty=torch.tensor(0.0)

        ''' #was for version without different speeds 
        if theta.ndim>0 and theta.squeeze().shape[0] > 1:
            middleIdx = int( np.ceil(theta.squeeze().shape[0]/2) )
            theta = theta[middleIdx]
        if tx.ndim>0 and tx.squeeze().shape[0] > 1:
            middleIdx = int( np.ceil(tx.squeeze().shape[0]/2) )
            tx = tx[middleIdx]
        if ty.ndim >0 and ty.squeeze().shape[0] > 1:
            middleIdx = int( np.ceil(ty.squeeze().shape[0]/2) )
            ty = ty[middleIdx]
        '''
        
        #y=torch.tensor( [ theta, tx, ty ])

        #take whatevwer index, they should all be
        #the same since we just did torch.where(torch.max...) above
        if theta.ndim > 0 and tx.ndim > 0 and ty.ndim > 0:
            y=torch.tensor( [ theta[0], tx[0], ty[0] ])
        else:
            #assuming 'NoMo' dataset with theta==tx==ty==0
            y=torch.tensor( [ theta, tx, ty ])
        
        #y=torch.cat((theta,tx,ty),dim=0)

        if self.x_format == 'real_imag':
            #if torch.Size([1, 3, 256]) -> cat along dim=0 which is channels in this case
            x = torch.cat((x_real, x_imag),dim=0)
        elif self.x_format == 'abs':
            x = torch.abs(x_real+1j*x_imag)
        elif self.x_format == 'phase':
            x = torch.angle(x_real+1j*x_imag)

        if self.y_format == 'theta_tx_ty':
            return x,y
        elif self.y_format == 'theta':
            y = y[0]
        elif self.y_format == 'tx':
            y = y[1]
        elif self.y_format == 'ty':
            y = y[2]
        elif self.y_format == 'tx_ty':
            y = y[1::]
            
        return x,y


#newer -> rather use that one
class CustomDataset_v2_preload(Dataset):
    def __init__(self, dataDir,datasetName, offset, PELine, x_format, y_format, coilConfig):
        #coilConfig: may be used later if needed to know if dataset is
        #individual coil or espirit combined
        datasetFullPath = os.path.join(dataDir, datasetName)
        print('>>>>>>>>>>>>>> loading dataset : ',datasetFullPath)
        with open(datasetFullPath,'rb') as f:
            data = pickle.load(f)
            
        self.nb_col = 256#frequency encoding 
        self.offset = offset
        self.PELine=PELine
        #training example format i.e. real+imag, abs only, phase only etc.
        self.x_format = x_format
        self.y_format = y_format
        #number of rows = 2*offset (because before and after) and +1 for central line
        self.nrows_phase = 2*offset+1
        
        #labels embedded in each example file 
        #self.training_examples_dir = dataDir
        #self.training_example_files = os.listdir(self.training_examples_dir)
        #self.training_example_files = [os.path.join(self.training_examples_dir, f) for f in self.training_example_files]

        if x_format == 'real_imag' or x_format == 'abs_phase':
            nb_channels = 2
        elif x_format == 'real_imag_correl':
            nb_channels = 4 #x_real,x_imag,xcorr_real,xcorr_imag
        elif x_format == 'abs' or x_format == 'phase':
            nb_channels = 1
            
        if y_format == 'theta_tx_ty':
            y_out_size = 3
        elif y_format == 'tx' or y_format == 'ty' or y_format == 'theta':
            y_out_size = 1
            
        self.N = len(data)#self.training_example_files)
        print('len data : ', self.N)
        
        #initialize variables to store dataset
        self.X = torch.zeros(self.N,nb_channels,self.nrows_phase,self.nb_col)
        self.Y = torch.zeros(self.N,y_out_size)
        
        #load all contents into RAM
        for idxf in range(self.N):
            
            print('>>>>>>>>>>>>>>> processing index #',idxf,'/',self.N,' ... ')
            
            #print('>>> im trying to load:'+self.training_example_files[idx])
            #matfile = h5py.File(self.training_example_files[idxf], 'r')
        
            #print(matfile.keys())
            idxRange = computeIdxRangeFromOffset(offset=self.offset, PELine=self.PELine)
            #manage 'old' key nomenclature
            tmp_keys = data[idxf].keys()
            if 'ksp_corrupted_real' not in tmp_keys and coilConfig=='espirit':
                ksp_corrupted_real_key = 'ksp_corrupted_combined_real'
                ksp_corrupted_imag_key = 'ksp_corrupted_combined_imag'
            else:
                ksp_corrupted_real_key = 'ksp_corrupted_real'
                ksp_corrupted_imag_key = 'ksp_corrupted_imag'
                
            #dict_keys(['ksp_corrupted_combined_real', 'ksp_corrupted_combined_imag', 'theta', 'tx', 'ty', 'cx_offset', 'cy_offset'])
            
            x_real = data[idxf][ksp_corrupted_real_key]#torch.tensor(np.array(matfile['ksp_corrupted_combined']['real'])).T[idxRange,:]
            x_imag = data[idxf][ksp_corrupted_imag_key]#torch.tensor(np.array(matfile['ksp_corrupted_combined']['imag'])).T[idxRange,:]

            #need to add channels dimension betfore cat
            x_real=x_real.view(1,x_real.shape[0],x_real.shape[1])
            x_imag=x_imag.view(1,x_imag.shape[0],x_imag.shape[1])
        
            #REM: always convert first to np.array for efficienfy reasons

            theta = data[idxf]['theta'] #torch.tensor(np.array(matfile['labels_sim']['theta']))[idxRange,:].view(self.nrows_phase)
            tx = data[idxf]['tx'] #torch.tensor(np.array(matfile['labels_sim']['tx']))[idxRange,:].view(self.nrows_phase)
            ty = data[idxf]['ty'] #torch.tensor(np.array(matfile['labels_sim']['ty']))[idxRange,:].view(self.nrows_phase)
        
            #take only the non-zero angle. assuming 1 motion event
            theta = theta[ torch.where(torch.abs(theta) == torch.max(torch.abs(theta))) ]
            tx = tx[ torch.where(torch.abs(tx) == torch.max(torch.abs(tx))) ]
            ty = ty[ torch.where(torch.abs(ty) == torch.max(torch.abs(ty))) ]
            if torch.sum(theta) == 0:
                theta=torch.tensor(0.0)
            if torch.sum(tx) == 0:
                tx=torch.tensor(0.0)
            if torch.sum(ty) == 0:
                ty=torch.tensor(0.0)

            #y=torch.tensor( [ theta, tx, ty ])

            #take whatevwer index, they should all be
            #the same since we just did torch.where(torch.max...) above
            if theta.ndim > 0 and tx.ndim > 0 and ty.ndim > 0:
                y=torch.tensor( [ theta[0], tx[0], ty[0] ])
            else:
                #assuming 'NoMo' dataset with theta==tx==ty==0
                y=torch.tensor( [ theta, tx, ty ] )
            
            #y=torch.cat((theta,tx,ty),dim=0)

            if self.x_format == 'real_imag':
                #if torch.Size([1, 3, 256]) -> cat along dim=0 which is channels in this case
                x = torch.cat((x_real, x_imag),dim=0)
            if self.x_format == 'abs_phase':
                x = torch.cat(( torch.abs(x_real+1j*x_imag) , torch.angle(x_real+1j*x_imag) ),dim=0)
            elif self.x_format == 'abs':
                x = torch.abs(x_real+1j*x_imag)
            elif self.x_format == 'phase':
                x = torch.angle(x_real+1j*x_imag)
            elif self.x_format == 'real_imag_correl':
                tmp_cat = torch.cat((x_real, x_imag),dim=0)
                x_correl = torch.tensor( normxcorr_adj(np.array(tmp_cat)) )
                x_correl_real = x_correl[0,:,:].unsqueeze(0)
                x_correl_imag = x_correl[1,:,:].unsqueeze(0)
                x = torch.cat((x_real, x_imag, x_correl_real, x_correl_imag),dim=0)
                
            #if self.y_format == 'theta_tx_ty':
            #    return x,y
            if self.y_format == 'theta':
                y = y[0]
            elif self.y_format == 'tx':
                y = y[1]
            elif self.y_format == 'ty':
                y = y[2]
            elif self.y_format == 'tx_ty':
                y = y[1::]

            self.X[idxf,:] = x
            self.Y[idxf,:] = y

            #matfile.close()
        #endfor
        del data#matfile

        
    def __len__(self):
        #nb files in the directory 'training_examples_path'
        return self.N

    def __getitem__(self, idx):
        return self.X[idx,:],self.Y[idx,:]



########################################### helper functions for composite .mat file

#***************************************************************************************************
#this function creates the actual dataset that will be fully loaded in RAM (~26GB for 250k examples)
#***************************************************************************************************
def createDataset_fromMat(srcDir, dstDir,dstFileName, offset, PELine, simuType='espirit'):
    ksp_corrupted_fieldName, ksp_nomo_fieldName = getSimuFieldNames(simuType)
    
    nb_col = 256#frequency encoding 
   
    #number of rows = 2*offset (because before and after) and +1 for central line
    nrows_phase = 2*offset+1

    files = get_mat_files(srcDir)
    #self.training_example_files = os.listdir(self.training_examples_dir)
    #self.training_example_files = [os.path.join(self.training_examples_dir, f) for f in self.training_example_files]
        
    N = len(files)

    #training set (or val or test)
    allData = []
    
    #load all contents into RAM
    for idxf in range(N):
        curr_file = files[idxf]
        print('Processing file: ', idxf,'/',N, '(', curr_file, ')',' ...')
        
        append_structs_from_mat_withOffset(curr_file, offset,PELine, allData, ksp_corrupted_fieldName, ksp_nomo_fieldName)
        
    #save allData to disk
    # Saving a list to a file
    with open(os.path.join(dstDir, dstFileName+'.pkl'), 'wb') as f:
        pickle.dump(allData, f)
    



########################################################################
def getSimuFieldNames(simuType):
    if simuType == 'espirit':
        return ('ksp_corrupted_combined', 'ksp_nomo_slice')
    elif simuType == 'coil':
        return ('ksp_corrupted_coil', 'ksp_nomo_coil')

    
def load_hdf5_group_inRAM(group):
    data = {}
    for key in group.keys():
        item = group[key]
        if isinstance(item, h5py.Dataset):
            dataset_data = item[()]  # Load dataset data into memory
            data[key] = dataset_data
        elif isinstance(item, h5py.Group):
            data[key] = load_hdf5_group_inRAM(item)
    return data

def load_hdf5_file_inRAM(filepath):
    with h5py.File(filepath, 'r') as f:
        data = load_hdf5_group_inRAM(f)
    return data


def loadAllFromMat(fn_list, simuType):
    n=len(fn_list)
    f_data = []
    for i in range(n):
        print('fn:',fn_list[i])
        allSimuForFile_i = load_structs_from_mat(fn_list[i], simuType)
        f_data.append(allSimuForFile_i)
    return f_data


def load_structs_from_mat(fn, simuType):
    ksp_corrupted_fieldName, ksp_nomo_fieldName = getSimuFieldNames(simuType)
    
    mf = h5py.File(fn,'r')
    simu_struct=mf['simu_struct']
    nb_simu = len(simu_struct[ksp_corrupted_fieldName])

    allSimuForFile = []
    for idx_simu in range(nb_simu):
        kspcorr=simu_struct[ksp_corrupted_fieldName]
        ksp_corrupted_combined_real = torch.tensor( np.array(mf[kspcorr[idx_simu][0]]['real']) )
        ksp_corrupted_combined_imag = torch.tensor( np.array(mf[kspcorr[idx_simu][0]]['imag']) )
        
        kspnomo=simu_struct[ksp_nomo_fieldName]
        ksp_nomo_slice_real = torch.tensor( np.array(mf[kspnomo[idx_simu][0]]['real']) )
        ksp_nomo_slice_imag = torch.tensor( np.array(mf[kspnomo[idx_simu][0]]['imag']) )
        
        labels_sim=simu_struct['labels_sim']
        labels_sim= mf[labels_sim[idx_simu][0]]
        # keys : <KeysViewHDF5 ['cx_offset', 'cy_offset', 'theta', 'tx', 'ty']>
        theta = torch.tensor(np.array( labels_sim['theta'] ))
        tx = torch.tensor(np.array( labels_sim['tx']))
        ty = torch.tensor(np.array( labels_sim['ty']))
        cx_offset = torch.tensor(np.array(labels_sim['cx_offset']).squeeze())
        cy_offset = torch.tensor(np.array(labels_sim['cy_offset']).squeeze())
        
        file_dict = {}
        file_dict['ksp_corrupted_real'] =ksp_corrupted_combined_real
        file_dict['ksp_corrupted_imag'] = ksp_corrupted_combined_imag
        file_dict['ksp_nomo_slice_real'] = ksp_nomo_slice_real
        file_dict['ksp_nomo_slice_imag'] = ksp_nomo_slice_imag
        file_dict['theta']=theta
        file_dict['tx']=tx
        file_dict['ty']=ty
        file_dict['cx_offset']=cx_offset
        file_dict['cy_offset']=cy_offset

        allSimuForFile.append(file_dict)
        
    mf.close()
    return allSimuForFile

def append_structs_from_mat_withOffset(fn, offset,PELine, allData, ksp_corrupted_fieldName, ksp_nomo_fieldName ):
    nrows_phase = 2*offset+1
    mf = h5py.File(fn,'r')
    simu_struct=mf['simu_struct']
    nb_simu = len(simu_struct[ksp_corrupted_fieldName])

    allSimuForFile = []

    idxRange = computeIdxRangeFromOffset(offset=offset, PELine=PELine)
        
    for idx_simu in range(nb_simu):
        kspcorr=simu_struct[ksp_corrupted_fieldName]
        ksp_corrupted_real = torch.tensor( np.array(mf[kspcorr[idx_simu][0]]['real']) ).T[idxRange,:]
        ksp_corrupted_imag = torch.tensor( np.array(mf[kspcorr[idx_simu][0]]['imag']) ).T[idxRange,:]
        
        #kspnomo=simu_struct[ksp_nomo_fieldName]
        #ksp_nomo_slice_real = torch.tensor( np.array(mf[kspnomo[idx_simu][0]]['real']) )
        #ksp_nomo_slice_imag = torch.tensor( np.array(mf[kspnomo[idx_simu][0]]['imag']) )
        
        labels_sim=simu_struct['labels_sim']
        labels_sim= mf[labels_sim[idx_simu][0]]
        # keys : <KeysViewHDF5 ['cx_offset', 'cy_offset', 'theta', 'tx', 'ty']>
        theta = torch.tensor(np.array( labels_sim['theta'] ))
        tx = torch.tensor(np.array( labels_sim['tx']))
        ty = torch.tensor(np.array( labels_sim['ty']))
        cx_offset = torch.tensor(np.array(labels_sim['cx_offset']).squeeze())
        cy_offset = torch.tensor(np.array(labels_sim['cy_offset']).squeeze())
        
        file_dict = {}
        file_dict['ksp_corrupted_real'] =ksp_corrupted_real
        file_dict['ksp_corrupted_imag'] = ksp_corrupted_imag
        #file_dict['ksp_nomo_slice_real'] = ksp_nomo_slice_real
        #file_dict['ksp_nomo_slice_imag'] = ksp_nomo_slice_imag
        file_dict['theta']=theta
        file_dict['tx']=tx
        file_dict['ty']=ty
        file_dict['cx_offset']=cx_offset
        file_dict['cy_offset']=cy_offset

        #IMPORTANT REMARK:
        #appends data to the EXTERNAL list allData that is passed as arg !
        allData.append(file_dict)
        
    mf.close()
    
