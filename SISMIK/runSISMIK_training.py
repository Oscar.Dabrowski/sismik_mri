import sys, os, time

### Description ###
#this is a script used for convenience:
#hardcode training parameters and run SISMIK tranining
#-> the main_train... .py script called at the end of this script
#receives the my_args cmd-line args and runs on an available GPU 

#possible values for x_format:
#'real_imag'
#'abs'
#'phase'

#possible values for y_format:
#'theta'
#'tx'
#'ty'
#'theta_tx_ty'
#'tx_ty'

#speeds 0 and 1 and ~700k training and as usual Siemens Prisma unseen testsets
#(but valset is different simu on same slices as train set)

dstDir = os.path.join(os.getcwd(),'trained_models')

srcDirTrain = '/home/od/data/sismik_datasets/combined/PE75/train/winSz9/'#e.g., like ...combined/PE75/train/winSz9/
srcDirVal = '/home/od/data/sismik_datasets/combined/PE75/val/winSz9/'
srcDirTest =  '/home/od/data/sismik_datasets/combined/PE75/test/winSz9/'

#adjust args depending on your hardware config, e.g. num of CPUS etc.
my_args = (' --epochs 101 '
           ' --minibatch_size 128 '
           ' --numworkers_train 8 '
           ' --numworkers_val 2 '
           ' --numworkers_test 1 '
           ' --windowSize 9 '
           ' --PELine_train 75 '
           ' --PELine_val 75 '
           ' --PELine_test 75 '
           ' --L2_reg 1.5 '
           ' --dstDir '+os.path.join(dstDir,'SISMIK_ks3_standard_pe75_winSize9_bs128_model_tr600k_100epochs_realImag_L2Reg1_5/')+' '
           ' --srcDirTrain '+srcDirTrain+' '
           ' --srcDirVal '+srcDirVal+' '
           ' --srcDirTest '+srcDirTest+' '
           ' --mapsDir kspace_normalization/muStdMaps/v7_3_format/ '
           ' --x_format real_imag '
           ' --y_format theta_tx_ty '
           ' --model_type ks3_medium '
           ' --nbconvFilters 128 '
           ' --nbConvLayers 7')


os.system('python3 main_trainSISMIK.py '+my_args)
